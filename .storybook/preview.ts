import type { Preview } from '@storybook/react';
import '../lib/styles/index.scss';

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    options: {
      /**
       *  Sort by two conditions and with priorities:
       *  Number of nestings, from max to min.
       *  Alphabet order, from Aa to Zz.
       */

      storySort: (a, b) => {
        // 1. Number of nestings.
        const aLength = a.title.split('/').length;
        const bLength = b.title.split('/').length;

        if (aLength > bLength) return -1;
        if (aLength < bLength) return 1;

        // 2. Alphabet order.
        const aTitle = a.id;
        const bTitle = b.id;

        if (aTitle !== bTitle) {
          return a.id.localeCompare(b.id, undefined, { numeric: true });
        }
        return 0;
      },
    },
  },

  tags: ['autodocs']
};

export default preview;
