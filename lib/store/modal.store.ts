import { ModalProps } from '@components/modal/modal.types';
import { create } from 'zustand';

type ModalState = {
  readonly isOpened: boolean;
  readonly data?: ModalProps;
};

type ModalActions = {
  readonly showModal: (payload: ModalProps) => void;
  readonly hideModal: () => void;
};

const initialValues: ModalState = {
  isOpened: false,
  data: undefined,
};

export const useModalStore = create<ModalState & ModalActions>((set) => ({
  ...initialValues,
  showModal: (data) => set((state) => ({ ...state, data, isOpened: true })),
  hideModal: () => set((state) => ({ ...state, isOpened: false })),
}));
