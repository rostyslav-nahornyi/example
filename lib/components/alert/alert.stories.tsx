import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { Meta, Story } from '@storybook/react';
import { useState } from 'react';
import { Alert } from './alert.component';
import { AlertProps } from './alert.types';
export default {
  title: 'UI-KIT/Alert',
  component: Alert,
} as Meta;

const Template: Story<AlertProps> = (args) => {
  const [isVisible, setIsVisible] = useState(args.isVisible);
  return (
    <Alert
      {...args}
      isVisible={isVisible}
      onClose={() => setIsVisible(false)}
    />
  );
};

const Default = Template.bind({});
Default.args = {
  text: 'Text',
  title: 'Title',
  icon: <Circle />,
  type: 'default',
  direction: 'vertical',
  isVisible: true,
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

const Success = Template.bind({});
Success.args = {
  ...Default.args,
  type: 'success',
};

const Warning = Template.bind({});
Warning.args = {
  ...Default.args,
  type: 'warning',
};

const Error = Template.bind({});
Error.args = {
  ...Default.args,
  type: 'error',
  direction: 'horizontal',
};

const Info = Template.bind({});
Info.args = {
  ...Default.args,
  type: 'info',
  direction: 'horizontal',
};

export { Default, Error, Info, Success, Warning };
