import { FC, ReactNode } from 'react';

import Close from '@assets/icons/alert-close.svg';
import CheckCircle from '@assets/icons/check-circle.svg';
import ErrorAlt from '@assets/icons/error-alt.svg';
import Error from '@assets/icons/error.svg';
import InfoCircle from '@assets/icons/info-circle.svg';
import { Text } from '@components';
import classNames from 'classnames';
import { Button } from '../button';
import styles from './alert.module.scss';
import { AlertProps } from './alert.types';

const renderIcon = (
  type: 'default' | 'success' | 'warning' | 'error' | 'info',
  icon: ReactNode,
) => {
  switch (type) {
    case 'default':
      return icon ? <div className={styles.icon}>{icon}</div> : null;
    case 'success':
      return <CheckCircle className={styles.icon} />;
    case 'error':
      return <ErrorAlt className={styles.icon} />;
    case 'warning':
      return <Error className={styles.icon} />;
    case 'info':
      return <InfoCircle className={styles.icon} />;
    default:
      return null;
  }
};

/**
 * Alert component
 * @description Alert
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { AlertProps } props - AlertProps defined in the './alert.types.ts'
 */
const Alert: FC<AlertProps> = ({
  text,
  type = 'default',
  icon,
  direction = 'vertical',
  title,
  controlButtons,
  className,
  isVisible,
  onClose,
}) => (
  <>
    {isVisible && (
      <div className={classNames(styles[direction], className)}>
        <div className={classNames(styles.headIcon, styles[type])}>{renderIcon(type, icon)}</div>
        <div className={styles.contentWrapper}>
          <div className={styles.textBlock}>
            <Text
              variant="caption14Medium"
              className={styles.title}
            >
              {title}
            </Text>
            <Text
              variant="caption14Regular"
              className={styles.text}
            >
              {text}
            </Text>
          </div>
          <div className={styles.controlButtons}>
            <Button
              className={styles.button}
              variant="main"
              buttonType="borderless"
              size="s"
              {...controlButtons?.main}
            />
            <Button
              className={styles.button}
              variant="minor"
              buttonType="borderless"
              size="s"
              {...controlButtons?.secondary}
            />
          </div>
        </div>
        <Close
          className={styles.closeIcon}
          onClick={onClose}
        />
      </div>
    )}
  </>
);

export { Alert };
