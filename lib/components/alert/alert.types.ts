import { ReactNode } from 'react';
import { ButtonProps } from '../button/button.types';
/**
 * Props of Alert component.
 *
 * @author Vitalii Bodnarchuk
 */
interface AlertProps {
  /**
   * Title
   */
  readonly title?: string;

  /**
   * Handle the visibility of alert
   */
  readonly isVisible?: boolean;

  /**
   * Handle the visibility of alert
   */
  readonly onClose?: () => void;

  /**
   * Text
   */
  readonly text?: string;
  /**
   * Type of alert
   */
  readonly type: 'default' | 'success' | 'warning' | 'error' | 'info';
  /**
   * Structure of alert
   */
  readonly direction?: 'vertical' | 'horizontal';
  /**
   * Icon for default type
   */
  readonly icon?: ReactNode;
  /**
   * Buttons
   */
  readonly controlButtons?: {
    /**
     * First button
     */
    readonly main: Pick<ButtonProps, 'label' | 'onClick' | 'disabled'>;

    /**
     * Second button
     */
    readonly secondary?: Pick<ButtonProps, 'label' | 'onClick' | 'disabled'>;
  };

  /**
   * ClassName
   */
  readonly className?: string;
}

export type { AlertProps };
