import { FC } from 'react';

import { Text } from '@components';
import classNames from 'classnames';
import styles from './toggle.module.scss';
import { ToggleProps } from './toggle.types';

/**
 * Toggle component
 * @description Toggle
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { ToggleProps } props - ToggleProps defined in the './toggle.types.ts'
 */
const Toggle: FC<ToggleProps> = ({ id, onChange, checked, helpText, text, disabled, icon }) => (
  <div className={styles.wrapper}>
    <div className={classNames(styles.toggle, disabled && styles.disabledWrapper)}>
      <label
        htmlFor={id}
        id="labelId"
        className={classNames(styles.label, checked && styles.checked)}
      >
        <input
          id={id}
          className={styles.input}
          type="checkbox"
          onChange={() => onChange(!checked)}
          checked={checked}
          aria-labelledby={id}
        />
      </label>

      <span className={classNames(styles.slider, checked && styles.sliderChecked)}>
        {icon && icon.length >= 2 ? (checked ? icon[1] : icon[0]) : null}
      </span>
    </div>
    {helpText ? (
      <div className={styles.helpBlock}>
        <Text
          variant="caption16Medium"
          className={styles.text}
        >
          {text}
        </Text>
        <Text
          variant="caption14Regular"
          className={styles.helpText}
        >
          {helpText}
        </Text>
      </div>
    ) : (
      <Text
        variant="caption14Medium"
        className={styles.text}
      >
        {text}
      </Text>
    )}
  </div>
);

export { Toggle };
