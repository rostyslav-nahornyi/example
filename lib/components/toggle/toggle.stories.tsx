import Moon from '@assets/icons/moon.svg';
import Sun from '@assets/icons/sun.svg';
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';
import { Toggle } from './toggle.component';

export default {
  title: 'UI-KIT/Toggle',
  component: Toggle,
} as Meta<typeof Toggle>;

const Template: StoryFn<typeof Toggle> = (args) => {
  const [isChecked, setIsChecked] = useState(false);

  return (
    <Toggle
      {...args}
      checked={isChecked}
      onChange={() => setIsChecked(!isChecked)}
    />
  );
};

const Default = Template.bind({});
Default.args = {};

const WithText = Template.bind({});
WithText.args = {
  ...Default.args,
  text: 'Label',
};

const WithIcon = Template.bind({});
WithIcon.args = {
  ...Default.args,
  icon: [<Moon key="icon" />, <Sun key="icon" />],
};

const WithIconAndText = Template.bind({});
WithIconAndText.args = {
  ...WithIcon.args,
  icon: [<Moon key="icon" />, <Sun key="icon" />],
  text: 'Label',
};

const WithIconAndHelpText = Template.bind({});
WithIconAndHelpText.args = {
  ...Default.args,
  text: 'Label',
  helpText: 'This is help text for the toggle.',
  icon: [<Moon key="icon" />, <Sun key="icon" />],
};

export { Default, WithIcon, WithIconAndHelpText, WithIconAndText, WithText };
