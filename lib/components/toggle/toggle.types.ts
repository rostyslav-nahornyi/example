import { ReactNode } from 'react';

/**
 * Props of Toggle component.
 *
 * @author Vitalii Bodnarchuk
 */
type ToggleProps = {
  /**
   *  Checked value
   */
  readonly checked: boolean;

  /**
   * Specifies input id
   */
  readonly id: string;

  /**
   * a callback function which updates appropriate form field with the input value
   */
  readonly onChange: (value: boolean) => void;

  /**
   * The label text for the toggle.
   */
  readonly text?: string;

  /**
   * The help text of the label for the toggle.
   */
  readonly helpText?: string;

  /**
   * Is disabled.
   */
  readonly disabled?: boolean;

  /**
   * Icon of the toggle
   */
  readonly icon?: [ReactNode, ReactNode];
};

export type { ToggleProps };
