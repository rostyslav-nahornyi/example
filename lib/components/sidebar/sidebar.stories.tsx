import { Meta, StoryFn } from '@storybook/react';

import AvatarIcon from '@assets/icons/avatar-1.svg';
import Circle from '@assets/icons/circle.svg';
import Icon from '@assets/icons/logo.svg';
import { Badge } from '../badge';

import { action } from '@storybook/addon-actions';
import { Sidebar } from './sidebar.component';
import styles from './sidebar.module.scss';

export default {
  title: 'UI-KIT/Sidebar',
  component: Sidebar,
} as Meta<typeof Sidebar>;

const Template: StoryFn<typeof Sidebar> = (props) => <Sidebar {...props} />;

const Basic = Template.bind({});
Basic.args = {
  header: {
    logo: <Icon className={styles.logoImg} />,
    name: 'Document',
    onClick: () => action('click')('Header'),
  },
  textField: {
    searchPlaceholder: 'Search',
  },
  sections: [
    [
      {
        title: 'TITLE',
        leadingIcon: <Circle />,
        label: 'Qwerty',
        selectedItem: 'Qwerty',
        trailingIcon: (
          <Badge
            badgeType={'default'}
            emphasis="low"
            size="small"
            label="60"
          />
        ),
      },
      {
        leadingIcon: <Circle />,
        label: 'Label',
        selectedItem: null,
        trailingIcon: (
          <Badge
            badgeType={'default'}
            emphasis="low"
            size="small"
            label="60"
          />
        ),
      },
      {
        leadingIcon: <Circle />,
        label: 'Label',
        selectedItem: null,
        trailingIcon: (
          <Badge
            badgeType={'colored'}
            emphasis="low"
            size="small"
            label="60"
          />
        ),
      },
    ],
  ],
};

const WithSearch = Template.bind({});
WithSearch.args = {
  ...Basic.args,
  textField: {
    searchable: true,
  },
};

const WithProfile = Template.bind({});
WithProfile.args = {
  ...Basic.args,
  textField: {
    searchable: true,
  },
  profileBlock: {
    withProfile: true,
    avatarImageSource: <AvatarIcon />,
    profileTitle: 'Leslie Alexander',
    profileText: 'lesieexample@mail.com',
    icon: <Circle />,
  },
};

const WithProfileAndNavigation = Template.bind({});
WithProfileAndNavigation.args = {
  ...WithProfile.args,
  profileBlock: {
    isNavigated: true,
  },
};

const WithButton = Template.bind({});
WithButton.args = {
  ...Basic.args,
  buttonConfiguration: {
    expand: true,
    placeholder: 'Label',
    onButtonClick: () => action('clicked')(),
  },
};

const WithDropdown: StoryFn<typeof Sidebar> = () => {
  const handleSelect = (value: string) => {
    action('selected')(value);
  };

  return (
    <div>
      <Sidebar
        header={{
          logo: <Icon className={styles.logoImg} />,
          name: 'Document',
        }}
        textField={{
          searchPlaceholder: 'Search',
        }}
        dropdownConfig={{
          placeholder: 'Label',
          onDropdownSelect: handleSelect,
          hideChevron: true,
          buttonIcon: <Circle />,
          expand: true,
          options: [
            { value: 'Item 1', id: '1', leadingIcon: <Circle /> },
            { value: 'Item 2', id: '2' },
            { value: 'Item 3', id: '3' },
            { value: 'Item 4', id: '4', leadingIcon: <Circle /> },
          ],
        }}
        sections={[
          [
            {
              title: 'TITLE',
              leadingIcon: <Circle />,
              label: 'Qwerty',
              selectedItem: 'Qwerty',
              trailingIcon: (
                <Badge
                  badgeType={'default'}
                  emphasis="low"
                  size="small"
                  label="60"
                />
              ),
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
              trailingIcon: (
                <Badge
                  badgeType={'default'}
                  emphasis="low"
                  size="small"
                  label="60"
                />
              ),
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
              trailingIcon: (
                <Badge
                  badgeType={'colored'}
                  emphasis="low"
                  size="small"
                  label="60"
                />
              ),
            },
          ],
          [
            {
              title: 'TITLE',
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
              expandable: true,
              trailingIcon: (
                <Badge
                  badgeType={'default'}
                  emphasis="low"
                  size="small"
                  label="60"
                />
              ),
              items: [
                {
                  icon: <Circle />,
                  label: 'qaz',
                  trailingBadge: {
                    label: '60',
                    badgeType: 'default',
                  },
                  selectedItem: 'qaz',
                },
                {
                  icon: <Circle />,
                  label: 'Label',
                  trailingBadge: {
                    label: '60',
                    badgeType: 'default',
                  },
                },
                {
                  icon: <Circle />,
                  label: 'Label',
                  trailingBadge: {
                    label: '60',
                    badgeType: 'default',
                  },
                },
              ],
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
            },
          ],
          [
            {
              title: 'TITLE',
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
            },
            {
              leadingIcon: <Circle />,
              label: 'Label',
              selectedItem: null,
            },
          ],
        ]}
      />
    </div>
  );
};

export { Basic, WithButton, WithDropdown, WithProfile, WithProfileAndNavigation, WithSearch };
