import { FC, useState } from 'react';

import Search from '@assets/icons/search.svg';
import classNames from 'classnames';
import { Avatar } from '../avatar';
import { DropdownButton } from '../dropdown/components';
import { Dropdown } from '../dropdown/dropdown.component';
import { Text } from '../text';
import { TextField } from '../text-field';
import { NavigationItem } from './components/navigation-item';
import styles from './sidebar.module.scss';
import { SidebarProps } from './sidebar.types';

/**
 * Sidebar
 * @description Sidebar component displayed on the left side of the app.
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { SidebarProps } props - SidebarProps defined in the './sidebar.types.ts'.
 */
const Sidebar: FC<SidebarProps> = ({
  header,
  sections,
  textField,
  dropdownConfig,
  profileBlock,
  buttonConfiguration,
}) => {
  const onDropdownSelect = dropdownConfig?.onDropdownSelect;
  const { onButtonClick, buttonIcon, placeholder, hideChevron, expand } = buttonConfiguration ?? {};
  const { withProfile, avatarImageSource, profileTitle, profileText, icon, isNavigated } =
    profileBlock ?? {};
  const { searchable, searchPlaceholder } = textField ?? {};
  const { logo, name, onClick } = header;

  const [searchQuery, setSearchQuery] = useState<string>();

  const handleSearchChange = (value: string) => {
    setSearchQuery(value);
  };

  const filteredSections = searchQuery
    ? sections
        .map((section) =>
          section.filter((item) => item.label.toLowerCase().includes(searchQuery.toLowerCase())),
        )
        .filter((section) => section.length > 0)
    : sections;

  return (
    <aside className={styles.wrapper}>
      <div
        role="button"
        className={styles.logo}
        onClick={onClick}
        onKeyDown={onClick}
        tabIndex={0}
      >
        <div className={styles.img}>{logo}</div>
        <Text
          variant="caption20Medium"
          className={styles.name}
        >
          {name}
        </Text>
      </div>
      {searchable && (
        <TextField
          leadingIcon={<Search />}
          placeholder={searchPlaceholder}
          value={searchQuery}
          onChange={handleSearchChange}
        />
      )}
      {onButtonClick && (
        <DropdownButton
          expand={expand}
          className={styles.button}
          icon={buttonIcon}
          onClick={onButtonClick}
          placeholder={placeholder}
          hideChevron={hideChevron}
        />
      )}
      {dropdownConfig && dropdownConfig.options && (
        <Dropdown
          className={styles.dropdown}
          button={
            <DropdownButton
              className={styles.button}
              icon={dropdownConfig.buttonIcon}
              placeholder={dropdownConfig.placeholder}
              hideChevron={dropdownConfig.hideChevron}
            />
          }
          options={dropdownConfig.options}
          onSelect={(value) => onDropdownSelect && onDropdownSelect(value)}
          expand={dropdownConfig.expand}
        />
      )}
      <div
        className={classNames(
          styles.menu,
          withProfile && !isNavigated && styles.menuWithProfile,
          dropdownConfig && styles.menuWithButton,
        )}
      >
        {filteredSections.length > 0 &&
          filteredSections.map((section, index) => (
            <div
              className={styles.section}
              key={index}
            >
              {section.map((item, itemIndex) => (
                <NavigationItem
                  {...item}
                  key={item.label + itemIndex}
                />
              ))}
            </div>
          ))}
        {withProfile && (
          <div className={classNames(styles.profile, isNavigated && styles.profileNavigation)}>
            <div className={styles.content}>
              {avatarImageSource && (
                <Avatar
                  statusIndicatorPosition="off"
                  img={avatarImageSource}
                  size="m"
                />
              )}
              <div className={styles.textBlock}>
                <Text
                  variant="caption16Medium"
                  className={styles.title}
                >
                  {profileTitle}
                </Text>
                <Text
                  variant="caption14Regular"
                  className={styles.text}
                >
                  {profileText}
                </Text>
              </div>
            </div>
            {icon && icon}
          </div>
        )}
      </div>
    </aside>
  );
};

export { Sidebar };
