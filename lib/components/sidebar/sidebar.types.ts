import { ReactNode } from 'react';
import { DropdownMenuProps } from '../dropdown/components/dropdown-menu/dropdown-menu.types';
import { NavigationItemProps } from './components/navigation-item/navigation-item.types';

/**
 * SidebarProps
 * @description Props for the Sidebar component.
 *
 * @author Vitalii Bodnarchuk
 */
type SidebarProps = {
  // HEADER
  readonly header: {
    /**
     * Specifies current company logo image source.
     */
    readonly logo: ReactNode;

    /**
     * Specifies current name
     */
    readonly name?: string;

    /**
     * On header click handler.
     */
    readonly onClick?: () => void;
  };

  // Sections and Navigation
  /**
   * Array of menu buttons properties.
   */
  readonly sections: ReadonlyArray<ReadonlyArray<NavigationItemProps>>;

  // TextField
  readonly textField?: {
    /**
     * Add input
     */
    readonly searchable?: boolean;

    /**
     * Placeholder of textField
     */
    readonly searchPlaceholder?: string;
  };

  // Profile block
  readonly profileBlock?: {
    /**
     * Add profile block
     */
    readonly withProfile?: boolean;

    /**
     * Avatar img source
     */
    readonly avatarImageSource?: ReactNode;

    /**
     * Title of profile block
     */
    readonly profileTitle?: string;

    /**
     * Text of profile block
     */
    readonly profileText?: string;

    /**
     * Icon of profile block
     */
    readonly icon?: ReactNode;

    /**
     * Specifies profileBlock to be static or fixed
     */
    readonly isNavigated?: boolean;
  };

  // Button configuration
  readonly buttonConfiguration?: {
    /**
     * Specifies profileBlock to be with button
     */
    readonly onButtonClick?: () => void;

    /**
     * Icon of button
     */
    readonly buttonIcon?: ReactNode;

    /**
     * Label of button
     */
    readonly placeholder?: string;

    /**
     * Expand button
     */
    readonly expand?: boolean;

    /**
     * Hide chevron of dropdown
     */
    readonly hideChevron?: boolean;
  };

  // Dropdown configuration
  readonly dropdownConfig?: {
    /**
     * Specifies profileBlock to be with button
     */
    readonly onDropdownSelect?: (value: string) => void;

    /**
     * Icon of button
     */
    readonly buttonIcon?: ReactNode;

    /**
     * Expand button
     */
    readonly expand?: boolean;

    /**
     * Label of button
     */
    readonly placeholder?: string;

    /**
     * Hide chevron of dropdown
     */
    readonly hideChevron?: boolean;

    /**
     * Options list
     */
    readonly options: DropdownMenuProps['options'];
  };
};

export type { SidebarProps };
