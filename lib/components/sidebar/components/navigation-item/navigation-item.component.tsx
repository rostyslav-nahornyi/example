import { FC, useState } from 'react';

import ChevronDown from '@assets/icons/chevron-down.svg';
import { Badge, Text } from '@components';
import classNames from 'classnames';
import styles from './navigation-item.module.scss';
import { NavigationItemProps } from './navigation-item.types';

/**
 * NavigationItem component
 * @description Exclusively for the Sidebar component as its menu navigation item
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { NavigationProps } props - NavigationItemProps defined in the './navigation-item.types.ts'
 */
const NavigationItem: FC<NavigationItemProps> = ({
  items,
  leadingIcon,
  label,
  expandable,
  trailingIcon,
  onSelect,
  disabled,
  selectedItem,
  title,
}) => {
  const [isUnfolded, setIsUnfolded] = useState<boolean>(false);

  const handleToggleAndSelect = (
    event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>,
    itemLabel?: string,
  ) => {
    setIsUnfolded(!isUnfolded);
    if (onSelect) {
      onSelect(event, itemLabel ?? label);
    }
  };

  const handleDropdownItemClick = (
    e: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>,
    itemLabel: string,
  ) => {
    onSelect && onSelect(e, itemLabel);
  };

  const isSelected = label === selectedItem;

  return (
    <div className={styles.wrapper}>
      <div className={classNames(styles.content)}>
        <Text
          variant="caption14Medium"
          className={styles.title}
        >
          {title}
        </Text>
        <button
          className={classNames(styles.item, isSelected && styles.selected)}
          onClick={(e) => handleToggleAndSelect(e)}
          disabled={disabled}
        >
          <div className={styles.block}>
            {leadingIcon}
            {label}
          </div>
          <div className={styles.blockWithTrailing}>
            {trailingIcon && (
              <div className={classNames(styles.icon, !expandable && styles.trailingIcon)}>
                {trailingIcon}
              </div>
            )}
            {expandable && <ChevronDown className={classNames(isUnfolded && styles.up)} />}
          </div>
        </button>
      </div>

      {expandable && (
        <div className={classNames(styles.list, isUnfolded && styles.unfolded)}>
          {!!items?.length &&
            items?.map(
              (
                {
                  label: itemLabel,
                  href,
                  icon,
                  onClick: onItemClick,
                  disabled,
                  trailingBadge,
                  selectedItem,
                },
                index,
              ) => {
                const { badgeType, label } = trailingBadge ?? {};
                const isChildItemSelected = itemLabel === selectedItem;

                return href ? (
                  <a
                    href={href}
                    className={classNames(
                      styles.item,
                      styles.expandingBlock,
                      isChildItemSelected && styles.selected,
                    )}
                    key={itemLabel ? itemLabel.concat(index.toString()) : `item-${index}`}
                    onClick={(e) => {
                      e.preventDefault();
                      if (disabled) return;
                      itemLabel && handleDropdownItemClick(e, itemLabel);
                      onItemClick && onItemClick(e);
                    }}
                  >
                    <div className={styles.itemChildWithTrailing}>
                      <div className={styles.itemChild}>
                        {icon}
                        {itemLabel}
                      </div>
                      <div className={styles.trailingIcon}>{trailingIcon}</div>
                    </div>
                  </a>
                ) : (
                  <button
                    className={classNames(
                      styles.item,
                      styles.expandingBlock,
                      isChildItemSelected && styles.selected,
                    )}
                    onClick={(e) => {
                      e.preventDefault();
                      if (disabled) return;
                      itemLabel && handleDropdownItemClick(e, itemLabel);
                      onItemClick && onItemClick(e);
                    }}
                    key={itemLabel ? itemLabel.concat(index.toString()) : `item-${index}`}
                    disabled={disabled}
                  >
                    <div className={styles.itemChildWithTrailing}>
                      <div className={styles.itemChild}>
                        {icon}
                        {itemLabel}
                      </div>
                      {badgeType && (
                        <Badge
                          className={styles.trailingIcon}
                          badgeType={badgeType}
                          emphasis="low"
                          size="small"
                          label={label}
                        />
                      )}
                    </div>
                  </button>
                );
              },
            )}
        </div>
      )}
    </div>
  );
};

export { NavigationItem };
