import { BaseHTMLAttributes, MouseEvent, ReactNode } from 'react';

import { BadgeProps } from '../../../badge/badge.types';

/**
 * Props of NavigationItem component.
 *
 * @author Vitalii Bodnarchuk
 */

type NavigationItemProps = {
  /**
   * The list of items to show.
   */
  readonly items?: ReadonlyArray<NavigationItemChild>;

  /**
   * Specifies navigation item label text
   */
  readonly label: string;

  /**
   * Navigation item icon element
   */
  readonly leadingIcon: ReactNode;

  /**
   * Is expandable?
   */
  readonly expandable?: boolean;
  /**
   * Navigation item trailing icon element
   */
  readonly trailingIcon?: ReactNode;

  /**
   * Disabled
   */
  readonly disabled?: boolean;

  /**
   * Function which select item
   */
  readonly onSelect?: (
    event: MouseEvent<HTMLButtonElement | HTMLAnchorElement>,
    itemLabel?: string,
  ) => void;

  /**
   * Selected item
   */
  readonly selectedItem?: string | null;

  /**
   * Additional title for item
   */
  readonly title?: string;
};

interface NavigationItemChild
  extends Pick<BaseHTMLAttributes<HTMLAnchorElement>, 'href'>,
    Pick<BaseHTMLAttributes<HTMLButtonElement>, 'onClick'> {
  /**
   * Selected item
   */
  readonly selectedItem?: string | null | boolean;

  /**
   * Specifies contact label text.
   */
  readonly label?: string;

  /**
   * Contact icon element.
   */
  readonly icon?: ReactNode;

  /**
   * Specifies if the item is currently disabled.
   */
  readonly disabled?: boolean;

  /**
   * Trailing badge
   */
  readonly trailingBadge?: Pick<BadgeProps, 'badgeType' | 'label'>;

  /**
   * Function in expanded item
   */
  readonly onClick?: (event: React.MouseEvent<HTMLAnchorElement | HTMLButtonElement>) => void;
}

export type { NavigationItemProps };
