import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import Circle from '@assets/icons/circle.svg';
import { Badge, Indicator } from '@components';
import { action } from '@storybook/addon-actions';
import { NavigationItem } from './navigation-item.component';

export default {
  title: 'UI-KIT/Sidebar/NavigationItem',
  component: NavigationItem,
} as Meta<typeof NavigationItem>;

const Template: StoryFn<typeof NavigationItem> = (args) => {
  const [selectedItem, setSelectedItem] = useState<string>();

  const handleItemSelected = (
    event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>,
    label?: string,
  ) => {
    event.preventDefault();
    setSelectedItem(label);
  };

  return (
    <NavigationItem
      {...args}
      selectedItem={selectedItem}
      onSelect={handleItemSelected}
    />
  );
};

const ItemExpanding = Template.bind({});
ItemExpanding.args = {
  leadingIcon: <Circle />,
  label: 'Label',
  expandable: true,
  items: [
    {
      icon: <Circle />,
      label: 'Label 1',
      href: '1',
    },
    {
      icon: <Circle />,
      label: 'Label 2',
      onClick: action('clicked on the button item'),
      href: '2',
      selectedItem: 'Label 2',
    },
    {
      icon: <Circle />,
      label: 'Label 3',
      onClick: action('clicked on the button item'),
    },
    {
      icon: <Circle />,
      label: 'Label 4',
      onClick: action('clicked on the button item'),
    },
    {
      icon: <Circle />,
      label: 'Label 5',
      onClick: action('clicked on the button item'),
    },
  ],
};

const ItemIndicator = Template.bind({});
ItemIndicator.args = {
  leadingIcon: <Circle />,
  label: 'Label',
  trailingIcon: (
    <Indicator
      size="s"
      type="success"
    />
  ),
};

const ItemDefault = Template.bind({});
ItemDefault.args = {
  label: 'Label',
  title: 'TITLE',
};

const ItemWithBadge: StoryFn<typeof NavigationItem> = () => {
  const selectedItem = 'Badge selected';

  return (
    <NavigationItem
      leadingIcon={<Circle />}
      label="Label"
      expandable={true}
      selectedItem={selectedItem}
      items={[
        {
          icon: <Circle />,
          label: 'Badge selected',
          href: '1',
        },
        {
          icon: <Circle />,
          label: 'Label 2',
          onClick: action('clicked on the button item'),
          href: '2',
          selectedItem: 'Label 2',
        },
        {
          icon: <Circle />,
          label: 'Label 3',
          onClick: action('clicked on the button item'),
        },
        {
          icon: <Circle />,
          label: 'Label 4',
          onClick: action('clicked on the button item'),
        },
        {
          icon: <Circle />,
          label: 'Label 5',
          onClick: action('clicked on the button item'),
        },
      ]}
      trailingIcon={
        <Badge
          badgeType={selectedItem !== 'Badge selected' ? 'colored' : 'default'}
          emphasis="low"
          size="small"
          label="Label"
        />
      }
    />
  );
};

const ItemDisabled = Template.bind({});
ItemDisabled.args = {
  label: 'Label',
  disabled: true,
};

export { ItemDefault, ItemDisabled, ItemExpanding, ItemIndicator, ItemWithBadge };
