import { FC } from 'react';
import { NumberProps } from './number.types';

import textStyles from '@components/text/text.module.scss';
import classNames from 'classnames';

/**
 * Typography.Number component
 * @description component which render number with different props
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { NumberProps } props - NumberProps defined in the './number.types.ts'
 */
const Number: FC<NumberProps> = ({ variant, value }) => (
  <p className={classNames(textStyles[variant])}>{value}</p>
);

export { Number };
