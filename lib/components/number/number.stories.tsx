import { Meta, StoryFn } from '@storybook/react';

import { Number } from './number.component';

export default {
  title: 'UI-KIT/Typography/Number',
  component: Number,
} as Meta<typeof Number>;

const Template: StoryFn<typeof Number> = (props) => <Number {...props} />;

const Basic = Template.bind({});
Basic.args = {
  value: 111,
};

const heading128Bold = Template.bind({});
heading128Bold.args = {
  ...Basic.args,
  variant: 'heading128Bold',
};

const heading96Bold = Template.bind({});
heading96Bold.args = {
  ...Basic.args,
  variant: 'heading96Bold',
};

const heading64Bold = Template.bind({});
heading64Bold.args = {
  ...Basic.args,
  variant: 'heading64Bold',
};

const heading36Semibold = Template.bind({});
heading36Semibold.args = {
  ...Basic.args,
  variant: 'heading36Semibold',
};

const heading20Semibold = Template.bind({});
heading20Semibold.args = {
  ...Basic.args,
  variant: 'heading20Semibold',
};

const caption56Medium = Template.bind({});
caption56Medium.args = {
  ...Basic.args,
  variant: 'caption56Medium',
};

const caption40Medium = Template.bind({});
caption40Medium.args = {
  ...Basic.args,
  variant: 'caption40Medium',
};

const caption32Bold = Template.bind({});
caption32Bold.args = {
  ...Basic.args,
  variant: 'caption32Bold',
};

const caption24Semibold = Template.bind({});
caption24Semibold.args = {
  ...Basic.args,
  variant: 'caption24Semibold',
};

const caption24Medium = Template.bind({});
caption24Medium.args = {
  ...Basic.args,
  variant: 'caption24Medium',
};

const caption22Medium = Template.bind({});
caption22Medium.args = {
  ...Basic.args,
  variant: 'caption22Medium',
};

const caption20Medium = Template.bind({});
caption20Medium.args = {
  ...Basic.args,
  variant: 'caption20Medium',
};

const caption18Semibold = Template.bind({});
caption18Semibold.args = {
  ...Basic.args,
  variant: 'caption18Semibold',
};

const caption18Medium = Template.bind({});
caption18Medium.args = {
  ...Basic.args,
  variant: 'caption18Medium',
};

const caption16Bold = Template.bind({});
caption16Bold.args = {
  ...Basic.args,
  variant: 'caption16Bold',
};

const caption16Medium = Template.bind({});
caption16Medium.args = {
  ...Basic.args,
  variant: 'caption16Medium',
};

const caption14Semibold = Template.bind({});
caption14Semibold.args = {
  ...Basic.args,
  variant: 'caption14Semibold',
};

const caption14Medium = Template.bind({});
caption14Medium.args = {
  ...Basic.args,
  variant: 'caption14Medium',
};

const caption14Regular = Template.bind({});
caption14Regular.args = {
  ...Basic.args,
  variant: 'caption14Regular',
};

const caption12Medium = Template.bind({});
caption12Medium.args = {
  ...Basic.args,
  variant: 'caption12Medium',
};

const caption12Regular = Template.bind({});
caption12Regular.args = {
  ...Basic.args,
  variant: 'caption12Regular',
};

const body16Medium = Template.bind({});
body16Medium.args = {
  ...Basic.args,
  variant: 'body16Medium',
};

const body16Regular = Template.bind({});
body16Regular.args = {
  ...Basic.args,
  variant: 'body16Regular',
};

const body14Medium = Template.bind({});
body14Medium.args = {
  ...Basic.args,
  variant: 'body14Medium',
};

const body14Regular = Template.bind({});
body14Regular.args = {
  ...Basic.args,
  variant: 'body14Regular',
};

const body12Regular = Template.bind({});
body12Regular.args = {
  ...Basic.args,
  variant: 'body12Regular',
};

const label20Medium = Template.bind({});
label20Medium.args = {
  ...Basic.args,
  variant: 'label20Medium',
};

const label18Medium = Template.bind({});
label18Medium.args = {
  ...Basic.args,
  variant: 'label18Medium',
};

const label16Medium = Template.bind({});
label16Medium.args = {
  ...Basic.args,
  variant: 'label16Medium',
};

const label14Medium = Template.bind({});
label14Medium.args = {
  ...Basic.args,
  variant: 'label14Medium',
};

const link20Medium = Template.bind({});
link20Medium.args = {
  ...Basic.args,
  variant: 'link20Medium',
};

const link18Medium = Template.bind({});
link18Medium.args = {
  ...Basic.args,
  variant: 'link18Medium',
};

const link16Medium = Template.bind({});
link16Medium.args = {
  ...Basic.args,
  variant: 'link16Medium',
};

const link14Medium = Template.bind({});
link14Medium.args = {
  ...Basic.args,
  variant: 'link14Medium',
};

export {
  body12Regular,
  body14Medium,
  body14Regular,
  body16Medium,
  body16Regular,
  caption12Medium,
  caption12Regular,
  caption14Medium,
  caption14Regular,
  caption14Semibold,
  caption16Bold,
  caption16Medium,
  caption18Medium,
  caption18Semibold,
  caption20Medium,
  caption22Medium,
  caption24Medium,
  caption24Semibold,
  caption32Bold,
  caption40Medium,
  caption56Medium,
  heading128Bold,
  heading20Semibold,
  heading36Semibold,
  heading64Bold,
  heading96Bold,
  label14Medium,
  label16Medium,
  label18Medium,
  label20Medium,
  link14Medium,
  link16Medium,
  link18Medium,
  link20Medium,
};
