import { TextProps } from '../text/text.types';

/**
 * NumberProps
 * @description Props for the Typography.Number component
 *
 * @author Vitalii Bodnarchuk
 */
interface NumberProps extends Pick<TextProps, 'variant'> {
  /**
   * Number
   */
  readonly value?: number;
}

export type { NumberProps };
