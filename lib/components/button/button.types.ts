import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from 'react';

/**
 * Props of Button component.
 *
 * @author Vitalii Bodnarchuk
 */
interface ButtonProps
  extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  /**
   *  Specifies button size.
   */
  readonly size: 's' | 'm' | 'l' | 'xl';

  /**
   * Label.
   */
  readonly label?: string;

  /**
   * Specifies button variant.
   */
  readonly variant: 'main' | 'minor' | 'danger' | 'warning' | 'success';

  /**
   * Specifies button type.
   */
  readonly buttonType: 'primary' | 'secondary' | 'outlined' | 'borderless' | 'floating' | 'link';

  /**
   * Left button icon.
   */
  readonly leadingIcon?: ReactNode;

  /**
   * Right button icon.
   */
  readonly trailingIcon?: ReactNode;

  /**
   * Disabled
   */
  readonly disabled?: boolean;
}

export type { ButtonProps };
