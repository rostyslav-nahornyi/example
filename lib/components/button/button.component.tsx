import { Ref, forwardRef } from 'react';

import classNames from 'classnames';
import styles from './button.module.scss';
import { ButtonProps } from './button.types';

/**
 * Button component
 * @description Button
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { ButtonProps } props - ButtonProps defined in the './button.types.ts'
 */
const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      size,
      variant,
      leadingIcon,
      trailingIcon,
      label,
      className,
      style,
      type = 'button',
      buttonType,
      disabled,
      onClick,
      children,
    },
    ref: Ref<HTMLButtonElement>,
  ) => {
    const isIconOnly = !label && (leadingIcon || trailingIcon);
    return (
      <button
        className={classNames(
          styles.button,
          styles[size],
          styles[variant],
          styles[buttonType],
          isIconOnly && styles.iconOnly,
          disabled && styles.disabled,
          className,
        )}
        type={type}
        disabled={disabled}
        style={style}
        tabIndex={0}
        aria-label={'button'}
        ref={ref}
        onClick={onClick}
      >
        <>
          {leadingIcon && <div className={styles.icon}>{leadingIcon}</div>}
          {label && <p>{label}</p>}
          {!label && children && children}
          {trailingIcon && <div className={styles.icon}>{trailingIcon}</div>}
        </>
      </button>
    );
  },
);
Button.displayName = 'Button';

export { Button };
