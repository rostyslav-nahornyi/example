import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { Button } from './button.component';

export default {
  title: 'UI-KIT/Button',
  component: Button,
} as Meta<typeof Button>;

const Template: StoryFn<typeof Button> = (props) => (
  <Button
    {...props}
    onClick={action('clicked on button...')}
  />
);

const MainPrimary = Template.bind({});
MainPrimary.args = {
  size: 's',
  buttonType: 'primary',
  variant: 'main',
  label: 'Label',
  leadingIcon: <Circle />,
};

const MinorPrimary = Template.bind({});
MinorPrimary.args = {
  size: 's',
  buttonType: 'primary',
  variant: 'minor',
  label: 'Label',
  leadingIcon: <Circle />,
};

const DangerPrimary = Template.bind({});
DangerPrimary.args = {
  size: 's',
  buttonType: 'primary',
  variant: 'danger',
  label: 'Label',
  leadingIcon: <Circle />,
};

const WarningPrimary = Template.bind({});
WarningPrimary.args = {
  size: 's',
  buttonType: 'primary',
  variant: 'warning',
  label: 'Label',
  leadingIcon: <Circle />,
};

const SuccessPrimary = Template.bind({});
SuccessPrimary.args = {
  size: 's',
  buttonType: 'primary',
  label: 'Label',
  variant: 'success',
  leadingIcon: <Circle />,
};

const Disabled = Template.bind({});
Disabled.args = {
  size: 's',
  buttonType: 'primary',
  label: 'Label',
  variant: 'success',
  leadingIcon: <Circle />,
  disabled: true,
};

export { DangerPrimary, Disabled, MainPrimary, MinorPrimary, SuccessPrimary, WarningPrimary };
