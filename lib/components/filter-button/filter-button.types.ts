import { ReactNode } from 'react';
import { ChipProps } from '../chip/chip.types';

/**
 * Props of FilerButton component.
 *
 * @author Rostyslav Nahornyi
 */
type FilterButtonProps = {
  /**
   * The ID of the icon to be displayed
   */
  readonly icon: ReactNode;

  /**
   * Callback function to be invoked when button is clicked.
   */
  readonly onClick: () => void;

  /**
   * Optional props for additional chip.
   */
  readonly chip?: Required<Pick<ChipProps, 'label' | 'onClick'>>;

  /**
   * Disabled state.
   * By default is false.
   */
  readonly disabled?: boolean;
};

export type { FilterButtonProps };
