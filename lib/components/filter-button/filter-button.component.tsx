import { Chip } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './filter-button.module.scss';
import { FilterButtonProps } from './filter-button.types';

/**
 * Filter Button component
 * @description Opens FilterPanel as rule.
 *
 * @author Rostyslav Nahornyi
 * @category Components
 * @param { FilterButtonProps } props - FilterButton defined in the './filter-button.types.ts'
 */
const FilterButton: FC<FilterButtonProps> = ({ icon, chip, disabled = false, onClick }) => (
  <div
    className={classNames(styles.filterButtonWrapper, disabled && styles.disabled)}
    role="button"
    tabIndex={0}
    onClick={disabled ? undefined : onClick}
    onKeyDown={({ code }) => code === 'Enter' && !disabled && onClick()}
  >
    {icon && <div className={styles.icon}>{icon}</div>}
    {chip && (
      <Chip
        emphasis="lowColored"
        size="m"
        variant="closable"
        disabled={disabled}
        {...chip}
      />
    )}
  </div>
);

export { FilterButton };
