import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { FilterButton } from './filter-button.component';

export default {
  title: 'UI-KIT/FilterButton',
  component: FilterButton,
} as Meta<typeof FilterButton>;

const Template: StoryFn<typeof FilterButton> = (props) => <FilterButton {...props} />;

const WithoutChip = Template.bind({});
WithoutChip.args = {
  icon: 'circle',
  onClick: () => action('filter button click'),
};

const WithChip = Template.bind({});
WithChip.args = {
  icon: 'circle',
  chip: { label: 'Text', onClick: action('chip click') },
  onClick: () => action('filter button click'),
};

export { WithChip, WithoutChip };
