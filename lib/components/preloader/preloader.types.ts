/**
 * @description Props for the Preloader component
 *
 * @author Vitalii Bodnarchuk
 */
type PreloaderProps = {
  /**
   * Is preloader active
   */
  readonly isActive: boolean;

  /**
   * Specifies preloader type.
   * By default is 'overlay'
   */
  readonly type: 'overlay' | 'static';

  /**
   * Specifies size of the loader.
   */
  readonly size: 'large' | 'small';
  /**
   * Loading text
   */
  readonly text?: string;
  /**
   * Class Name
   */
  readonly className?: string;
};

export type { PreloaderProps };
