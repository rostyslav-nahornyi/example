import PreloaderImg from '@assets/images/preloader.png';
import { Text } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './preloader.module.scss';
import { PreloaderProps } from './preloader.types';

/**
 * Preloader component
 * @description Preloader component which indicates data loading/processing
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 */
const Preloader: FC<PreloaderProps> = ({ isActive, type, text, size, className }) => {
  return isActive ? (
    <>
      {type === 'overlay' ? (
        <div className={styles.overlay}>
          <img
            src={PreloaderImg}
            alt={'preloader'}
            className={classNames(styles[size], className)}
          />
          {text && (
            <Text
              variant={size === 'large' ? 'caption20Medium' : 'caption14Medium'}
              className={text}
            >
              {text}
            </Text>
          )}
        </div>
      ) : (
        <img
          src={PreloaderImg}
          alt={'preloader'}
          className={classNames(styles[size], className)}
        />
      )}
    </>
  ) : null;
};

export { Preloader };
