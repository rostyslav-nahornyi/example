import { Meta, StoryFn } from '@storybook/react';

import { Preloader } from './preloader.component';

export default {
  title: 'UI-KIT/Preloader',
  component: Preloader,
  decorators: [(Story) => <Story />],
} as Meta<typeof Preloader>;

const Template: StoryFn<typeof Preloader> = (props) => <Preloader {...props} />;

const Overlay = Template.bind({});
Overlay.args = {
  isActive: true,
  type: 'overlay',
  size: 'large',
};

const StaticSmall = Template.bind({});
StaticSmall.args = {
  isActive: true,
  type: 'static',
  size: 'small',
};

const StaticLarge = Template.bind({});
StaticLarge.args = {
  isActive: true,
  type: 'static',
  size: 'large',
};

const OverlayLargeWithText = Template.bind({});
OverlayLargeWithText.args = {
  ...Overlay.args,
  type: 'overlay',
  text: 'Loading text...',
};

const OverlaySmallWithText = Template.bind({});
OverlaySmallWithText.args = {
  ...Overlay.args,
  size: 'small',
  text: 'Loading text...',
};

export { Overlay, OverlayLargeWithText, OverlaySmallWithText, StaticLarge, StaticSmall };
