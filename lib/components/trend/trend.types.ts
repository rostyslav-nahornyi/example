/**
 * Props of Trend component.
 *
 * @author Vitalii Bodnarchuk
 */

type TrendProps = {
  /**
   * Text.
   */
  readonly text: string;

  /**
   * Icon of the trend.
   */
  readonly icon?: boolean;
  /**
   * Variant
   */
  readonly variant: 'statics' | 'positive' | 'negative';
};

export type { TrendProps };
