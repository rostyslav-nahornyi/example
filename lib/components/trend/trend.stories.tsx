import { Meta, StoryFn } from '@storybook/react';

import { Trend } from './trend.component';

export default {
  title: 'UI-KIT/Trend',
  component: Trend,
} as Meta<typeof Trend>;

const Template: StoryFn<typeof Trend> = (props) => <Trend {...props} />;

const StaticsWithIcon = Template.bind({});
StaticsWithIcon.args = {
  variant: 'statics',
  text: 'Label',
  icon: true,
};

const StaticsWithoutIcon = Template.bind({});
StaticsWithoutIcon.args = {
  variant: 'statics',
  text: 'Label',
};

const PositiveWithIcon = Template.bind({});
PositiveWithIcon.args = {
  variant: 'positive',
  text: 'Label',
  icon: true,
};

const PositiveWithoutIcon = Template.bind({});
PositiveWithoutIcon.args = {
  variant: 'positive',
  text: 'Label',
};

const NegativeWithIcon = Template.bind({});
NegativeWithIcon.args = {
  variant: 'negative',
  text: 'Label',
  icon: true,
};

const NegativeWithoutIcon = Template.bind({});
NegativeWithoutIcon.args = {
  variant: 'negative',
  text: 'Label',
};

export {
  NegativeWithIcon,
  NegativeWithoutIcon,
  PositiveWithIcon,
  PositiveWithoutIcon,
  StaticsWithIcon,
  StaticsWithoutIcon,
};
