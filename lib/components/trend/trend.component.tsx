import DownArrowAlt from '@assets/icons/down-arrow-alt.svg';
import Minus from '@assets/icons/minus.svg';
import UpArrowAlt from '@assets/icons/up-arrow-alt.svg';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './trend.module.scss';
import { TrendProps } from './trend.types';

/**
 * Trend component
 * @description Trend
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TrendProps } props - TrendProps defined in the './trend.types.ts'
 */
const Trend: FC<TrendProps> = ({ text, icon, variant = 'statics' }) => {
  const iconComponentsMap = {
    positive: <UpArrowAlt />,
    negative: <DownArrowAlt />,
    statics: <Minus />,
  };

  return (
    <div className={classNames(styles.trend, styles[variant])}>
      {icon && <span className={styles.icon}>{iconComponentsMap[variant] ?? <Minus />}</span>}
      <p>{text}</p>
    </div>
  );
};

export { Trend };
