import { FC, useCallback, useEffect, useRef, useState } from 'react';

import ChevronDown from '@assets/icons/chevron-down.svg';
import { Chip, DropdownMenu, Text } from '@components';
import { DropdownMenuProps } from '@components/dropdown/components/dropdown-menu/dropdown-menu.types';
import { useClickOutside } from '@hooks';
import classNames from 'classnames';
import styles from './chip-field.module.scss';
import { ChipFieldProps } from './chip-field.types';

/**
 * Chip Field component
 * @description Input of chips.
 *
 * @author Rostyslav Nahornyi
 * @category Components
 * @param { ChipFieldProps } props - ChipField defined in the './chip-field.types.ts'
 */

const ChipField: FC<ChipFieldProps> = ({
  options,
  placeholder,
  size,
  disabled,
  label,
  dropdownMenu,
  name,
  values,
  onChange,
  onAdd,
  onRemove,
}) => {
  /**
   * STATE
   */
  const [isDropdownOpened, setIsDropdownOpened] = useState(false);
  const [isInputFocused, setIsInputFocused] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [filteredOptions, setFilteredOptions] = useState<typeof options>(options);

  /**
   * REFS
   */
  const inputRef = useRef<HTMLInputElement>(null);
  const referenceElement = useRef<HTMLDivElement | null>(null);
  const menuRef = useRef<{ updatePopperPosition: () => void } | null>(null);

  /**
   * OTHER
   */
  useClickOutside(referenceElement, () => setIsDropdownOpened(false));

  /**
   * LIFECYCLE
   */
  useEffect(() => {
    if (!inputValue) return setFilteredOptions(options);

    const searchedOptions = options?.filter(({ value }) =>
      value.toLowerCase().includes(inputValue.toLowerCase()),
    );
    setFilteredOptions(searchedOptions);
  }, [inputValue, options]);

  useEffect(() => {
    if (isInputFocused) {
      inputRef.current?.focus();
    } else {
      inputRef.current?.blur();
    }
  }, [isInputFocused]);

  useEffect(() => {
    if (values && onChange) {
      onChange(values);
      const withSelectionOptions = filteredOptions.map((option) => ({
        ...option,
        isSelected: values.includes(option.id),
      }));

      setFilteredOptions(withSelectionOptions);
    }

    // Reason: breaks logic of rendering formik that produces to infinity rerenders
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values]);

  useEffect(() => {
    setFilteredOptions(options);
  }, [options]);

  useEffect(() => {
    setTimeout(() => {
      updatePopper();
    }, 500);
  }, []);

  /**
   * HANDLERS
   */
  const updatePopper = () => {
    if (!menuRef.current?.updatePopperPosition) return;

    menuRef.current.updatePopperPosition();
  };

  const onSelectChipHandler: DropdownMenuProps['onSelect'] = (optionId) => {
    if (values?.includes(optionId)) {
      onChange && onChange(values.filter((value) => optionId !== value));
      onRemove && onRemove(optionId);
    } else {
      onChange && onChange(values ? [optionId, ...values] : [optionId]);
      onAdd && onAdd(optionId);
    }

    updatePopper();
    setInputValue('');
  };

  const onRemoveChipHandler = useCallback(
    (id: string) => {
      const ids = values?.filter((selectedId) => selectedId !== id);

      if (onChange && ids) {
        onChange(ids);
      }

      onRemove && onRemove(id);

      updatePopper();
    },
    [values, onChange, onRemove],
  );

  return (
    <div
      className={classNames(styles.chipFieldWrapper, styles[`size-${size}`])}
      ref={referenceElement}
    >
      {label && (
        <Text
          variant="caption14Medium"
          className={styles.label}
        >
          {label}
        </Text>
      )}
      <div
        className={classNames(
          styles.inputWrapper,
          isDropdownOpened && styles.focus,
          disabled && styles.disabled,
        )}
        role="button"
        tabIndex={0}
        onKeyDown={(e) => e.code === 'Enter' && !disabled && setIsDropdownOpened(!isDropdownOpened)}
        onFocus={() => {
          if (disabled) return;

          setIsInputFocused(true);
        }}
        onClick={() => {
          if (isDropdownOpened || disabled) return;

          setIsDropdownOpened(true);
        }}
        onBlur={() => setIsInputFocused(false)}
      >
        <div className={styles.main}>
          {placeholder && (
            <input
              ref={inputRef}
              type="text"
              name={name}
              placeholder={placeholder}
              value={inputValue}
              className={styles.input}
              disabled={disabled}
              onChange={({ target: { value } }) => setInputValue(value)}
            />
          )}

          {!!values?.length && (
            <div className={styles.chips}>
              {values?.map((selectedId) => {
                const props = options?.find(({ id }) => selectedId === id);

                return props ? (
                  <Chip
                    key={selectedId}
                    isRectangle
                    emphasis="low"
                    size="l"
                    variant="closable"
                    disabled={disabled}
                    label={props?.value}
                    icon={props?.leadingIcon}
                    tabIndex={isDropdownOpened ? -1 : 0}
                    onClick={() => onRemoveChipHandler(selectedId)}
                  />
                ) : (
                  <></>
                );
              })}
            </div>
          )}
        </div>

        <div className={classNames(styles.iconWrapper, values?.length && styles.itemsExist)}>
          <ChevronDown
            className={classNames(styles.icon, isDropdownOpened && styles.reversed)}
            onClick={() => !disabled && setIsDropdownOpened(!isDropdownOpened)}
          />
        </div>
      </div>

      <DropdownMenu
        updatePopper={(updatePopperPosition) => (menuRef.current = { updatePopperPosition })}
        menuRef={referenceElement.current}
        multiple
        isOpened={isDropdownOpened}
        options={filteredOptions}
        hideScrollbar={true}
        className={styles.dropdownMenu}
        onSelect={onSelectChipHandler}
        {...dropdownMenu}
      />
    </div>
  );
};

ChipField.displayName = 'ChipField';

export { ChipField };
