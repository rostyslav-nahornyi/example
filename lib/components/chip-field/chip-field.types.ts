import { DropdownItemProps } from '../dropdown/components/dropdown-menu/components/dropdown-item/dropdown-item.types';
import { DropdownMenuProps } from '../dropdown/components/dropdown-menu/dropdown-menu.types';

/**
 * Represents the type of each option in the ChipField component.
 */
type OptionsType = ReadonlyArray<
  Pick<
    DropdownItemProps,
    'id' | 'value' | 'subText' | 'leadingIcon' | 'isSelected' | 'isAvatar' | 'isHidden'
  >
>;

/**
 * Props of ChipField component.
 *
 * @author Rostyslav Nahornyi
 */
type ChipFieldProps = {
  /**
   * The label for the ChipField.
   */
  readonly label?: string;

  /**
   * The size of the ChipField, either "small" or "large".
   */
  readonly size: 'small' | 'large';

  /**
   * The placeholder for the input.
   */
  readonly placeholder?: string;

  /**
   * The array of options.
   */
  readonly options: OptionsType;

  /**
   * The array of selected values.
   */
  readonly values?: Array<string>;

  /**
   * The dropdown menu props.
   */
  readonly dropdownMenu?: Pick<
    DropdownMenuProps,
    'className' | 'style' | 'maxHeight' | 'hideScrollbar' | 'placement'
  >;

  /**
   * A flag indicating whether the ChipField is disabled.
   */
  readonly disabled?: boolean;

  /**
   * The callback function triggered when values change.
   */
  readonly onChange?: (values: Array<string>) => void;

  /**
   * On item add callback
   */
  readonly onAdd?: (id: string) => void;

  /**
   * On item remove callback
   */
  readonly onRemove?: (id: string) => void;

  /**
   * The name for formik FieldArray
   */
  readonly name?: string;
};

export type { ChipFieldProps, OptionsType };
