import AvatarIcon from '@assets/icons/avatar.svg';
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';
import { ChipField } from './chip-field.component';
import { ChipFieldProps } from './chip-field.types';

export default {
  title: 'UI-KIT/ChipField',
  component: ChipField,
} as Meta<typeof ChipField>;

const Template: StoryFn<ChipFieldProps> = (props: ChipFieldProps) => {
  const [values, setValues] = useState<Array<string>>(['1', '2', '3']);
  const [options, setOptions] = useState(props.options);

  const clearOptions = () => {
    setOptions([]);
  };

  return (
    <>
      {JSON.stringify(values)}
      <button onClick={clearOptions}>Skip</button>
      <ChipField
        key={options.length}
        {...props}
        options={options}
        values={values}
        onChange={(values) => {
          setValues(values);
        }}
      />
    </>
  );
};

const WithPlaceholder = Template.bind({});
WithPlaceholder.args = {
  label: 'Label',
  placeholder: 'Placeholder text',
  options: [
    { id: '1', value: 'Text 1' },
    { id: '2', value: 'Text 2' },
    {
      id: '3',
      value: 'Text 3',
      isAvatar: true,
      leadingIcon: <AvatarIcon />,
    },
    { id: '4', value: 'Text 4' },
    { id: '5', value: 'Text 5' },
    { id: '6', value: 'Text 6' },
    { id: '7', value: 'Text 7' },
    { id: '8', value: 'Text 8' },
    { id: '9', value: 'Text 9' },
  ],
  size: 'small',
  dropdownMenu: { maxHeight: 150 },
};

const WithoutPlaceholder = Template.bind({});
WithoutPlaceholder.args = {
  ...WithPlaceholder.args,
  placeholder: undefined,
};

export { WithPlaceholder, WithoutPlaceholder };
