import { Meta, StoryObj } from '@storybook/react';
import { Indicator } from './indicator.component';
import { IndicatorProps } from './indicator.types';

export default {
  title: 'UI-KIT/Indicator',
  component: Indicator,
} as Meta<typeof Indicator>;

const Default: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'default',
  },
};

const Success: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'success',
    stroke: true,
  },
};

const Warning: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'warning',
  },
};

const Danger: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'danger',
    stroke: true,
  },
};

const Brand: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'brand',
  },
};

const Pending: StoryObj<IndicatorProps> = {
  args: {
    size: 'xxl',
    type: 'pending',
    stroke: true,
  },
};

export { Brand, Danger, Default, Pending, Success, Warning };
