/**
 * Props of Indicator component.
 *
 * @author Vitalii Bodnarchuk
 */

import { HTMLAttributes, PropsWithChildren } from 'react';

interface IndicatorProps extends PropsWithChildren<HTMLAttributes<HTMLDivElement>> {
  /**
   * The size of the indicator
   */
  readonly size: 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';
  /**
   * Type of the indicator
   */
  readonly type: 'default' | 'success' | 'warning' | 'danger' | 'brand' | 'pending';

  /**
   * Boolean value specifies indicator stroke
   */
  readonly stroke?: boolean;
}

export type { IndicatorProps };
