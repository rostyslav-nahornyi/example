import { IndicatorProps } from './indicator.types';

import classNames from 'classnames';
import styles from './indicator.module.scss';

/**
 * Indicator component
 * @description Indicator
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { IndicatorTypes } props - IndicatorTypes defined in the './indicator.types.ts'
 */
const Indicator = ({ size, type, stroke, className, ...props }: IndicatorProps) => (
  <div
    className={classNames(
      stroke ? styles.outer : styles.inner,
      styles[size],
      styles[type],
      stroke && [styles.stroke],
      className,
    )}
    {...props}
  >
    {stroke && <div className={classNames(styles.inner, styles[size], styles[type])} />}
  </div>
);

export { Indicator };
