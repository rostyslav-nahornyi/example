import classNames from 'classnames';
import { FC } from 'react';
import styles from './badge.module.scss';
import { BadgeProps } from './badge.types';

/**
 * Button component
 * @description Badge
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { BadgeProps } props - BadgeProps defined in the './badge.types.ts'
 */
const Badge: FC<BadgeProps> = ({
  label,
  icon,
  size,
  emphasis,
  badgeType,
  className,
  isRectangle,
  title,
}) => {
  const isIconOnly = !label && icon;
  return (
    <div
      className={classNames(
        styles.badge,
        styles[size],
        styles[emphasis],
        styles[badgeType],
        isIconOnly && styles.iconOnly,
        isRectangle && styles.rectangle,
        className,
      )}
      title={title}
    >
      {icon && <span className={styles.icon}>{icon}</span>}
      {label && <div className={styles.label}>{label}</div>}
    </div>
  );
};

export { Badge };
