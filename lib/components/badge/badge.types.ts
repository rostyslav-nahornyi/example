import { HTMLAttributes, ReactNode } from 'react';
/**
 * Props of Badge component.
 *
 * @author Vitalii Bodnarchuk
 */
interface BadgeProps extends Pick<HTMLAttributes<HTMLDivElement>, 'className' | 'title'> {
  /**
   * Label.
   */
  readonly label?: string;

  /**
   * Specifies badge type.
   */
  readonly badgeType: 'default' | 'colored' | 'success' | 'warning' | 'error' | 'pending';

  /**
   *  Specifies badge size.
   */
  readonly size: 'small' | 'large';

  /**
   *  Specifies badge emphasis.
   */
  readonly emphasis: 'high' | 'low';

  /**
   * icon.
   */
  readonly icon?: ReactNode;

  /**
   * rectangle
   */
  readonly isRectangle?: boolean;
}

export type { BadgeProps };
