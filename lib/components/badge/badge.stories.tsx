import { Meta, StoryFn } from '@storybook/react';

import Circle from '@assets/icons/circle.svg';
import { Indicator } from '@components';
import { Badge } from './badge.component';
export default {
  title: 'UI-KIT/Badge',
  component: Badge,
} as Meta<typeof Badge>;

const Template: StoryFn<typeof Badge> = (props) => <Badge {...props} />;

// high emphasis

const HighEmphasis = Template.bind({});
HighEmphasis.args = {
  size: 'small',
  badgeType: 'default',
  label: 'Text',
  emphasis: 'high',
  icon: (
    <Indicator
      size="xs"
      type="default"
    />
  ),
};

const HighEmphasisColored = Template.bind({});
HighEmphasisColored.args = {
  size: 'small',
  badgeType: 'colored',
  label: 'Text',
  emphasis: 'high',
  isRectangle: true,
  icon: (
    <Indicator
      size="xs"
      type="brand"
    />
  ),
};

// low emphasis

const LowEmphasis = Template.bind({});
LowEmphasis.args = {
  size: 'small',
  badgeType: 'default',
  label: 'Text',
  emphasis: 'low',
  isRectangle: true,
  icon: <Circle />,
};

const LowEmphasisColored = Template.bind({});
LowEmphasisColored.args = {
  size: 'small',
  badgeType: 'colored',
  label: 'LowEmphasisColored',
  title: 'title',
  emphasis: 'low',
  icon: <Circle />,
};

export { HighEmphasis, HighEmphasisColored, LowEmphasis, LowEmphasisColored };
