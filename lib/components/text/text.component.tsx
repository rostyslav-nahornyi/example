import { FC } from 'react';

import classNames from 'classnames';
import styles from './text.module.scss';
import { TextProps } from './text.types';

/**
 * Typography.Text component
 * @description component which render text
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TextProps } props - text defined in the './text.types.ts'
 */
const Text: FC<TextProps> = ({ variant, children, className, title }) => (
  <p
    className={classNames(styles[variant], className)}
    title={title}
  >
    {children}
  </p>
);
export { Text };
