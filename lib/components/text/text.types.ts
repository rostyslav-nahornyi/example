import { HTMLAttributes, PropsWithChildren } from 'react';

/**
 * TextProps
 * @description Props for the Typography.Text component
 *
 * @author Vitalii Bodnarchuk
 */
interface TextProps
  extends PropsWithChildren,
    Pick<HTMLAttributes<HTMLDivElement>, 'className' | 'title'> {
  /**
   * Variant of the text
   */
  readonly variant:
    | 'heading128Bold'
    | 'heading96Bold'
    | 'heading64Bold'
    | 'heading36Semibold'
    | 'heading20Semibold'
    | 'caption56Medium'
    | 'caption40Medium'
    | 'caption32Bold'
    | 'caption24Semibold'
    | 'caption24Medium'
    | 'caption22Medium'
    | 'caption20Medium'
    | 'caption18Medium'
    | 'caption18Semibold'
    | 'caption16Bold'
    | 'caption16Medium'
    | 'caption16Semibold'
    | 'caption14Semibold'
    | 'caption14Medium'
    | 'caption14Regular'
    | 'caption12Medium'
    | 'caption12Regular'
    | 'body16Medium'
    | 'body16Regular'
    | 'body14Medium'
    | 'body14Regular'
    | 'body12Regular'
    | 'label20Medium'
    | 'label18Medium'
    | 'label16Medium'
    | 'label14Medium'
    | 'link20Medium'
    | 'link18Medium'
    | 'link16Medium'
    | 'link14Medium';
}

export type { TextProps };
