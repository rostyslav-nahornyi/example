import InfoCircle from '@assets/icons/info-circle.svg';
import { Meta, Story } from '@storybook/react';
import { useState } from 'react';
import { usePopper } from 'react-popper';
import { Tooltip } from './tooltip.component';
import styles from './tooltip.module.scss';
import { TooltipProps } from './tooltip.types';

export default {
  title: 'UI-KIT/Tooltip',
  component: Tooltip,
} as Meta;

const TooltipTemplate: Story<TooltipProps> = (args) => {
  const [referenceElement, setReferenceElement] = useState<HTMLButtonElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);
  const [arrowElement, setArrowElement] = useState<HTMLDivElement | null>(null);
  const [isTooltipVisible, setIsTooltipVisible] = useState<boolean>(false);

  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [
      {
        name: 'arrow',
        options: {
          element: arrowElement,
          padding: 4,
        },
      },
      {
        name: 'offset',
        options: {
          offset: [0, 8],
        },
      },
    ],
    strategy: 'absolute',
  });
  return (
    <>
      <button
        style={{ border: 'none', background: 'inherit' }}
        ref={setReferenceElement}
        onMouseEnter={() => setIsTooltipVisible(true)}
        onMouseLeave={() => setIsTooltipVisible(true)}
      >
        <InfoCircle />
      </button>
      {isTooltipVisible && (
        <Tooltip
          {...args}
          arrow={args.arrow !== null ? { style: styles.arrow, hostRef: setArrowElement } : null}
          hostRef={setPopperElement}
          style={styles.popper}
          {...attributes.popper}
        />
      )}
    </>
  );
};

const centerDecorator = (Story: Story) => (
  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
    <Story />
  </div>
);

const Basic = TooltipTemplate.bind({});
Basic.args = {
  children: <div>Tooltip title goes here</div>,
};

const BasicDark = TooltipTemplate.bind({});
BasicDark.args = {
  theme: 'dark',
  children: <div className={styles.tooltipDark}>Tooltip title goes here</div>,
};

const Primary = TooltipTemplate.bind({});
Primary.args = {
  children: (
    <div className={styles.primaryBlock}>
      <p className={styles.title}>Tooltip title goes here</p>
      <p className={styles.text}>
        Tooltips are text labels that appear when the user hovers over, focuses on, or touches an
        element. Tooltips identify an element when they are activated.
      </p>
    </div>
  ),
};

const PrimaryDark = TooltipTemplate.bind({});
PrimaryDark.args = {
  theme: 'dark',
  children: (
    <div className={styles.primaryDarkBlock}>
      <p className={styles.title}>Tooltip title goes here</p>
      <p className={styles.text}>
        Tooltips are text labels that appear when the user hovers over, focuses on, or touches an
        element. Tooltips identify an element when they are activated.
      </p>
    </div>
  ),
};

const PrimaryTop = TooltipTemplate.bind({});
PrimaryTop.args = {
  theme: 'dark',
  children: (
    <div className={styles.primaryDarkBlock}>
      <p className={styles.title}>Tooltip title goes here</p>
      <p className={styles.text}>
        Tooltips are text labels that appear when the user hovers over, focuses on, or touches an
        element. Tooltips identify an element when they are activated.
      </p>
    </div>
  ),
};
PrimaryTop.decorators = [centerDecorator];

const NoArrow = TooltipTemplate.bind({});
NoArrow.args = {
  children: <div>Tooltip without arrow</div>,
  arrow: null,
};

export { Basic, BasicDark, NoArrow, Primary, PrimaryDark, PrimaryTop };
