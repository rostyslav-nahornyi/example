import { FC } from 'react';

import classNames from 'classnames';
import styles from './tooltip.module.scss';
import { TooltipProps } from './tooltip.types';

/**
 * Tooltip component
 * @description Tooltip
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TooltipProps } props - TooltipProps defined in the './tooltip.types.ts'
 */
const Tooltip: FC<TooltipProps> = ({
  className,
  zIndex,
  children,
  arrow,
  style,
  hostRef,
  theme = 'white',
  ...props
}) => (
  <div
    role="tooltip"
    className={classNames(styles.host, className)}
    style={{ ...style, zIndex }}
    ref={hostRef}
    {...props}
  >
    {arrow && (
      <div
        className={classNames(
          theme === 'dark' ? styles.arrowDark : styles.arrowLight,
          arrow && styles.arrow,
        )}
        style={arrow?.style}
        ref={arrow.hostRef}
        data-popper-arrow
      />
    )}

    <div className={classNames(styles.content, theme === 'dark' && styles.contentDark)}>
      {children}
    </div>
  </div>
);

Tooltip.displayName = 'Tooltip';

export { Tooltip };
