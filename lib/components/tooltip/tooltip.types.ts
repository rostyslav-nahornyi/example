import { ComponentProps } from 'react';

/**
 * Props of Tooltip component.
 *
 * @author Vitalii Bodnarchuk
 */

type TooltipProps = {
  /**
   * z-index, default value = 60
   */
  readonly zIndex?: number;

  /**
   * tooltip theme
   */
  readonly theme?: 'white' | 'dark';

  /**
   * arrow of the tooltip
   */
  readonly arrow?: {
    readonly style?: React.CSSProperties;
    readonly hostRef?: React.Ref<HTMLDivElement>;
  } | null;
  /**
   * hostRef
   */
  readonly hostRef?: React.Ref<HTMLDivElement>;
} & ComponentProps<'div'>;

export type { TooltipProps };
