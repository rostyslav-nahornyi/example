import { HTMLAttributes, ReactNode } from 'react';

import { ToolsMenuProps } from './components/tools-menu.types';

/**
 * Props of NavTab component.
 *
 * @author Sergii Goncharuk, Vitalii Bodnarchuk
 */
interface NavTabProps extends Pick<HTMLAttributes<HTMLDivElement>, 'style' | 'className'> {
  /** Icon */
  readonly icon: ReactNode;

  /** Indicator */
  readonly indicator?: boolean;

  /** Is tab active? */
  readonly isActive?: boolean;

  /** Is disabled? */
  readonly disabled?: boolean;

  /** Item of tools menu*/
  readonly items?: ToolsMenuProps['items'];
}

export type { NavTabProps };
