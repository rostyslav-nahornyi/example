import { FC, useRef, useState } from 'react';

import { NavTabProps } from './nav-tab.types';

import { useClickOutside } from '@hooks';
import classNames from 'classnames';
import { ToolsMenu } from './components';
import styles from './nav-tab.module.scss';

/**
 * NavTab component
 * @description Navigation Bar component
 *
 * @author Sergii Goncharuk, Vitalii Bodnarchuk
 * @category Components
 * @param { NavTabProps } props - NavTabProps defined in the './nav-tab.types.ts'
 */
const NavTab: FC<NavTabProps> = ({
  icon,
  isActive = false,
  indicator = false,
  disabled = false,
  items,
}) => {
  const [isOptionsMenuOpen, setIsMenuOptionsOpen] = useState<boolean>(false);
  const toggleOptionsMenu = () => setIsMenuOptionsOpen((prevState) => !prevState);
  const ref = useRef(null);

  useClickOutside(ref, () => setIsMenuOptionsOpen(false));

  return (
    <div ref={ref}>
      <button
        onClick={() => {
          toggleOptionsMenu();
        }}
        className={classNames(
          styles.tab,
          isActive && styles.current,
          disabled && styles.disabled,
          indicator && styles.indicator,
        )}
      >
        <div className={styles.icon}>{icon}</div>
      </button>
      {isOptionsMenuOpen && !!items && (
        <ToolsMenu
          items={items}
          refElement={ref.current}
        />
      )}
    </div>
  );
};

export { NavTab };
