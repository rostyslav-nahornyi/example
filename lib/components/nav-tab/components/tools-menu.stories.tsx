import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { ToolsMenu } from './tools-menu.component';

export default {
  title: 'UI-KIT/NavTab/ToolsMenu',
  component: ToolsMenu,
} as ComponentMeta<typeof ToolsMenu>;

const Template: ComponentStory<typeof ToolsMenu> = (props) => <ToolsMenu {...props} />;

const Default = Template.bind({});
Default.args = {
  items: [
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click1'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click2'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click3'),
    },
  ],
};

export { Default };
