import { FC, useState } from 'react';
import { usePopper } from 'react-popper';

import { Text } from '@components';
import styles from './tools-menu.module.scss';
import { ToolsMenuProps } from './tools-menu.types';
/**
 * ToolsMenu component
 * @description ToolsMenu component exclusively for NavTab
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { ToolsMenuProps } props - ToolsMenuProps defined in the './tools-menu.types.ts'
 */
const ToolsMenu: FC<ToolsMenuProps> = ({ items, refElement }) => {
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);

  const { styles: popperStyles, attributes } = usePopper(refElement, popperElement, {
    placement: 'bottom-start',
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 8],
        },
      },
      {
        name: 'preventOverflow',
        options: {
          boundary: 'clippingParents',
          padding: 0,
        },
      },
    ],
  });

  return (
    <div
      ref={setPopperElement}
      style={{ ...popperStyles.popper, zIndex: 30 }}
      {...attributes.popper}
    >
      <div className={styles.tool}>
        {items.map(({ onClick, text, icon }, idx) => (
          <button
            key={idx}
            className={styles.block}
            onClick={onClick}
            tabIndex={0}
          >
            <div className={styles.icon}>{icon}</div>
            <Text
              variant="caption14Medium"
              className={styles.text}
            >
              {text}
            </Text>
          </button>
        ))}
      </div>
    </div>
  );
};

export { ToolsMenu };
