import { ReactNode } from 'react';

/**
 * Props of ToolsMenu component.
 *
 * @author Vitalii Bodnarchuk
 */
type ToolsMenuItem = {
  /** Text of items */
  readonly text: string;

  /** Icon of items */
  readonly icon: ReactNode;

  /** Function of item */
  readonly onClick?: () => void;
};

type ToolsMenuProps = {
  /** Array of items */
  readonly items: ReadonlyArray<ToolsMenuItem>;

  /** Ref element */
  readonly refElement: HTMLElement | null;
};

export type { ToolsMenuItem, ToolsMenuProps };
