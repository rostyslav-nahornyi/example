import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { ComponentMeta, ComponentStory, Story } from '@storybook/react';
import { NavTab } from './nav-tab.component';

export default {
  title: 'UI-KIT/NavTab',
  component: NavTab,
} as ComponentMeta<typeof NavTab>;

const withPadding = (Story: Story) => (
  <div style={{ display: 'flex', justifyContent: 'right' }}>
    <Story />
  </div>
);

const Template: ComponentStory<typeof NavTab> = (props) => <NavTab {...props} />;

const Default = Template.bind({});
Default.args = {
  icon: <Circle />,
  items: [
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click1'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click2'),
    },
    {
      icon: <Circle />,
      text: 'Label',
      onClick: action('Click3'),
    },
  ],
};

const Current = Template.bind({});
Current.args = {
  ...Default.args,
  isActive: true,
  indicator: true,
};

const Disabled = Template.bind({});
Disabled.args = {
  icon: <Circle />,
  isActive: true,
  indicator: true,
  disabled: true,
};

const DefaultAlignmentLeft = Template.bind({});
DefaultAlignmentLeft.args = {
  ...Default.args,
};
DefaultAlignmentLeft.decorators = [withPadding];

export { Current, Default, DefaultAlignmentLeft, Disabled };
