import Circle from '@assets/icons/circle.svg';
import { Meta, Story } from '@storybook/react';
import { useState } from 'react';
import { TextArea } from './textarea.component';

export default {
  title: 'UI-KIT/TextArea',
  component: TextArea,
} as Meta;

const Template: Story = (args) => {
  const [value, setValue] = useState('');

  return (
    <TextArea
      {...args}
      value={value}
      onChange={setValue}
    />
  );
};

const Default = Template.bind({});
Default.args = {
  placeholder: 'Example text',
  label: 'Label',
  icon: <Circle />,
  helpText: 'Help text',
};

const Success = Template.bind({});
Success.args = {
  placeholder: 'Example text',
  type: 'success',
  label: 'Label',
  successText: 'Success Text',
  icon: <Circle />,
};

const Loading = Template.bind({});
Loading.args = {
  placeholder: 'Example text',
  type: 'loading',
  label: 'Label',
  helpText: 'Help text',
  icon: <Circle />,
};

const Error = Template.bind({});
Error.args = {
  placeholder: 'Example text',
  type: 'error',
  label: 'Label',
  errorText: 'Error Text',
  icon: <Circle />,
};

const Warning = Template.bind({});
Warning.args = {
  placeholder: 'Example text',
  type: 'warning',
  label: 'Label',
  warningText: 'Warning Text',
  icon: <Circle />,
};

export { Default, Error, Loading, Success, Warning };
