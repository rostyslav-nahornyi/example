import { ReactNode } from 'react';

/**
 * Props of TextArea component.
 *
 * @author Vitalii Bodnarchuk
 */
type TextAreaProps = {
  /**
   * Placeholder
   */
  readonly placeholder?: string;
  /**
   * Value of textArea
   */
  readonly value: string;
  /**
   * The callback function for writing value
   */
  onChange: (value: string) => void;
  /**
   * Label
   */
  readonly label?: string;
  /**
   * Type of textArea
   */
  readonly type?: 'default' | 'success' | 'loading' | 'warning' | 'error';
  /**
   * Help text
   */
  readonly helpText?: string;
  /**
   * Icon
   */
  readonly icon?: ReactNode;
  /**
   * Success text
   */
  readonly successText?: string;
  /**
   * Error text
   */
  readonly errorText?: string;
  /**
   * Warning text
   */
  readonly warningText?: string;
};

export type { TextAreaProps };
