import CheckCircle from '@assets/icons/check-circle.svg';
import ErrorAlt from '@assets/icons/error-alt.svg';
import styles from './textarea.module.scss';

import Error from '@assets/icons/error.svg';
import { Preloader } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import { Text } from '../text';
import { TextAreaProps } from './textarea.types';

/**
 * TextArea component
 * @description TextArea
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TextAreaTypes } props - TextAreaTypes defined in the './textarea.types.ts'
 */
const renderFeedback = (
  type: 'default' | 'success' | 'loading' | 'warning' | 'error',
  successText?: string,
  errorText?: string,
  warningText?: string,
) => {
  switch (type) {
    case 'success':
      return (
        <div className={classNames(styles.block, styles.success)}>
          <Text variant="caption12Regular">{successText}</Text>
          <CheckCircle />
        </div>
      );
    case 'error':
      return (
        <div className={classNames(styles.block, styles.error)}>
          <Text variant="caption12Regular">{errorText}</Text>
          <ErrorAlt />
        </div>
      );
    case 'warning':
      return (
        <div className={classNames(styles.block, styles.warning)}>
          <Text variant="caption12Regular">{warningText}</Text>
          <Error />
        </div>
      );
    default:
      return null;
  }
};

const TextArea: FC<TextAreaProps> = ({
  placeholder,
  value,
  onChange,
  label,
  helpText,
  type = 'default',
  successText,
  warningText,
  errorText,
  icon,
}) => (
  <div className={classNames(styles.wrapper)}>
    <div className={styles.block}>
      <Text
        variant="caption16Medium"
        className={styles.label}
      >
        {label}
      </Text>
      {icon && <div className={styles.icon}>{icon}</div>}
      {type === 'loading' && !icon && (
        <Preloader
          className={styles.preloader}
          isActive={true}
          size="small"
          type="static"
        />
      )}
    </div>
    <textarea
      className={classNames(styles.textArea, styles[type])}
      placeholder={placeholder}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
    {renderFeedback(type, successText, errorText, warningText)}
    <div className={styles.block}>
      <Text
        variant="caption12Regular"
        className={styles.helpText}
      >
        {helpText}
      </Text>
      {type === 'loading' && icon && (helpText || label) && (
        <Preloader
          className={styles.preloader}
          isActive={true}
          size="small"
          type="static"
        />
      )}
    </div>
  </div>
);

export { TextArea };
