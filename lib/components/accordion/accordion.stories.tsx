import { Meta, StoryFn } from '@storybook/react';

import { Text } from '@components';
import { Accordion } from './accordion.component';

export default {
  title: 'UI-KIT/Accordion',
  component: Accordion,
} as Meta<typeof Accordion>;

const Template: StoryFn<typeof Accordion> = (props) => <Accordion {...props} />;

const Default = Template.bind({});
Default.args = {
  title: 'Title',
  body: {
    content: <Text variant="body16Regular">Body example</Text>,
  },
};

const Disabled = Template.bind({});
Disabled.args = {
  ...Default.args,
  disabled: true,
};

export { Default, Disabled };
