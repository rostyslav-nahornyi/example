import { FC, useEffect, useState } from 'react';
import { AccordionBody, AccordionHeader } from './components';

import classNames from 'classnames';
import styles from './accordion.module.scss';
import { AccordionProps } from './accordion.types';

/**
 * Message component
 * @description Accordion which have two parts: header and body, after click on header body will be shown
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 */
const Accordion: FC<AccordionProps> = ({
  title,
  body,
  className,
  disabled = false,
  isExpanded = false,
  onToggleExpandState,
}) => {
  const [isAccordionExpanded, setIsAccordionExpanded] = useState<boolean>(!!isExpanded);

  const toggleExpand = () => {
    if (!disabled) {
      setIsAccordionExpanded(!isAccordionExpanded);
      onToggleExpandState && onToggleExpandState();
    }
  };

  useEffect(() => {
    setIsAccordionExpanded(!!isExpanded);
  }, [isExpanded]);

  return (
    <div
      className={classNames(className, styles.accordion, disabled && styles.disabled)}
      tabIndex={0}
      role="button"
    >
      <AccordionHeader
        title={title}
        isExpanded={isAccordionExpanded}
        onClick={toggleExpand}
      />
      {isAccordionExpanded && body && (
        <AccordionBody
          content={body.content}
          onClose={() => toggleExpand()}
        />
      )}
    </div>
  );
};

export { Accordion };
