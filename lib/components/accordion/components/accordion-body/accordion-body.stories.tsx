import { Text } from '@components';
import { Meta, StoryFn } from '@storybook/react';
import { AccordionBody } from './accordion-body.component';

export default {
  title: 'UI-KIT/Accordion/AccordionBody',
  component: AccordionBody,
} as Meta<typeof AccordionBody>;

const Template: StoryFn<typeof AccordionBody> = (props) => <AccordionBody {...props} />;

const Basic = Template.bind({});
Basic.args = {
  content: <Text variant="body16Regular">Body content</Text>,
};

export { Basic };
