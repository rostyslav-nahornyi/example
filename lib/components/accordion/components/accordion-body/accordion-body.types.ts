import { ReactNode } from 'react';
import { ChipProps } from '../../../chip/chip.types';
/**
 * Props of AccordionBody component
 *
 * @author Vitalii Bodnarchuk
 *
 */
type AccordionBodyProps = {
  readonly content: ReactNode;
  readonly onClose?: () => void;
};

type OmitRef<T> = Omit<T, 'ref'>;

type CommentsType = {
  /**
   * text
   */
  readonly text: string;
  /**
   * array of chips
   */
  readonly chip: OmitRef<ReadonlyArray<ChipProps>>;
};

export type { AccordionBodyProps, CommentsType, OmitRef };
