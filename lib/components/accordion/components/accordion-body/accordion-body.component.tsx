import classNames from 'classnames';
import { FC } from 'react';
import styles from './accordion-body.module.scss';
import { AccordionBodyProps } from './accordion-body.types';

/**
 * AccordionBody component
 * @description Body of accordion
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 */
const AccordionBody: FC<AccordionBodyProps> = ({ content }) => (
  <div className={classNames(styles.accordionBody)}>{content}</div>
);

export { AccordionBody };
