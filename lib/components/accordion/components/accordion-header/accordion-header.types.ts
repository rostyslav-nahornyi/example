/**
 * Props of AccordionHeader component
 *
 * @author Vitalii Bodnarchuk
 */
type AccordionHeaderProps = {
  /**
   * The label of according.
   */
  readonly title: string;

  /**
   * Specifies if the accordion is expanded
   */
  readonly isExpanded: boolean;

  /**
   * A callback function invoked on header click
   */
  readonly onClick: () => void;
};

export type { AccordionHeaderProps };
