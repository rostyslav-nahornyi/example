import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { AccordionHeader } from './accordion-header.component';

export default {
  title: 'UI-KIT/Accordion/AccordionHeader',
  component: AccordionHeader,
} as Meta<typeof AccordionHeader>;

const Template: StoryFn<typeof AccordionHeader> = (props) => <AccordionHeader {...props} />;

const Basic = Template.bind({});
Basic.args = {
  title: 'Example title',
  isExpanded: false,
  onClick: action('clicked on header'),
};

export { Basic };
