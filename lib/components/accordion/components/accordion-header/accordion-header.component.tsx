import ChevronDown from '@assets/icons/chevron-down.svg';
import { AccordionHeaderProps } from './accordion-header.types';

import { Text } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './accordion-header.module.scss';

/**
 * AccordionHeader component
 * @description Header of accordion
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 */
const AccordionHeader: FC<AccordionHeaderProps> = ({ title, isExpanded, onClick }) => (
  <div
    role={'button'}
    tabIndex={0}
    onClick={onClick}
    onKeyDown={onClick}
    className={classNames(
      styles.accordionHeader,
      isExpanded ? styles.expandedHeaderOpened : styles.expandedHeader,
    )}
  >
    <div className={styles.wrapperHeader}>
      <div className={styles.control}>
        <Text variant="caption16Semibold">{title}</Text>
        <ChevronDown className={classNames(styles.icon, isExpanded ? styles.on : styles.off)} />
      </div>
    </div>
  </div>
);

export { AccordionHeader };
