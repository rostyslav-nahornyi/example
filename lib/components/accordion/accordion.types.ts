import { BaseHTMLAttributes } from 'react';
import { AccordionBodyProps } from './components/accordion-body/accordion-body.types';
import { AccordionHeaderProps } from './components/accordion-header/accordion-header.types';

/**
 * Props for the Accordion component.
 *
 * @author Vitalii Bodnarchuk
 */
interface AccordionProps
  extends Pick<BaseHTMLAttributes<HTMLElement>, 'className'>,
    Omit<AccordionHeaderProps, 'onClick' | 'isExpanded'> {
  /**
   * Accordion ID
   */
  readonly id: string | number;

  /**
   * The body of accordion
   */
  readonly body?: AccordionBodyProps;

  /**
   * Specifies if the accordion is expanded depending on parent component
   * By default is false
   */
  readonly isExpanded?: boolean;

  /**
   * Specifies if the accordion is disabled depending on parent component
   * By default is false
   */
  readonly disabled?: boolean;

  /**
   * A callback function invoked on change expand state
   */
  readonly onToggleExpandState?: () => void;
}

export type { AccordionProps };
