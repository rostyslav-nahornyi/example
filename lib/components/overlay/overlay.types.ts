import { HTMLAttributes } from 'react';

/**
 * OverlayProps
 * @author Vitalii Bodnarchuk
 * @description Props for the Overlay component
 */
interface OverlayProps extends HTMLAttributes<HTMLDivElement> {
  /**
   * Toggle overlay
   */
  readonly isShown: boolean;

  /**
   * Blur effect intensity for the overlay background, specified in pixels.
   */
  readonly blur: number;

  /**
   * Callback function triggered on overlay click, typically used for closing the overlay.
   */
  readonly onOverlayClick: () => void;
}

export type { OverlayProps };
