import { FC } from 'react';
import { CSSTransition } from 'react-transition-group';
import { OverlayProps } from './overlay.types';

import classNames from 'classnames';
import styles from './overlay.module.scss';

/**
 * Overlay component
 * @description Window overlay
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { OverlayProps } props - OverlayProps defined in the './overlay.types.ts'
 */
const Overlay: FC<OverlayProps> = ({ isShown, onOverlayClick, blur, className }) => {
  if (!isShown) return null;

  return (
    <CSSTransition
      in={isShown}
      timeout={200}
      unmountOnExit
      classNames={styles}
    >
      <div
        tabIndex={0}
        role={'button'}
        style={{ backdropFilter: `blur(${blur}px)` }}
        className={classNames(styles.overlay, className)}
        onKeyDown={() => onOverlayClick()}
        onClick={() => onOverlayClick()}
      />
    </CSSTransition>
  );
};

export { Overlay };
