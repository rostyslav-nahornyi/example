import ChevronRight from '@assets/icons/Chevron_Right.svg';
import ChevronLeft from '@assets/icons/chevron-left.svg';
import ChevronsLeft from '@assets/icons/chevrons-left.svg';
import ChevronsRight from '@assets/icons/chevrons-right.svg';
import classNames from 'classnames';
import { FC } from 'react';
import { Button } from '../button';
import { usePaginationHook } from './pagination.hook';
import styles from './pagination.module.scss';
import { PaginationProps } from './pagination.types';

/**
 * Pagination component
 * @description Pagination
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { PaginationProps } props - PaginationProps defined in the './pagination.types.ts'
 */
const Pagination: FC<PaginationProps> = ({
  currentPage = 1,
  pageCount,
  setCurrentPage,
  type,
  buttonLabel,
  midLabel,
  className,
  pageLabel,
}) => {
  const { renderPageButtons } = usePaginationHook(pageCount, setCurrentPage, currentPage);

  const renderPaginationContent = () => {
    switch (type) {
      case 'fullRange':
        return (
          <>
            {currentPage !== 1 && (
              <span
                role={'button'}
                tabIndex={0}
                className={styles.chevronLeft}
                onClick={() => setCurrentPage(1)}
                onKeyDown={() => setCurrentPage(1)}
              >
                <ChevronsLeft />
              </span>
            )}
            <button
              tabIndex={0}
              className={classNames(styles.arrowLeft, currentPage === 1 && styles.disabled)}
              onClick={() => setCurrentPage(currentPage - 1)}
              onKeyDown={() => setCurrentPage(currentPage - 1)}
              disabled={currentPage === 1}
            >
              <ChevronLeft />
            </button>

            {renderPageButtons()}

            <button
              tabIndex={0}
              className={classNames(
                styles.arrowRight,
                currentPage === pageCount && styles.disabled,
              )}
              onClick={() => setCurrentPage(currentPage + 1)}
              onKeyDown={() => setCurrentPage(currentPage + 1)}
              disabled={currentPage === pageCount}
            >
              <ChevronRight />
            </button>

            {currentPage !== pageCount && (
              <button
                tabIndex={0}
                className={styles.arrowRight}
                onClick={() => setCurrentPage(pageCount)}
                onKeyDown={() => setCurrentPage(pageCount)}
              >
                <ChevronsRight />
              </button>
            )}
          </>
        );
      case 'centeredRange':
        return (
          <div className={styles.centeredRange}>
            <Button
              disabled={currentPage === 1}
              buttonType="borderless"
              variant="minor"
              size="m"
              leadingIcon={<ChevronLeft />}
              onClick={() => setCurrentPage(currentPage - 1)}
              onKeyDown={() => setCurrentPage(currentPage - 1)}
              className={styles.centeredButton}
              label={buttonLabel && buttonLabel[0]}
            />

            <div className={styles.pagination}>{renderPageButtons()}</div>

            <Button
              disabled={currentPage === pageCount}
              buttonType="borderless"
              variant="minor"
              size="m"
              trailingIcon={<ChevronRight />}
              onClick={() => setCurrentPage(currentPage + 1)}
              onKeyDown={() => setCurrentPage(currentPage + 1)}
              className={styles.centeredButton}
              label={buttonLabel && buttonLabel[1]}
            />
          </div>
        );
      case 'textualNavigation':
        return (
          <div className={styles.textualNavigation}>
            <Button
              disabled={currentPage === 1}
              buttonType="outlined"
              variant="minor"
              size="m"
              leadingIcon={<ChevronLeft />}
              onClick={() => setCurrentPage(currentPage - 1)}
              onKeyDown={() => setCurrentPage(currentPage - 1)}
              className={styles.centeredButton}
              label={buttonLabel && buttonLabel[0]}
            />

            <div className={styles.textualNavigationText}>
              {`${pageLabel} ${currentPage} ${midLabel} ${pageCount}`}
            </div>

            <Button
              disabled={currentPage === pageCount}
              buttonType="outlined"
              variant="minor"
              size="m"
              trailingIcon={<ChevronRight />}
              onClick={() => setCurrentPage(currentPage + 1)}
              onKeyDown={() => setCurrentPage(currentPage + 1)}
              className={classNames(styles.centeredButton, styles.textualNavigationButton)}
              label={buttonLabel && buttonLabel[1]}
            />
          </div>
        );
      case 'textualEdges':
        return (
          <div className={styles.textualNavigation}>
            <div className={styles.textualNavigationText}>
              Page {`${currentPage} ${midLabel} ${pageCount}`}
            </div>
            <div className={styles.buttons}>
              <Button
                disabled={currentPage === 1}
                buttonType="outlined"
                variant="minor"
                size="m"
                leadingIcon={<ChevronLeft />}
                onClick={() => setCurrentPage(currentPage - 1)}
                onKeyDown={() => setCurrentPage(currentPage - 1)}
                className={styles.centeredButton}
                label={buttonLabel && buttonLabel[0]}
              />

              <Button
                disabled={currentPage === pageCount}
                buttonType="outlined"
                variant="minor"
                size="m"
                trailingIcon={<ChevronRight />}
                onClick={() => setCurrentPage(currentPage + 1)}
                onKeyDown={() => setCurrentPage(currentPage + 1)}
                className={styles.centeredButton}
                label={buttonLabel && buttonLabel[1]}
              />
            </div>
          </div>
        );
      default:
        return null;
    }
  };

  return <div className={classNames(styles.wrapper, className)}>{renderPaginationContent()}</div>;
};

export { Pagination };
