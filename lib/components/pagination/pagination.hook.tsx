import DotsHorizontal from '@assets/icons/dots-horizontal.svg';
import classNames from 'classnames';
import { Button } from '../button';
import styles from './pagination.module.scss';
/**
 * usePaginationHook
 * @description logic for pagination
 *
 * @author Vitalii Bodnarchuk
 * @category Hooks
 */
const usePaginationHook = (
  pageCount: number,
  setCurrentPage: (page: number) => void,
  currentPage: number,
) => {
  const handleDotsClick = (direction: string) => {
    if (direction === 'right') {
      if (currentPage <= 3) {
        setCurrentPage(4);
      } else {
        setCurrentPage(Math.min(pageCount - 2, currentPage + 1));
      }
    } else {
      if (currentPage >= pageCount - 2) {
        setCurrentPage(pageCount - 3);
      } else {
        setCurrentPage(Math.max(1, currentPage - 1));
      }
    }
  };

  const renderPageButtons = () => {
    const buttons = [];
    // Always render the first three pages
    for (let i = 1; i <= 3; i++) {
      buttons.push(
        <Button
          key={i}
          className={classNames(styles.button, i === currentPage && styles.selectedButton)}
          onClick={() => setCurrentPage(i)}
          size="s"
          variant="main"
          buttonType="secondary"
        >
          {i}
        </Button>,
      );
    }

    // Add dots before the current page if it's more than 4
    if (currentPage > 4) {
      buttons.push(
        <span
          role="button"
          className={styles.dotsHorizontal}
          tabIndex={0}
          key="dots-before"
          onKeyDown={() => handleDotsClick('left')}
          onClick={() => handleDotsClick('left')}
        >
          <DotsHorizontal />
        </span>,
      );
    }

    // Render the current page if it's not the first or the last page
    if (currentPage > 3 && currentPage < pageCount - 2) {
      buttons.push(
        <Button
          key={currentPage}
          onClick={() => setCurrentPage(currentPage)}
          size="s"
          variant="main"
          buttonType="secondary"
          className={classNames(styles.button, styles.selectedButton)}
        >
          {currentPage}
        </Button>,
      );
    }

    // Add dots after the current page if it's less than pageCount - 3
    if (currentPage < pageCount - 3) {
      buttons.push(
        <span
          role="button"
          tabIndex={0}
          key="dots-after"
          className={styles.dotsHorizontal}
          onKeyDown={() => handleDotsClick('right')}
          onClick={() => handleDotsClick('right')}
        >
          <DotsHorizontal />
        </span>,
      );
    }

    // Always render the last three pages
    for (let i = pageCount - 2; i <= pageCount; i++) {
      buttons.push(
        <Button
          key={i}
          onClick={() => setCurrentPage(i)}
          size="s"
          variant="main"
          buttonType="secondary"
          className={classNames(styles.button, i === currentPage && styles.selectedButton)}
        >
          {i}
        </Button>,
      );
    }

    return buttons;
  };

  return { renderPageButtons };
};

export { usePaginationHook };
