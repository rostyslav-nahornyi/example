import { HTMLAttributes } from 'react';

/**
 * Props of Pagination component
 *
 * @author Vitalii Bodnarchuk
 */
interface PaginationProps extends HTMLAttributes<HTMLDivElement> {
  /**
   * selectedPage
   */
  readonly currentPage: number;
  /**
   * amount of all the pages
   */
  readonly pageCount: number;
  /**
   * function which change current page
   */
  readonly setCurrentPage: (page: number) => void;
  /**
   * type of pagination
   */
  readonly type: 'fullRange' | 'centeredRange' | 'textualNavigation' | 'textualEdges';

  /**
   * buttons label
   */
  readonly buttonLabel?: string[];

  /**
   * label between buttons
   */
  readonly midLabel?: string;

  /**
   * page label
   */
  readonly pageLabel?: string;
}

export type { PaginationProps };
