import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';
import { Pagination } from './pagination.component';

export default {
  title: 'UI-KIT/Pagination',
  component: Pagination,
} as Meta<typeof Pagination>;

const Template: StoryFn<typeof Pagination> = (args) => {
  const [currentPage, setCurrentPage] = useState(1);

  return (
    <Pagination
      {...args}
      currentPage={currentPage}
      setCurrentPage={setCurrentPage}
      midLabel={'/'}
      buttonLabel={['Previous', 'Next']}
      pageLabel={'Page'}
    />
  );
};

const FullRange = Template.bind({});
FullRange.args = {
  pageCount: 100,
  type: 'fullRange',
};

const CenteredRange = Template.bind({});
CenteredRange.args = {
  pageCount: 100,
  type: 'centeredRange',
};

const TextualNavigation = Template.bind({});
TextualNavigation.args = {
  pageCount: 100,
  type: 'textualNavigation',
};

const TextualEdges = Template.bind({});
TextualEdges.args = {
  pageCount: 100,
  type: 'textualEdges',
};

export { CenteredRange, FullRange, TextualEdges, TextualNavigation };
