import Circle from '@assets/icons/circle.svg';
import { TextField } from '@components/text-field/text-field.component';
import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

export default {
  title: 'UI-KIT/TextField',
  component: TextField,
} as Meta<typeof TextField>;

const Template: StoryFn<typeof TextField> = (props) => {
  const [value, setValue] = useState('');
  action(`input chanded: ${value}`);

  return (
    <TextField
      {...props}
      value={value}
      onChange={(value) => setValue(value)}
    />
  );
};

const Basic = Template.bind({});
Basic.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  leadingIcon: <Circle />,
  tailingIcon: <Circle />,
  helpText: 'Help text',
  type: 'text',
};

const WithError = Template.bind({});
WithError.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  error: 'Error text',
  shouldHideErrorMessageText: false,
  leadingIcon: <Circle />,
};

const Success = Template.bind({});
Success.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  variant: 'success',
  measure: 'small',
  success: 'Success text',
  leadingIcon: <Circle />,
};

const Warning = Template.bind({});
Warning.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  warning: 'Warning text',
  variant: 'warning',
  leadingIcon: <Circle />,
  shouldHideErrorMessageText: false,
};

const Loading = Template.bind({});
Loading.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  leadingIcon: <Circle />,
  variant: 'loading',
  helpText: 'Help text',
};

const TextFieldUrl = Template.bind({});
TextFieldUrl.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  type: 'url',
  measure: 'small',
  helpText: 'Help text',
};

const TextFieldUrlSuccess = Template.bind({});
TextFieldUrlSuccess.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  type: 'url',
  variant: 'success',
  success: 'Success text',
};

const TextFieldSelect = Template.bind({});
TextFieldSelect.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  tailingIcon: <Circle />,
  leadingIcon: <Circle />,
  helpText: 'Help text',
  type: 'select',
  options: [
    { value: 'option1', label: 'US' },
    { value: 'option2', label: 'UK' },
    { value: 'option3', label: 'EU' },
  ],
};

const TextFieldSelectWarning = Template.bind({});
TextFieldSelectWarning.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  leadingIcon: <Circle />,
  variant: 'warning',
  warning: 'Help text',
  type: 'select',
  options: [
    { value: 'option1', label: 'US' },
    { value: 'option2', label: 'UK' },
    { value: 'option3', label: 'EU' },
  ],
};

const Disabled = Template.bind({});
Disabled.args = {
  label: 'Label',
  id: 'inputElement',
  placeholder: 'Placeholder text',
  leadingIcon: <Circle />,
  disabled: true,
};

export {
  Basic,
  Disabled,
  Loading,
  Success,
  TextFieldSelect,
  TextFieldSelectWarning,
  TextFieldUrl,
  TextFieldUrlSuccess,
  Warning,
  WithError,
};
