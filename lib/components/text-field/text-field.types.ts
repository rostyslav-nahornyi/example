import { DetailedHTMLProps, InputHTMLAttributes, ReactNode } from 'react';
import { Option } from '../dropdown/dropdown.types';

/**
 * Props of TextField component.
 *
 * @author Vitalii Bodnarchuk
 */

type OmittedInputProps = Omit<
  DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'onChange' | 'onSelect' | 'placeholder' | 'defaultValue'
>;

type DropdownTextFieldProps = {
  /**
   * Callback function invoked when a select item is chosen.
   */
  onSelect?: (value: string | number) => void;

  /**
   * options for select
   */
  readonly options?: ReadonlyArray<Option>;

  /**
   * selected value
   */
  readonly selectedOptionValue?: string;
};

interface TextFieldProps extends OmittedInputProps, DropdownTextFieldProps {
  /**
   * Callback function invoked when user changes input's value.
   */
  onChange?: (value: string) => void;

  /**
   * 1. If provided - the label will be rendered.
   * 2. Specifies the text of the input's label.
   */
  readonly label?: string;

  /**
   * Default value.
   */
  readonly defaultValue?: string;

  /**
   * Textfield error message
   */
  readonly error?: string;

  /**
   * Textfield success message
   */
  readonly success?: string;

  /**
   * Textfield warning message
   */
  readonly warning?: string;

  /**
   * Input left element description
   */
  readonly leftDescription?: string;

  /**
   * Input right element
   */
  readonly rightDescription?: {
    readonly label: string;
    readonly onClick?: () => void;
  };

  /**
   * Specifies if the input error message should be hidden
   * By default is 'false'.
   */
  readonly shouldHideErrorMessageText?: boolean;

  /**
   * Tailing icon element to display in the input field.
   */
  readonly tailingIcon?: ReactNode;

  /**
   * Leading icon element to display on the left.
   */
  readonly leadingIcon?: ReactNode;

  /**
   * Placeholder
   */
  readonly placeholder?: string;

  /**
   * HelpText
   */
  readonly helpText?: string;

  /**
   * variant
   */
  readonly variant?: 'success' | 'loading' | 'warning';

  /**
   * type of textField
   */
  readonly type?: 'text' | 'url' | 'select';

  /**
   * size of textField
   */
  readonly measure?: 'large' | 'small';
}

export type { TextFieldProps };
