import { FC, useEffect, useRef, useState } from 'react';

import CheckCircle from '@assets/icons/check-circle.svg';
import ChevronDown from '@assets/icons/chevron-down.svg';
import ErrorAlt from '@assets/icons/error-alt.svg';
import Error from '@assets/icons/error.svg';
import { Preloader, Text } from '@components';
import { useClickOutside } from '@hooks';
import classNames from 'classnames';
import styles from './text-field.module.scss';
import { TextFieldProps } from './text-field.types';

/**
 * TextField component
 * @description TextField component
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TextFieldProps } props - TextFieldProps defined in the './text-field.types.ts'
 */
const TextField: FC<TextFieldProps> = ({
  onChange,
  defaultValue,
  error,
  tailingIcon,
  label,
  leadingIcon,
  shouldHideErrorMessageText = false,
  className,
  disabled,
  helpText,
  variant,
  success,
  type = 'text',
  options,
  warning,
  onSelect,
  measure = 'large',
  selectedOptionValue,
  ...inputProps
}) => {
  const [inputValue, setInputValue] = useState<string>(
    defaultValue ?? inputProps?.value?.toString() ?? '',
  );
  const [isOptionsMenuOpen, setIsMenuOptionsOpen] = useState<boolean>(false);
  const toggleOptionsMenu = () => setIsMenuOptionsOpen((prevState) => !prevState);

  const findSelectedOptionLabel = () =>
    options?.find(({ value }) => value === selectedOptionValue)?.label;

  const ref = useRef<HTMLDivElement | null>(null);

  useClickOutside(ref, () => setIsMenuOptionsOpen(false));

  useEffect(() => {
    setInputValue(inputProps?.value?.toString() ?? '');
  }, [inputProps.value]);

  useEffect(() => {
    defaultValue && setInputValue(defaultValue);
  }, [defaultValue]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    if (onChange) {
      onChange(event.target.value);
    }
  };

  const selectInputField = (
    <div
      className={classNames(
        styles.inputContainerWithPrefix,
        tailingIcon && styles.withTailingIcon,
        error && styles.errored,
        variant === 'success' && styles.success,
        variant === 'warning' && styles.warning,
      )}
    >
      <div className={styles.staticPrefix}>
        {leadingIcon && (
          <div className={classNames(styles.icon, styles.leadingIcon)}>{leadingIcon}</div>
        )}
        <div
          className={styles.dropdownElement}
          ref={ref}
        >
          {isOptionsMenuOpen && options && (
            <div className={styles.optionsMenu}>
              {!!options?.length &&
                options.map(({ value, label, disabled, onClick }, index) => (
                  <button
                    type={'button'}
                    tabIndex={0}
                    onClick={() => {
                      onSelect && onSelect(value);
                      onClick && onClick();
                      toggleOptionsMenu();
                    }}
                    className={classNames(
                      styles.optionItem,
                      selectedOptionValue === value && styles.selectedItem,
                    )}
                    key={String(value).concat(index.toString())}
                    disabled={disabled}
                  >
                    <span className={styles.optionItemLabel}>{label}</span>
                  </button>
                ))}
            </div>
          )}
          {options && (
            <button
              type={'button'}
              tabIndex={0}
              disabled={disabled}
              onClick={toggleOptionsMenu}
              className={classNames(
                styles.selector,
                isOptionsMenuOpen && styles.selectorWithOpenedMenu,
              )}
            >
              <span className={styles.selectedOptionLabel}>
                {findSelectedOptionLabel() ?? options[0]?.label}
              </span>
              {isOptionsMenuOpen ? (
                <ChevronDown
                  className={classNames(styles.icon, isOptionsMenuOpen ? styles.on : styles.off)}
                />
              ) : (
                <ChevronDown
                  className={classNames(styles.icon, isOptionsMenuOpen ? styles.on : styles.off)}
                />
              )}
            </button>
          )}
        </div>
      </div>
      <div
        className={classNames(
          styles.inputBlock,
          measure === 'large' ? styles.inputBlockLarge : styles.inputBlockSmall,
        )}
      >
        <input
          type="text"
          className={styles.inputElement}
          value={inputValue ?? ''}
          onChange={handleChange}
          disabled={disabled}
          {...inputProps}
        />
        {tailingIcon && (
          <div className={classNames(styles.icon, styles.tailingIcon)}>{tailingIcon}</div>
        )}
        {error && <ErrorAlt className={styles.icon} />}
        {variant === 'loading' && (
          <Preloader
            className={styles.icon}
            isActive={true}
            size="small"
            type="static"
          />
        )}
        {variant === 'success' && <CheckCircle className={styles.icon} />}
        {variant === 'warning' && <Error className={styles.icon} />}
      </div>
    </div>
  );

  const urlInputField = (
    <div
      className={classNames(
        styles.inputContainerWithPrefix,
        tailingIcon && styles.withTailingIcon,
        error && styles.errored,
        variant === 'success' && styles.success,
        variant === 'warning' && styles.warning,
      )}
    >
      <div className={styles.staticPrefix}>http://</div>
      <div
        className={classNames(
          styles.inputBlock,
          measure === 'large' ? styles.inputBlockLarge : styles.inputBlockSmall,
        )}
      >
        <input
          type="text"
          className={styles.inputElement}
          value={inputValue ?? ''}
          onChange={handleChange}
          disabled={disabled}
          {...inputProps}
        />
        {tailingIcon && (
          <div className={classNames(styles.icon, styles.tailingIcon)}>{tailingIcon}</div>
        )}
        {error && <ErrorAlt className={styles.icon} />}
        {variant === 'loading' && (
          <Preloader
            className={styles.icon}
            isActive={true}
            size="small"
            type="static"
          />
        )}
        {variant === 'success' && <CheckCircle className={styles.icon} />}
        {variant === 'warning' && <Error className={styles.icon} />}
      </div>
    </div>
  );

  const defaultInputField = (
    <>
      <input
        type={type}
        className={styles.inputElement}
        value={inputValue ?? ''}
        onChange={handleChange}
        disabled={disabled}
        {...inputProps}
      />
      {tailingIcon && (
        <div className={classNames(styles.icon, styles.tailingIcon)}>{tailingIcon}</div>
      )}
      {error && <ErrorAlt className={styles.icon} />}
      {variant === 'loading' && (
        <Preloader
          className={styles.icon}
          isActive={true}
          size="small"
          type="static"
        />
      )}
      {variant === 'success' && <CheckCircle className={styles.icon} />}
      {variant === 'warning' && <Error className={styles.icon} />}
    </>
  );

  const getInputField = (type: TextFieldProps['type']) => {
    if (type === 'select') return selectInputField;

    if (type === 'url') return urlInputField;

    return defaultInputField;
  };

  return (
    <div className={classNames(styles.wrapper, className, disabled && styles.disabled)}>
      {label && (
        <label
          htmlFor={inputProps.id}
          className={classNames(styles.label)}
        >
          {label}
        </label>
      )}
      <div
        className={classNames(
          type !== 'url' && type !== 'select' && styles.inputContainer,
          tailingIcon && styles.withTailingIcon,
          measure === 'large' && type === 'text' && styles.inputContainerLarge,
          measure === 'small' && type === 'text' && styles.inputContainerSmall,
          leadingIcon && styles.withLeadingIcon,
          error && styles.errored,
          variant === 'success' && styles.success,
          variant === 'warning' && styles.warning,
        )}
      >
        {leadingIcon && type !== 'url' && type !== 'select' && (
          <div className={classNames(styles.icon, styles.leadingIcon)}>{leadingIcon}</div>
        )}
        {getInputField(type)}
      </div>
      {helpText && (
        <Text
          variant="caption12Regular"
          className={styles.helpText}
        >
          {helpText}
        </Text>
      )}
      {variant === 'success' && (
        <Text
          variant="caption12Regular"
          className={styles.successText}
        >
          {success}
        </Text>
      )}
      {variant === 'warning' && (
        <Text
          variant="caption12Regular"
          className={styles.warningText}
        >
          {warning}
        </Text>
      )}
      {error && !shouldHideErrorMessageText && (
        <Text
          variant="caption12Regular"
          className={styles.errorText}
        >
          {error}
        </Text>
      )}
    </div>
  );
};

export { TextField };
