import { ReactNode } from 'react';
import { PaginationProps } from '../pagination/pagination.types';

/**
 * TableProps
 * @description Props for the Table component.
 *
 */
type TableProps = {
  /**
   * An array of table sections
   */
  readonly data: ReadonlyArray<{
    /**
     * Specifies section name
     */
    readonly sectionName?: string;

    /**
     * Table heading row
     */
    readonly header: Omit<TableRow, 'id'>;

    /**
     * Array of table rows
     */
    readonly rows: ReadonlyArray<TableRow>;

    /**
     * Specifies if the table content is loading.
     */
    readonly isLoading: boolean;

    /**
     * Set loading text.
     */
    readonly loadingText?: string;

    /**
     * A callback function invoked on sort
     */
    readonly onSort?: (options: SortingOptions) => void;

    /**
     * Specifies available columns indexed for sorting.
     * If not provided - all columns will be available for sorting
     */
    readonly availableSortingColumnsIndexes?: ReadonlyArray<number>;

    /**
     * Specifies current sorting options
     */
    readonly currentSortingOptions?: SortingOptions;

    /**
     * A callback function invoked on checking a row in a table
     */
    readonly onSelect?: (rowId: string) => void;

    /**
     * A callback function invoked on clicking a row in a table.
     */
    readonly onRowClick?: (rowId: string) => void;

    /**
     * Specifies cta element in the end of the row
     */
    readonly cta?: (rowId: string) => ReactNode;

    /**
     * Index of expanded column
     */
    readonly expandedColumnOptions?: {
      readonly columnIndex: number;
      readonly preferredWidth: number;
    };
  }>;

  /**
   * Specifies the Rows of table to be crossed
   */
  readonly tableCrossed?: boolean;

  /**
   * Add pagination to the table
   */
  readonly pagination?: boolean;

  /**
   * props of pagination
   */
  readonly paginationProps?: PaginationProps;
};

type SortingOptions = {
  readonly sortByColumnIndex: number;
  readonly direction: 'ascending' | 'descending';
};

type TableRow = {
  /**
   * Table row id.
   */
  readonly id: string;

  /**
   * Array of row columns
   */
  readonly columns: ReadonlyArray<TableColumn>;

  /**
   * Specifies if the row is selected
   */
  readonly isSelected?: boolean;

  /**
   * Specifies if the row is dark.
   */
  readonly isDark?: boolean;
};

type TableColumn = ReactNode;

export type { SortingOptions, TableProps, TableRow };
