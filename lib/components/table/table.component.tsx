import { FC } from 'react';

import ChevronDown from '@assets/icons/chevron-down.svg';
import { Checkbox, Pagination } from '@components';
import classNames from 'classnames';
import styles from './table.module.scss';
import { TableProps } from './table.types';

const selectAllHandler = (
  selectHandler: (value: string) => void,
  rows: TableProps['data'][0]['rows'],
) => {
  if (
    (rows.every(({ isSelected }) => isSelected) || rows.every(({ isSelected }) => !isSelected)) &&
    rows.length > 0
  ) {
    rows.forEach(({ id }) => {
      selectHandler(id);
    });
  } else {
    [...rows]
      .filter(({ isSelected }) => !isSelected)
      .forEach(({ id }) => {
        selectHandler(id);
      });
  }
};

const calculateColumnsCount = (
  dataColumnsCount: number,
  includeCta: boolean,
  includeCheckbox: boolean,
  expandedColumnOptions?: {
    readonly columnIndex: number;
    readonly preferredWidth: number;
  },
) => {
  const count = dataColumnsCount;
  const columns = includeCheckbox ? ['min-content'] : [];
  for (let i = 0; i < count; i++) {
    expandedColumnOptions && expandedColumnOptions.columnIndex === i
      ? columns.push(`${expandedColumnOptions.preferredWidth}px`)
      : columns.push('1fr');
  }

  includeCta && columns.push('min-content');

  return columns.join(' ');
};

/**
 * Table component.
 * @description Universal table component for the global app usage
 * @category Components
 * @param { TableProps } props - TableProps defined in the './table.types.ts'.
 */
const Table: FC<TableProps> = ({ data, tableCrossed, pagination, paginationProps }) => (
  <div className={classNames(styles.wrapper, pagination && styles.paginationActive)}>
    {!!data.length &&
      data.map(
        (
          {
            header,
            isLoading,
            loadingText,
            rows,
            availableSortingColumnsIndexes,
            cta,
            currentSortingOptions,
            onSelect,
            onSort,
            onRowClick,
            expandedColumnOptions,
          },
          index,
        ) => (
          <div
            key={`table${index}`}
            className={classNames(styles.table, tableCrossed && styles.tableCrossed)}
          >
            <div className={styles.heading}>
              <div
                className={classNames(styles.tr, styles.thead)}
                style={{
                  gridTemplateColumns: `${calculateColumnsCount(
                    header.columns.length,
                    !!cta,
                    !!onSelect,
                    expandedColumnOptions,
                  )}`,
                }}
              >
                {onSelect && (
                  <div className={styles.checkbox}>
                    <Checkbox
                      checkedState={(() => {
                        if (rows.every(({ isSelected }) => isSelected) && rows.length > 0) {
                          return 'checked';
                        } else if (rows.some(({ isSelected }) => isSelected)) {
                          return 'indeterminate';
                        } else {
                          return 'unchecked';
                        }
                      })()}
                      onChange={() => selectAllHandler(onSelect, rows)}
                    />
                  </div>
                )}
                {header.columns.map((heading, index) => (
                  <div
                    className={styles.th}
                    key={index.toString()}
                  >
                    {heading}
                    {onSort &&
                      (!availableSortingColumnsIndexes ||
                        availableSortingColumnsIndexes.some((item) => index === item)) && (
                        <ChevronDown
                          className={classNames(
                            styles.chevronDown,
                            currentSortingOptions?.direction === 'ascending' &&
                              currentSortingOptions.sortByColumnIndex === index &&
                              styles.chevronUp,
                          )}
                          onClick={() =>
                            onSort({
                              direction:
                                currentSortingOptions?.direction === 'ascending'
                                  ? 'descending'
                                  : 'ascending',
                              sortByColumnIndex: index,
                            })
                          }
                        />
                      )}
                  </div>
                ))}
                {cta && (
                  <div className={classNames(styles.th, styles.hiddenTh, styles.cta)}>
                    {cta('')}
                  </div>
                )}
              </div>
            </div>

            {isLoading ? (
              <div
                className={styles.tr}
                style={{
                  gridTemplateColumns: `${calculateColumnsCount(
                    header.columns.length,
                    !!cta,
                    !!onSelect,
                    expandedColumnOptions,
                  )}`,
                }}
              >
                <div
                  className={classNames(styles.td, styles.preloader)}
                  style={{
                    gridColumnStart: '1',
                    gridColumnEnd: `${cta ? header.columns.length + 2 : header.columns.length + 1}`,
                  }}
                >
                  {loadingText ?? 'Loading...'}
                </div>
              </div>
            ) : (
              rows.map(({ columns, id, isSelected, isDark }, index) => (
                <div
                  role={'button'}
                  tabIndex={0}
                  key={id + index}
                  className={classNames(styles.tr, isDark && styles.trDark)}
                  style={{
                    gridTemplateColumns: `${calculateColumnsCount(
                      header.columns.length,
                      !!cta,
                      !!onSelect,
                      expandedColumnOptions,
                    )}`,
                  }}
                  onKeyDown={() => onRowClick && onRowClick(id)}
                  onClick={() => onRowClick && onRowClick(id)}
                >
                  {onSelect && (
                    <div className={styles.checkbox}>
                      <Checkbox
                        checkedState={isSelected ? 'checked' : 'unchecked'}
                        onChange={() => onSelect(id)}
                      />
                    </div>
                  )}
                  {columns.map((col, index) => (
                    <div
                      title={typeof col === 'string' ? col : undefined}
                      className={styles.td}
                      key={id.concat(index.toString())}
                    >
                      {col}
                    </div>
                  ))}
                  {cta && (
                    <div
                      role={'button'}
                      tabIndex={-1}
                      onKeyDown={(event) => event.stopPropagation()}
                      onClick={(event) => event.stopPropagation()}
                      className={classNames(styles.cta, styles.td)}
                    >
                      {cta(id)}
                    </div>
                  )}
                </div>
              ))
            )}
          </div>
        ),
      )}
    {pagination && paginationProps && (
      <Pagination
        className={styles.pagination}
        {...paginationProps}
      />
    )}
  </div>
);

export { Table };
