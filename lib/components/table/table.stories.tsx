/* eslint-disable react/prop-types */
import { action } from '@storybook/addon-actions';
import { ComponentStory, Meta, StoryFn } from '@storybook/react';
import { Dispatch, SetStateAction, useState } from 'react';
import { Table } from './table.component';
import styles from './table.module.scss';
import { SortingOptions, TableProps } from './table.types';

import DotsVertical from '@assets/icons/dots-vertical.svg';
import IconVerified from '@assets/icons/icon-verified.svg';

export default {
  title: 'UI-KIT/Table',
  component: Table,
  decorators: [
    (Story) => (
      <div style={{ display: 'flex', maxHeight: 'calc(100vh - 32px)' }}>
        <Story />
      </div>
    ),
  ],
} as Meta<typeof Table>;

const handleSortItems = ({
  direction,
  items,
  setCurrentSortingOptions,
  setSortedItems,
  sortByColumnIndex,
}: {
  direction: SortingOptions['direction'];
  sortByColumnIndex: SortingOptions['sortByColumnIndex'];
  setSortedItems: Dispatch<SetStateAction<TableProps['data'][0]['rows']>>;
  items: TableProps['data'][0]['rows'];
  setCurrentSortingOptions: Dispatch<SetStateAction<SortingOptions | undefined>>;
}) => {
  setSortedItems(
    [...items].sort((a, b) => {
      const aColumn = a.columns[sortByColumnIndex];
      const bColumn = b.columns[sortByColumnIndex];

      if (aColumn && bColumn) {
        if (sortByColumnIndex !== 2) {
          switch (direction) {
            case 'ascending': {
              if (aColumn.toString() > bColumn.toString()) {
                return -1;
              } else if (aColumn.toString() < bColumn.toString()) {
                return 1;
              } else {
                return 0;
              }
            }

            case 'descending': {
              if (aColumn.toString() < bColumn.toString()) {
                return -1;
              } else if (aColumn.toString() > bColumn.toString()) {
                return 1;
              } else {
                return 0;
              }
            }

            default:
              return 0;
          }
        } else {
          switch (direction) {
            case 'ascending': {
              if (+aColumn > +bColumn) {
                return -1;
              } else if (+aColumn < +bColumn) {
                return 1;
              } else {
                return 0;
              }
            }

            case 'descending': {
              if (+aColumn < +bColumn) {
                return -1;
              } else if (+aColumn > +bColumn) {
                return 1;
              } else {
                return 0;
              }
            }

            default:
              return 0;
          }
        }
      } else {
        return 0;
      }
    }),
  );
  setCurrentSortingOptions({ direction, sortByColumnIndex });
};

const Template: StoryFn<typeof Table> = (props) => <Table {...props} />;

const SortingTemplate: StoryFn<typeof Table> = ({ data }) => {
  const [sortedItems, setSortedItems] = useState<TableProps['data'][0]['rows']>(
    data[0]?.rows ?? [],
  );
  const [currentSortingOptions, setCurrentSortingOptions] =
    useState<TableProps['data'][0]['currentSortingOptions']>();
  const [selectedItems, setSelectedItems] = useState<ReadonlyArray<string>>();

  return (
    <Table
      data={data.map((item) => ({
        ...item,
        rows: sortedItems.map((item) => ({
          ...item,
          isSelected: selectedItems?.some((selectedItem) => selectedItem === item.id),
        })),
        onSort: (options) =>
          handleSortItems({
            ...options,
            items: item.rows,
            setCurrentSortingOptions,
            setSortedItems,
          }),
        currentSortingOptions: currentSortingOptions,
        onSelect: (rowId) => {
          if (selectedItems?.some((item) => item === rowId)) {
            setSelectedItems((prevState) => (prevState ?? []).filter((item) => item !== rowId));
          } else {
            setSelectedItems((prevState) => [...(prevState ?? []), rowId]);
          }
        },
      }))}
    />
  );
};

const CheckboxTemplate: ComponentStory<typeof Table> = ({ data }) => {
  const [selectedItems, setSelectedItems] = useState<ReadonlyArray<string>>();

  return (
    <Table
      data={data.map((item) => ({
        ...item,
        rows: item.rows.map((row) => ({
          ...row,
          isSelected: selectedItems?.some((item) => item === row.id),
        })),
        onSelect: (rowId) => {
          if (selectedItems?.some((item) => item === rowId)) {
            setSelectedItems((prevState) => (prevState ?? []).filter((item) => item !== rowId));
          } else {
            setSelectedItems((prevState) => [...(prevState ?? []), rowId]);
          }
        },
      }))}
    />
  );
};

const Basic = Template.bind({});
Basic.args = {
  data: [
    {
      header: {
        columns: ['id', 'name', 'amount', 'status', 'text', 'text'],
      },
      rows: [
        {
          id: 'JD1',
          isDark: true,
          columns: [
            '01',
            <div
              key="JD1"
              className={styles.storiesBlock}
            >
              <IconVerified />
              <div className={styles.storiesTextBlock}>
                <p className={styles.storiesTitle}>Title</p>
                <p className={styles.storiesText}>Support text</p>
              </div>
            </div>,
            '12345.33',
            'pending',
            'text',
            'text',
          ],
        },
        {
          id: 'JD2',
          columns: ['02', 'Jim Doe', '54321.24', 'pending', 'text', 'text'],
        },
        {
          id: 'JD3',
          columns: ['03', 'Jim Doe', '12334345.33', 'pending', 'text', 'text'],
        },
        {
          id: 'JD4',
          columns: ['04', 'Jack Doe', '521.24', 'pending', 'text', 'text'],
        },
        {
          id: 'JD5',
          columns: ['05', 'John Doe', '12345.33', 'pending', 'text', 'text'],
        },
        {
          id: 'JD6',
          columns: ['06', 'Jim Doe', '54321.24', 'pending', 'text', 'text'],
        },
        {
          id: 'JD7',
          columns: ['07', 'Jim Doe', '12334345.33', 'pending', 'text', 'text'],
        },
        {
          id: 'JD8',
          columns: ['08', 'Jack Doe', '521.24', 'pending', 'text', 'text'],
        },
        {
          id: 'JD9',
          columns: ['09', 'Jim Doe', '12334345.33', 'pending', 'text', 'text'],
        },
        {
          id: 'JD10',
          columns: ['10', 'Jack Doe', '521.24', 'pending', 'text', 'text'],
        },
      ],
      isLoading: false,
      onSort: undefined,
      onSelect: undefined,
    },
  ],
};

const PaginationTemplate: ComponentStory<typeof Table> = (args) => {
  const [currentPage, setCurrentPage] = useState(1);
  const pageCount = 10;

  const paginationProps = {
    currentPage,
    setCurrentPage,
    pageCount,
    type: 'centeredRange' as const,
  };

  return (
    <Table
      {...args}
      pagination={true}
      paginationProps={paginationProps}
    />
  );
};

const Primary = Template.bind({});
Primary.args = {
  ...Basic.args,
  tableCrossed: true,
  pagination: true,
};

const PrimaryWithPagination = PaginationTemplate.bind({});
PrimaryWithPagination.args = {
  ...Basic.args,
  tableCrossed: true,
  pagination: true,
};

const WithCTA = Template.bind({});
WithCTA.args = {
  data: [
    {
      header: { columns: ['id', 'name', 'amount', 'status'] },
      rows: [
        { id: 'JD1', columns: ['01', 'John Doe', '12345.33', 'pending'] },
        { id: 'JD2', columns: ['02', 'Jane Doe', '54321.24', 'pending'] },
        { id: 'JD3', columns: ['03', 'Jim Doe', '12334345.33', 'pending'] },
        { id: 'JD4', columns: ['04', 'Jack Doe', '521.24', 'pending'] },
      ],
      isLoading: false,
      onSort: undefined,
      onSelect: undefined,
      cta: () => (
        <DotsVertical
          style={{ margin: '-8px 0' }}
          onClick={action('clicked on kebab menu button')}
        />
      ),
    },
  ],
};

const WithSorting = SortingTemplate.bind({});
WithSorting.args = Basic.args;

const WithCheckbox = CheckboxTemplate.bind({});
WithCheckbox.args = Basic.args;

const Complete = SortingTemplate.bind({});
Complete.args = WithCTA.args;

export { Basic, Complete, Primary, PrimaryWithPagination, WithCTA, WithCheckbox, WithSorting };
