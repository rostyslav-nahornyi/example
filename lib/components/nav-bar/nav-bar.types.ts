import { ReactNode } from 'react';
import { ButtonProps } from '../button/button.types';
import { NavTabProps } from '../nav-tab/nav-tab.types';
import { TabMenuProps } from '../tab-menu/tab-menu.types';

/**
 * Props of NavBar component.
 *
 * @author Sergii Goncharuk, Vitalii Bodnarchuk
 */
type NavBarProps = {
  /** Logo */
  readonly logo?: {
    /** Logo icon */
    readonly icon?: ReactNode;

    /** Logo title */
    readonly title?: string;
  };

  /** Tabs */
  readonly tabMenuProps: TabMenuProps;

  /** Search field */
  readonly search?: {
    /** Search field placeholder */
    readonly placeholder?: string;

    /** Search field icon */
    readonly icon?: ReactNode;

    /** onChange handler */
    readonly onChange: () => void;
  };

  /** Buttons */
  readonly buttons?: ReadonlyArray<
    Pick<ButtonProps, 'label' | 'onClick' | 'buttonType' | 'leadingIcon'>
  >;

  /** Nav tabs */
  readonly navTabs?: ReadonlyArray<NavTabProps>;

  /** Avatar */
  readonly avatar?: ReactNode;

  /** Avatar click handler */
  readonly onAvatarClick?: () => void;

  /** Type of NavBar */
  readonly type?: 'default' | 'sidebar';
};

export type { NavBarProps };
