import { FC } from 'react';

import { Avatar, Button, NavTab, TabMenu, Text, TextField } from '@components';
import classNames from 'classnames';
import styles from './nav-bar.module.scss';
import { NavBarProps } from './nav-bar.types';

/**
 * Navbar component
 * @description Navigation Bar component
 *
 * @author Sergii Goncharuk, Vitalii Bodnarchuk
 * @category Components
 * @param { NavigationProps } props - NavbarProps defined in the './navbar.types.ts'
 */
const Navbar: FC<NavBarProps> = ({
  logo,
  tabMenuProps,
  search,
  buttons,
  navTabs,
  avatar,
  onAvatarClick,
  type = 'default',
}) => (
  <div className={styles.navbar}>
    {logo && (
      <div className={classNames(styles.logoTabs, type === 'sidebar' && styles.sidebarTabs)}>
        {logo && logo.icon && (
          <div className={styles.logo}>
            <figure>{logo.icon}</figure>
            <span>{logo.title}</span>
          </div>
        )}
        {tabMenuProps && (
          <TabMenu
            {...tabMenuProps}
            className={styles.tabMenu}
          />
        )}
      </div>
    )}
    {logo?.title && !logo.icon && (
      <Text
        variant="caption22Medium"
        className={styles.title}
      >
        {logo.title}
      </Text>
    )}
    <div className={styles.actions}>
      {search && (
        <TextField
          className={styles.search}
          placeholder={search.placeholder}
          leadingIcon={search.icon}
          onChange={search.onChange}
        />
      )}
      {buttons && (
        <div className={styles.btn}>
          {buttons.map((btn, idx) => (
            <Button
              key={idx}
              size="m"
              buttonType={btn.buttonType ?? 'primary'}
              variant={'main'}
              label={btn.label}
              leadingIcon={btn.leadingIcon}
              onClick={btn.onClick}
            />
          ))}
        </div>
      )}
      {navTabs && (
        <div className={styles.navTabs}>
          {navTabs.map((item, idx) => (
            <NavTab
              key={idx}
              items={item.items}
              icon={item.icon}
              isActive={0 === idx}
            />
          ))}
        </div>
      )}
      {avatar && (
        <Avatar
          size="m"
          img={avatar}
          statusIndicatorPosition="off"
          onClick={onAvatarClick}
        />
      )}
    </div>
  </div>
);

export { Navbar };
