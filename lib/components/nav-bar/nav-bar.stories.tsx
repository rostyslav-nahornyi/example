import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import AvatarIcon from '@assets/icons/avatar-1.svg';
import Circle from '@assets/icons/circle.svg';
import File from '@assets/icons/file.svg';
import { Avatar } from '@components/avatar/avatar.components';
import { action } from '@storybook/addon-actions';
import { Navbar } from './nav-bar.component';
import { NavBarProps } from './nav-bar.types';

export default {
  title: 'UI-KIT/NavBar',
  component: Navbar,
} as Meta<typeof Navbar>;

const Template: StoryFn<typeof Navbar> = ({ tabMenuProps, ...restProps }: NavBarProps) => {
  const [selectedTab, setSelectedTab] = useState<string>();

  const handledTabs = tabMenuProps?.tabs?.map((tab) => ({
    ...tab,
    onClick: () => setSelectedTab(tab.id),
    isSelected: tab.id === selectedTab,
  }));

  return (
    <Navbar
      {...restProps}
      tabMenuProps={{ ...tabMenuProps, ...{ tabs: handledTabs } }}
    />
  );
};

const Default = Template.bind({});
Default.args = {
  logo: {
    title: 'Documents',
    icon: <File />,
  },
  tabMenuProps: {
    tabs: [
      {
        label: 'Tab 1',
        id: 'tab1',
        color: 'blue',
      },
      {
        label: 'Tab 2',
        id: 'tab2',
        color: 'blue',
      },
      {
        label: 'Tab 3',
        id: 'tab3',
        color: 'blue',
      },
    ],
    menuType: 'pill',
    size: 'small',
  },
  navTabs: [
    {
      icon: <Circle />,
      items: [
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click1'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click2'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click3'),
        },
      ],
    },
    {
      icon: <Circle />,
      items: [
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click1'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click2'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click3'),
        },
      ],
    },
  ],
  avatar: <AvatarIcon />,
  onAvatarClick: action('User onClick'),
};

const DefaultWithSearch = Template.bind({});
DefaultWithSearch.args = {
  ...Default.args,
  search: {
    placeholder: 'Search',
    icon: <Circle />,
    onChange: () => action('onChange'),
  },
};

const DefaultWithButtons = Template.bind({});
DefaultWithButtons.args = {
  ...Default.args,
  buttons: [
    {
      label: 'Label',
      buttonType: 'primary',
      leadingIcon: <Circle />,
      onClick: () => action('onClick'),
    },
    {
      label: 'Label',
      buttonType: 'secondary',
      leadingIcon: <Circle />,
      onClick: () => action('onClick'),
    },
  ],
};

const DefaultWithButtonsAndSearch = Template.bind({});
DefaultWithButtonsAndSearch.args = {
  ...Default.args,
  search: {
    placeholder: 'Search',
    icon: <Circle />,
    onChange: () => action('onChange'),
  },
  buttons: [
    {
      label: 'Label',
      buttonType: 'primary',
      leadingIcon: <Circle />,
      onClick: () => action('onClick'),
    },
    {
      label: 'Label',
      buttonType: 'secondary',
      leadingIcon: <Circle />,
      onClick: () => action('onClick'),
    },
  ],
};

const Sidebar = Template.bind({});
Sidebar.args = {
  ...DefaultWithButtonsAndSearch.args,
  type: 'sidebar',
};

const SidebarWithTabsAndAvatar = Template.bind({});
SidebarWithTabsAndAvatar.args = {
  type: 'sidebar',
  avatar: (
    <Avatar
      size="m"
      img={<AvatarIcon />}
      statusIndicatorPosition="off"
      onClick={action('User onClick')}
    />
  ),
  navTabs: [
    {
      icon: <Circle />,
      items: [
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click1'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click2'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click3'),
        },
      ],
    },
    {
      icon: <Circle />,
      items: [
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click1'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click2'),
        },
        {
          icon: <Circle />,
          text: 'Label',
          onClick: action('Click3'),
        },
      ],
    },
  ],
};

const SidebarWithTitle = Template.bind({});
SidebarWithTitle.args = {
  ...SidebarWithTabsAndAvatar.args,
  logo: { title: 'Title' },
  type: 'sidebar',
};

export {
  Default,
  DefaultWithButtons,
  DefaultWithButtonsAndSearch,
  DefaultWithSearch,
  Sidebar,
  SidebarWithTabsAndAvatar,
  SidebarWithTitle,
};
