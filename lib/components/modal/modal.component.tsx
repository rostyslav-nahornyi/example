import Close from '@assets/icons/close.svg';
import { Button, Overlay, Portal, Text } from '@components';
import { useModalStore } from '@store';
import { FC, useEffect } from 'react';

import classNames from 'classnames';
import { HeaderIcon } from './components';
import styles from './modal.module.scss';

/**
 * Modal component.
 * @description Global modal window component
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { ModalProps } props - ModalProps defined in the './modal.types.ts'
 */
const Modal: FC = () => {
  const { hideModal, isOpened, data } = useModalStore();

  useEffect(() => {
    document.body.style.overflowY = 'hidden';

    return () => {
      document.body.style.overflowY = 'auto';
    };
  }, []);

  if (isOpened && data) {
    const { variant, title, description, controlButtons } = data;

    return (
      <Portal domNode={'modal-root'}>
        <Overlay
          isShown={!!isOpened}
          blur={7.5}
          onOverlayClick={() => hideModal()}
        />

        <div className={classNames(styles.modal)}>
          <div className={classNames(styles.wrapper)}>
            <Close
              className={styles.closeIcon}
              onClick={() => hideModal()}
            />
            <HeaderIcon variant={variant} />

            <div className={styles.contentWrapper}>
              <div className={styles.content}>
                <Text
                  variant="caption24Medium"
                  className={styles.title}
                >
                  {title}
                </Text>
                {variant === 'default' ? (
                  <div>
                    <hr className={styles.hr} />
                    <Text
                      variant="caption16Medium"
                      className={styles.defaultText}
                    >
                      {description}
                    </Text>
                  </div>
                ) : (
                  <Text
                    variant="caption16Medium"
                    className={styles.description}
                  >
                    {description}
                  </Text>
                )}
              </div>

              <div className={styles.controlButtons}>
                <Button
                  variant="main"
                  buttonType="primary"
                  size="l"
                  {...controlButtons.main}
                />
                {controlButtons.secondary && (
                  <Button
                    variant="main"
                    buttonType="secondary"
                    size="l"
                    {...controlButtons.secondary}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </Portal>
    );
  }

  return null;
};

export { Modal };
