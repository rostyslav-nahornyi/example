import { ReactNode } from 'react';
import { ButtonProps } from '../button/button.types';

/**
 * Props of Modal
 * @author Vitalii Bodnarchuk
 */

type ModalProps = {
  /**
   * Modal type.
   */
  readonly variant: 'success' | 'loading' | 'error' | 'default' | 'warning' | 'time';

  /**
   * Specifies modal window title.
   */
  readonly title: string;

  /**
   * Specifies modal window main text.
   */
  readonly description: ReactNode;

  /**
   * Buttons settings.
   */
  readonly controlButtons: {
    /**
     * Top button.
     */
    readonly main: Pick<ButtonProps, 'label' | 'onClick' | 'disabled'>;

    /**
     * Bottom button.
     * Works only if variant is 'default'.
     */
    readonly secondary?: Pick<ButtonProps, 'label' | 'onClick' | 'disabled'>;
  };
};

export type { ModalProps };
