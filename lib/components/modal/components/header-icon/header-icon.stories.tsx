import { Meta, StoryFn } from '@storybook/react';

import { HeaderIcon } from './header-icon.component';

export default {
  title: 'UI-KIT/Modal/HeaderIcon',
  component: HeaderIcon,
} as Meta<typeof HeaderIcon>;

const Template: StoryFn<typeof HeaderIcon> = (props) => <HeaderIcon {...props} />;

const Success = Template.bind({});
Success.args = {
  variant: 'success',
};

const Loading = Template.bind({});
Loading.args = {
  variant: 'loading',
};

const Error = Template.bind({});
Error.args = {
  variant: 'error',
};

const Time = Template.bind({});
Time.args = {
  variant: 'time',
};

const Warning = Template.bind({});
Warning.args = {
  variant: 'warning',
};

export { Error, Loading, Success, Time, Warning };
