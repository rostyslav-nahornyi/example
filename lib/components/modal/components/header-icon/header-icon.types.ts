import { ModalProps } from '../../modal.types';

/**
 * HeaderIconProps
 * @author Vitalii Bodnarchuk
 * @description Props for the HeaderIcon component
 */
type HeaderIconProps = Pick<ModalProps, 'variant'>;

export type { HeaderIconProps };
