import CheckCircle from '@assets/icons/check-circle.svg';
import ErrorAlt from '@assets/icons/error-alt.svg';
import Error from '@assets/icons/error.svg';
import Time from '@assets/icons/time.svg';
import { Preloader } from '@components/preloader/preloader.component';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './header-icon.module.scss';
import { HeaderIconProps } from './header-icon.types';
/**
 * HeaderIcon component
 * @description Modal window
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { HeaderIconProps } props - HeaderIconProps defined in the './overlay.types.ts'
 */
const HeaderIcon: FC<HeaderIconProps> = ({ variant }) => {
  const renderIcon = (
    variant: 'default' | 'error' | 'warning' | 'success' | 'time' | 'loading',
  ) => {
    switch (variant) {
      case 'error':
        return <ErrorAlt className={styles.icon} />;
      case 'warning':
        return <Error className={styles.icon} />;
      case 'success':
        return <CheckCircle className={styles.icon} />;
      case 'time':
        return <Time className={styles.icon} />;
      case 'loading':
        return (
          <Preloader
            className={styles.loader}
            type="static"
            size="small"
            isActive={true}
          />
        );
      default:
        return <></>;
    }
  };

  return variant !== 'default' ? (
    <div className={styles.wrapper}>
      <div className={classNames(styles.circleWrapper, styles[variant])}>{renderIcon(variant)}</div>
    </div>
  ) : null;
};

export { HeaderIcon };
