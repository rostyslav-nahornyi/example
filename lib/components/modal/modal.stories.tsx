import { Meta, StoryFn } from '@storybook/react';

import { useModalStore } from '@store';
import { action } from '@storybook/addon-actions';
import { Button } from '../button';
import { Modal } from './modal.component';
import { ModalProps } from './modal.types';

export default {
  title: 'UI-KIT/Modal',
  component: Modal,
  decorators: [
    (Story) => (
      <>
        <Story />
        <Modal />
      </>
    ),
  ],
} as Meta<typeof Modal>;

const Template: StoryFn<typeof Modal> = (props) => {
  const { showModal } = useModalStore();

  return (
    <div>
      <Button
        variant="success"
        size="l"
        buttonType="floating"
        label="Open/Close Modal"
        onClick={() => showModal(props as ModalProps)}
      />
    </div>
  );
};

const Default = Template.bind({});
Default.args = {
  variant: 'default',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
  },
};

const Time = Template.bind({});
Time.args = {
  variant: 'time',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

const Success = Template.bind({});
Success.args = {
  variant: 'success',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

const Loading = Template.bind({});
Loading.args = {
  variant: 'loading',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

const Warning = Template.bind({});
Warning.args = {
  variant: 'warning',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

const Error = Template.bind({});
Error.args = {
  variant: 'error',
  title: 'Label',
  description: 'Body Text',
  controlButtons: {
    main: { label: 'Label', onClick: action('main button click') },
    secondary: { label: 'Label', onClick: action('secondary button click') },
  },
};

export { Default, Error, Loading, Success, Time, Warning };
