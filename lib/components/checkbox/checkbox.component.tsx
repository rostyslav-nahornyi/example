import { forwardRef, useEffect, useRef } from 'react';

import Check from '@assets/icons/check.svg';
import Indeterminate from '@assets/icons/indeterminate.svg';
import { Text } from '@components';
import classNames from 'classnames';
import styles from './checkbox.module.scss';
import { CheckboxProps } from './checkbox.types';
/**
 * Checkbox component
 * @description Checkbox with indeterminate state
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { CheckboxProps } props - CheckboxProps defined in the './checkbox.types.ts'
 */

const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ checkedState = 'unchecked', label, helpText, ...inputProps }, ref) => {
    const internalRef = useRef<HTMLInputElement | null>(null);

    const synchronizeRefs = (el: HTMLInputElement | null) => {
      internalRef.current = el;

      if (!ref) return;

      if (typeof ref === 'object') {
        ref.current = el;
      } else {
        ref(el);
      }
    };

    useEffect(() => {
      if (internalRef.current) {
        internalRef.current.checked = checkedState === 'checked';
        internalRef.current.indeterminate = checkedState === 'indeterminate';
      }
    }, [checkedState]);

    return (
      <label className={styles.wrapper}>
        <div className={helpText ? styles.helpWrapper : styles.block}>
          <input
            hidden
            ref={synchronizeRefs}
            type="checkbox"
            {...inputProps}
          />
          <div
            tabIndex={checkedState ? 0 : -1}
            role="button"
            className={classNames(
              styles.checkWrapper,
              checkedState === 'checked' && styles.checked,
              checkedState === 'indeterminate' && styles.indeterminate,
            )}
          >
            <span className={classNames(styles.checkIcon)}>
              {checkedState === 'indeterminate' ? <Indeterminate /> : <Check />}
            </span>
          </div>
          {helpText ? (
            <div className={styles.helpBlock}>
              <Text
                variant="caption16Medium"
                className={styles.title}
              >
                {label}
              </Text>
              <Text
                variant="caption14Regular"
                className={styles.subtext}
              >
                {helpText}
              </Text>
            </div>
          ) : (
            <Text
              variant="caption14Medium"
              className={styles.text}
            >
              {label}
            </Text>
          )}
        </div>
      </label>
    );
  },
);

Checkbox.displayName = 'Checkbox';
export { Checkbox };
