import { DetailedHTMLProps, InputHTMLAttributes } from 'react';

/**
 * Props of Checkbox component.
 *
 * @author Vitalii Bodnarchuk
 */
interface CheckboxProps
  extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  /**
   * Determinate checkbox state
   */
  readonly checkedState: 'checked' | 'unchecked' | 'indeterminate';

  /**
   * Label
   */
  readonly label?: string;

  /**
   * Help text
   */
  readonly helpText?: string;
}

export type { CheckboxProps };
