import { Meta } from '@storybook/react';
import { useState } from 'react';
import { Checkbox } from './checkbox.component';

export default {
  title: 'UI-KIT/Checkbox',
  component: Checkbox,
} as Meta<typeof Checkbox>;

const ControlledBasic = () => {
  const [checkedState, setChecked] = useState<'checked' | 'unchecked' | 'indeterminate'>(
    'unchecked',
  );
  const handleChange = () => {
    setChecked(checkedState === 'unchecked' ? 'checked' : 'unchecked');
  };
  return (
    <Checkbox
      checkedState={checkedState}
      onChange={handleChange}
      label="Label"
    />
  );
};

const ControlledBasicWithHelpText = () => {
  const [checkedState, setCheckedState] = useState<'checked' | 'unchecked' | 'indeterminate'>(
    'unchecked',
  );

  const handleChange = () => {
    setCheckedState(checkedState === 'unchecked' ? 'checked' : 'unchecked');
  };
  return (
    <Checkbox
      checkedState={checkedState}
      onChange={handleChange}
      label="Label"
      helpText="Help Text"
    />
  );
};

const ControlledIndeterminate = () => {
  const [checkedState, setCheckedState] = useState<'checked' | 'unchecked' | 'indeterminate'>(
    'unchecked',
  );

  const toggleIndeterminate = () => {
    setCheckedState(checkedState !== 'indeterminate' ? 'indeterminate' : 'unchecked');
  };
  return (
    <Checkbox
      checkedState={checkedState}
      onChange={toggleIndeterminate}
      label="Indeterminate"
    />
  );
};

const IndeterminateGroup = () => {
  const groceryList = ['apples', 'bananas', 'oranges', 'bread', 'turkey', 'tomatoes'];
  const [selected, setSelected] = useState<string[]>([]);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        rowGap: '10px',
      }}
    >
      <Checkbox
        label={'Select all'}
        checkedState={(() => {
          if (selected.length === 0) {
            return 'unchecked';
          } else if (selected.length === groceryList.length) {
            return 'checked';
          } else {
            return 'indeterminate';
          }
        })()}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          if (event.target.checked) {
            setSelected(groceryList);
          } else {
            setSelected([]);
          }
        }}
      />

      {groceryList.map((item) => (
        <Checkbox
          key={item}
          label={item}
          checkedState={selected.includes(item) ? 'checked' : 'unchecked'}
          onChange={() => {
            if (selected.includes(item)) {
              setSelected((s) => s.filter((i) => i !== item));
            } else {
              setSelected((s) => [...s, item]);
            }
          }}
        />
      ))}
    </div>
  );
};

export {
  ControlledBasic,
  ControlledBasicWithHelpText,
  ControlledIndeterminate,
  IndeterminateGroup,
};
