import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from 'react';

/**
 * TabProps
 * @description Props for Pill, Underline tabs
 *
 * @author Vitalii Bodnarchuk
 */
interface TabProps
  extends Pick<
    DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>,
    'disabled' | 'onClick'
  > {
  /**
   * Inner tab text.
   */
  readonly label?: string;

  /**
   * Specifies tab is selected.
   */
  readonly isSelected?: boolean;

  /**
   * Tab size.
   */
  readonly size?: 'small' | 'large';

  /**
   * color of pill tab
   */
  readonly color?: 'gray' | 'blue' | 'white';

  /**
   * leading icon
   */
  readonly leadingIcon?: ReactNode;

  /**
   * add badge
   */
  readonly badgeText?: string;
}

export type { TabProps };
