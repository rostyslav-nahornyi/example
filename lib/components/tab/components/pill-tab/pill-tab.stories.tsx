import { Meta, StoryFn } from '@storybook/react';

import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { PillTab } from './pill-tab.component';
import { PillTabProps } from './pill-tab.types';

export default {
  title: 'UI-KIT/Tab/Pill',
  component: PillTab,
} as Meta<typeof PillTab>;

const Template: StoryFn<typeof PillTab> = (props: PillTabProps) => <PillTab {...props} />;

const GraySmall = Template.bind({});
GraySmall.args = {
  label: 'Text',
  size: 'small',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  color: 'gray',
  onClick: action('clicked on chart tab...'),
};

const GraySmallSelected = Template.bind({});
GraySmallSelected.args = {
  label: 'Text',
  size: 'small',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  isSelected: true,
  color: 'gray',
  onClick: action('clicked on chart tab...'),
};

const White = Template.bind({});
White.args = {
  label: 'Text',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  color: 'white',
  size: 'large',
  onClick: action('clicked on chart tab...'),
};

const WhiteSelected = Template.bind({});
WhiteSelected.args = {
  label: 'Text',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  color: 'white',
  size: 'large',
  isSelected: true,
  onClick: action('clicked on chart tab...'),
};

const Blue = Template.bind({});
Blue.args = {
  label: 'Text',
  color: 'blue',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  size: 'large',
  onClick: action('clicked on chart tab...'),
};

const BlueSelected = Template.bind({});
BlueSelected.args = {
  label: 'Text',
  color: 'blue',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  size: 'large',
  isSelected: true,
  onClick: action('clicked on chart tab...'),
};

const Disabled = Template.bind({});
Disabled.args = {
  label: 'Text',
  color: 'blue',
  leadingIcon: <Circle />,
  badgeText: 'Text',
  size: 'large',
  isSelected: true,
  disabled: true,
  onClick: action('clicked on chart tab...'),
};

export { Blue, BlueSelected, Disabled, GraySmall, GraySmallSelected, White, WhiteSelected };
