import { TabProps } from '../../tab.types';

/**
 * PillTabProps
 * @description Props for the Tab.Pill component
 *
 * @author Vitalii Bodnarchuk
 */
type PillTabProps = TabProps;

export type { PillTabProps };
