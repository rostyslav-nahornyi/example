import { Badge } from '@components';
import { FC } from 'react';
import { PillTabProps } from './pill-tab.types';

import classNames from 'classnames';
import styles from './pill-tab.module.scss';

/**
 * Exclusively PillTab component for Tab
 * @description Tab.Pill component
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { PillTabProps } props - TabProps defined in the './pill-tab.types.ts'
 */
const PillTab: FC<PillTabProps> = ({
  label,
  size = 'small',
  isSelected,
  disabled,
  onClick,
  color = 'gray',
  leadingIcon,
  badgeText,
}) => (
  <button
    tabIndex={0}
    aria-label={'button'}
    type={'button'}
    disabled={disabled}
    className={classNames(
      styles.pillTab,
      isSelected && styles.selected,
      !label && (leadingIcon || badgeText) && styles.iconOnly,
      styles[color],
      styles[size],
    )}
    onClick={onClick}
  >
    {leadingIcon && <div className={styles.icon}>{leadingIcon}</div>}
    <p>{label}</p>
    {badgeText && (
      <Badge
        size={size}
        label={badgeText}
        badgeType={isSelected && color === 'blue' ? 'colored' : 'default'}
        emphasis="low"
      />
    )}
  </button>
);

export { PillTab };
