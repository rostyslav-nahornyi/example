import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { UnderlineTab } from './underline-tab.component';
import { UnderlineTabProps } from './underline-tab.types';

export default {
  title: 'UI-KIT/Tab/Underline',
  component: UnderlineTab,
} as Meta<typeof UnderlineTab>;

const Template: StoryFn<typeof UnderlineTab> = (props: UnderlineTabProps) => (
  <UnderlineTab
    {...props}
    onClick={action('clicked on settings tab...')}
  />
);

const Default = Template.bind({});
Default.args = {
  label: 'Text',
  size: 'small',
  leadingIcon: <Circle />,
  badgeText: 'Text',
};

const Selected = Template.bind({});
Selected.args = {
  label: 'Text',
  isSelected: true,
  size: 'small',
  leadingIcon: <Circle />,
  badgeText: 'Text',
};

const Disabled = Template.bind({});
Disabled.args = {
  label: 'Text',
  isSelected: true,
  disabled: true,
  size: 'small',
  leadingIcon: <Circle />,
  badgeText: 'Text',
};

export { Default, Disabled, Selected };
