import { FC } from 'react';

import { Badge } from '@components';
import classNames from 'classnames';
import styles from './underline-tab.module.scss';
import { UnderlineTabProps } from './underline-tab.types';

/**
 * Exclusively UnderlineTab component for Tab
 * @description UnderlineTab tab
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { TabProps } props - TabProps defined in the '../tab-props.types.ts'
 */
const UnderlineTab: FC<UnderlineTabProps> = ({
  label,
  isSelected,
  onClick,
  size = 'small',
  leadingIcon,
  badgeText,
  disabled,
}) => (
  <button
    tabIndex={0}
    aria-label={'button'}
    type={'button'}
    className={classNames(
      styles.underlineTab,
      isSelected && styles.selected,
      !label && (leadingIcon || badgeText) && styles.iconOnly,
      size && styles[size],
    )}
    onClick={onClick}
    disabled={disabled}
  >
    {leadingIcon && <div className={styles.icon}>{leadingIcon}</div>}
    <p>{label}</p>
    {badgeText && (
      <Badge
        size={size}
        label={badgeText}
        badgeType={isSelected ? 'colored' : 'default'}
        emphasis="low"
      />
    )}
  </button>
);

export { UnderlineTab };
