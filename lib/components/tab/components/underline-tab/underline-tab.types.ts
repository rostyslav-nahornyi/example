import { TabProps } from '../../tab.types';

/**
 * UnderlineTabProps
 * @description Props for the Tab.Underline component
 *
 * @author Vitalii Bodnarchuk
 */
type UnderlineTabProps = Omit<TabProps, 'color'>;

export type { UnderlineTabProps };
