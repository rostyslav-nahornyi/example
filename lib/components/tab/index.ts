import { PillTab } from './components/pill-tab';
import { UnderlineTab } from './components/underline-tab';

const Tab = {
  Pill: PillTab,
  Underline: UnderlineTab,
};

export { Tab };
