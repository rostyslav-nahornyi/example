import { Ref, forwardRef, useState } from 'react';

import Check from '@assets/icons/check.svg';
import CloseIcon from '@assets/icons/close.svg';
import { Avatar } from '@components';
import classNames from 'classnames';
import styles from './chip.module.scss';
import { ChipProps } from './chip.types';
/**
 * Chip component
 * @description Chip
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { ChipProps } props - ButtonProps defined in the './chip.types.ts'
 */
const Chip = forwardRef<HTMLButtonElement, ChipProps>(
  (
    {
      id,
      size,
      variant,
      icon,
      label,
      emphasis,
      className,
      style,
      type = 'button',
      disabled,
      onClick,
      children,
      isAvatar,
      isRectangle = false,
      tabIndex = 0,
    },
    ref: Ref<HTMLButtonElement>,
  ) => {
    const [isChecked, setIsChecked] = useState<boolean>();
    const isDefaultOrSelected = variant === 'default' || variant === 'selected';
    return (
      <button
        id={id}
        className={classNames(
          styles.chip,
          styles[emphasis],
          styles[size],
          styles[variant],
          disabled && styles.disabled,
          isRectangle && styles.rectangle,
          isAvatar && styles.avatar,
          className,
        )}
        type={type}
        disabled={disabled}
        style={style}
        tabIndex={tabIndex}
        aria-label={'button'}
        ref={ref}
        onClick={(e) => {
          e.stopPropagation();
          setIsChecked(!isChecked);
          if (onClick && !disabled) onClick();
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <>
          {isDefaultOrSelected && isChecked && (
            <div className={classNames(styles.icon, (icon || isAvatar) && styles.checkedIcon)}>
              <Check />
            </div>
          )}
          {isAvatar && (
            <Avatar
              statusIndicatorPosition="off"
              className={styles.avatarSize}
              size={size === 'l' ? 'xs' : 'xxs'}
              img={icon}
            />
          )}
          {icon && !isAvatar && <div className={styles.icon}>{icon}</div>}
          {label && <p>{label}</p>}
          {!label && children && children}
          {variant === 'closable' && (
            <div className={classNames(styles.icon, (icon || isAvatar) && styles.closedIcon)}>
              <CloseIcon />
            </div>
          )}
        </>
      </button>
    );
  },
);
Chip.displayName = 'Chip';

export { Chip };
