import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from 'react';

/**
 * Props of Chip component.
 *
 * @author Vitalii Bodnarchuk
 */
interface ChipProps
  extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  /**
   *  Specifies chip size.
   */
  readonly size: 's' | 'm' | 'l';

  /**
   * Label.
   */
  readonly label?: string;

  /**
   * Specifies chip variant.
   */
  readonly variant: 'default' | 'selected' | 'closable' | 'empty';

  /**
   * Specifies chip type.
   */
  readonly emphasis: 'high' | 'highColored' | 'low' | 'lowColored';

  /**
   * Icon.
   */
  readonly icon?: ReactNode;

  /**
   * rectangle
   */
  readonly isRectangle?: boolean;

  /**
   * Chip with Avatar
   */
  readonly isAvatar?: boolean;

  /**
   * Invoked callback.
   */
  readonly onClick?: () => void;
}

export type { ChipProps };
