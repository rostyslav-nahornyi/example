import AvatarIcon from '@assets/icons/avatar.svg';
import Circle from '@assets/icons/circle.svg';
import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { Chip } from './chip.component';

export default {
  title: 'UI-KIT/Chip',
  component: Chip,
} as Meta<typeof Chip>;

const Template: StoryFn<typeof Chip> = (props) => (
  <Chip
    {...props}
    onClick={action('clicked on button...')}
  />
);

const HighEmphasis = Template.bind({});
HighEmphasis.args = {
  size: 's',
  variant: 'closable',
  label: 'Label',
  emphasis: 'high',
  icon: <Circle />,
};

const HighEmphasisColored = Template.bind({});
HighEmphasisColored.args = {
  size: 'm',
  variant: 'default',
  label: 'Label',
  emphasis: 'highColored',
  isRectangle: true,
  icon: <Circle />,
};

const LowEmphasis = Template.bind({});
LowEmphasis.args = {
  size: 'l',
  variant: 'default',
  label: 'Label',
  emphasis: 'low',
  icon: <Circle />,
};

const LowEmphasisColored = Template.bind({});
LowEmphasisColored.args = {
  size: 'l',
  variant: 'default',
  label: 'Label',
  icon: <AvatarIcon />,
  isAvatar: true,
  isRectangle: true,
  emphasis: 'lowColored',
};

export { HighEmphasis, HighEmphasisColored, LowEmphasis, LowEmphasisColored };
