import { useCallback, useEffect, useRef, useState } from 'react';
import { CalendarDayType, DatepickerProps, DatepickerValueType, Range } from './datepicker.types';

import dayjs from 'dayjs';
import isBetweenPlugin from 'dayjs/plugin/isBetween';
import isoWeekPlugin from 'dayjs/plugin/isoWeek';
import { CellProps } from './components/cell.types';

dayjs.extend(isBetweenPlugin);
dayjs.extend(isoWeekPlugin);

const getSelectionProperties: (
  currentDay: Date,
  selectedValue: DatepickerValueType,
) => Pick<CalendarDayType, 'isSelected' | 'position' | 'isSelectedBoundary'> = (
  currentDay,
  selectedValue,
) => {
  const isSelectedValueDate = selectedValue instanceof Date;

  // Not multiple when typeof selectedValue === "Date"
  if (isSelectedValueDate) {
    return { isSelected: dayjs(currentDay).isSame(selectedValue, 'day') };
  }

  // Multiple when typeof selectedValue === 'Range'
  const isRangeExist = selectedValue?.to && selectedValue.from;

  if (!isRangeExist && selectedValue?.from) {
    return {
      isSelected: dayjs(currentDay).isSame(selectedValue.from),
    };
  }

  const isFirstDay = dayjs(currentDay).isSame(selectedValue.from, 'day');
  const isLastDay = dayjs(currentDay).isSame(selectedValue.to, 'day');
  const isInMiddle = dayjs(currentDay).isBetween(
    dayjs(selectedValue.from).startOf('day'),
    dayjs(selectedValue.to).endOf('day'),
    undefined,
    '[]',
  );

  const isMondayPos: CellProps['position'] =
    dayjs(currentDay).isoWeekday() === 1 ? 'start' : undefined;
  const isSundayPos: CellProps['position'] =
    dayjs(currentDay).isoWeekday() === 7 ? 'end' : undefined;
  const isMiddlePos: CellProps['position'] = isInMiddle ? 'middle' : undefined;

  const boundaryPosition: CellProps['position'] =
    (isFirstDay ? 'start' : undefined) || (isLastDay ? 'end' : undefined);

  return {
    isSelected: isInMiddle,
    position: boundaryPosition || isMondayPos || isSundayPos || isMiddlePos,
    isSelectedBoundary: !!isFirstDay || !!isLastDay,
  };
};

const getCalendarDaysRange = (
  date: Date,
  selectedValue?: DatepickerValueType,
  minDate?: DatepickerProps['minDate'],
  maxDate?: DatepickerProps['maxDate'],
): Array<CalendarDayType> => {
  const firstDateInMonth = dayjs(date).startOf('month');
  const lastDateInMonth = dayjs(date).endOf('month');

  const firstMonday = firstDateInMonth.isoWeekday(1).toDate();
  const lastSunday = lastDateInMonth.isoWeekday(7).toDate();

  const dates: Array<CalendarDayType> = [];
  for (
    let currentDay = firstMonday;
    currentDay < lastSunday;
    currentDay = dayjs(currentDay).add(1, 'day').toDate()
  ) {
    const selectionProperties = selectedValue && getSelectionProperties(currentDay, selectedValue);
    const isDisabled =
      minDate || maxDate ? !dayjs(currentDay).isBetween(minDate, maxDate, 'day', '[]') : undefined;

    dates.push({
      date: currentDay,
      isCurrentMonth: dayjs(currentDay).isBetween(
        firstDateInMonth,
        lastDateInMonth,
        undefined,
        '[]',
      ),
      isCurrentDay: dayjs(currentDay).isSame(new Date(), 'day'),
      isDisabled,
      isSelected: selectionProperties?.isSelected,
      isSelectedBoundary: selectionProperties?.isSelectedBoundary,
      position: selectionProperties?.position,
    });
  }
  return dates;
};

const useDatepicker = ({
  title,
  maxDate,
  minDate,
  multiple,
  autoFocus,
  defaultValue,
  disableCurrentDay,
  onMonthChange,
  onChange,
}: Omit<DatepickerProps, 'shortDaysTranslate' | 'size'>) => {
  const [value, setValue] = useState<typeof defaultValue>(defaultValue);
  const [currentMonth, setCurrentMonth] = useState<Date>(new Date());
  const [currentMonthRange, setCurrentMonthRange] = useState<Array<CalendarDayType>>(
    getCalendarDaysRange(currentMonth, value, minDate, maxDate),
  );

  const datepickerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (onChange && value) {
      onChange(value);
    }

    // Reason: breaks logic of rendering formik that produces to infinity rerenders
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  useEffect(() => {
    setCurrentMonthRange(getCalendarDaysRange(currentMonth, value, minDate, maxDate));

    if (onMonthChange) {
      onMonthChange(currentMonth);
    }
  }, [currentMonth, maxDate, minDate, onMonthChange, value]);

  useEffect(() => {
    if (autoFocus) {
      datepickerRef.current?.focus();
    }
  }, [autoFocus]);

  const paginateMonth = useCallback(
    (direction: 'forward' | 'back') => {
      const prevMonth = dayjs(currentMonth).subtract(1, 'month').toDate();
      const nextMonth = dayjs(currentMonth).add(1, 'month').toDate();

      setCurrentMonth(direction === 'back' ? prevMonth : nextMonth);
    },
    [currentMonth],
  );

  const selectDate = useCallback(
    (selectedDate: Date) => {
      // const isSelectedValueDate = value instanceof Date;
      if (!multiple) {
        return setValue(selectedDate);
      }

      const range = value as Partial<Range>;

      if (!range?.from && !range?.to) {
        setValue({
          from: selectedDate,
          to: undefined,
        });
      } else if (range?.from && !range?.to) {
        const isSame = dayjs(range.from).isSame(selectedDate, 'day');
        const isSelectedAfter = dayjs(selectedDate).isAfter(range.from);

        if (isSame) return setValue({});

        return setValue({
          from: isSelectedAfter ? range.from : selectedDate,
          to: isSelectedAfter ? selectedDate : range.from,
        });
      } else if (range.from && range.to) {
        setValue({ from: selectedDate, to: undefined });
      }
    },
    [multiple, value],
  );

  const getCellState: (
    props: Omit<CalendarDayType, 'isCurrentMonth' | 'date'>,
  ) => CellProps['state'] = useCallback(
    ({ position, isCurrentDay, isSelected, isSelectedBoundary }) => {
      // Single selection
      if (isSelected && !position && !isSelectedBoundary) {
        return 'selected';
      }

      // Multiple selection
      if (isSelected && position && isSelectedBoundary) {
        return 'selected';
      }
      if (isSelected && position && !isSelectedBoundary) {
        return 'default';
      }

      // Current day
      if (!disableCurrentDay && isCurrentDay) {
        return 'current';
      }

      // Default cells
      return 'inactive';
    },
    [disableCurrentDay],
  );

  return {
    title: title(currentMonth),
    datepickerRef,
    currentMonthRange,
    paginateMonth,
    getCellState,
    selectDate,
  };
};

export { useDatepicker };
