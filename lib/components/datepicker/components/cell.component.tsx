import { Text } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './cell.module.scss';
import { CellProps } from './cell.types';

/**
 * Cell component
 * @description Cell button for datepicker component only.
 *
 * @author Rostyslav Nahornyi
 * @category Components
 * @param { CellProps } props - ButtonProps defined in the './cell.types.ts'
 */
const Cell: FC<CellProps> = ({ date, size, position, state, disabled, onClick }) => {
  return (
    <button
      onClick={() => !disabled && onClick && onClick(date)}
      className={classNames(
        styles.wrapper,
        styles[`${state}State`],
        styles[`${size}Size`],
        position && styles[`${position}Position`],
        disabled && styles.disabled,
      )}
      type="button"
    >
      {position !== 'middle' && state === 'selected' ? (
        <div className={styles.selectedWrapper}>
          <Text variant="caption14Regular">{date.getDate()}</Text>
        </div>
      ) : (
        <Text variant="caption14Regular">{date.getDate()}</Text>
      )}
    </button>
  );
};

export { Cell };
