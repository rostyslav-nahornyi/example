/**
 * Props of Cell component.
 *
 * @author Rostyslav Nahornyi
 */
interface CellProps {
  readonly date: Date;
  readonly size: 'small' | 'large';
  readonly position?: 'start' | 'middle' | 'end';
  readonly state: 'inactive' | 'default' | 'selected' | 'current';
  readonly disabled?: boolean;
  readonly onClick?: (date: Date) => void;
}

export type { CellProps };
