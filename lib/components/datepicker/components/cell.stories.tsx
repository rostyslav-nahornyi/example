import { Meta, StoryFn } from '@storybook/react';
import { Cell } from './cell.component';

export default {
  title: 'UI-KIT/Datepicker/Cell',
  component: Cell,
} as Meta<typeof Cell>;

const Template: StoryFn<typeof Cell> = (props) => <Cell {...props} />;

const Inactive = Template.bind({});
Inactive.args = {
  date: new Date(),
  size: 'large',
  position: 'middle',
  state: 'inactive',
  onClick: (date) => console.log(date.toDateString()),
};

const Default = Template.bind({});
Default.args = {
  date: new Date(),
  size: 'large',
  position: 'middle',
  state: 'default',
  onClick: (date) => console.log(date.toDateString()),
};

const Selected = Template.bind({});
Selected.args = {
  date: new Date(),
  size: 'large',
  position: 'middle',
  state: 'selected',
  onClick: (date) => console.log(date.toDateString()),
};

const Current = Template.bind({});
Current.args = {
  date: new Date(),
  size: 'large',
  position: 'middle',
  state: 'current',
  onClick: (date) => console.log(date.toDateString()),
};

export { Current, Default, Inactive, Selected };
