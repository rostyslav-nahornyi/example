import { BaseHTMLAttributes } from 'react';
import { CellProps } from './components/cell.types';

/**
 * Type representing an array of short day names to be used in translations.
 */
type ShortDaysTranslateType = [string, string, string, string, string, string, string];

/**
 * Type representing a date range.
 */
type Range = {
  readonly from: Date;
  readonly to: Date;
};

/**
 * Type representing a value of the Datepicker.
 */
type DatepickerValueType = Date | Partial<Range>;

/**
 * Type representing the properties of a calendar day.
 * Used for the correct rendering.
 */
type CalendarDayType = {
  /**
   * The date of the calendar day.
   */
  readonly date: Date;

  /**
   * Indicates if the day belongs to the current month.
   */
  readonly isCurrentMonth: boolean;

  /**
   * Indicates if the day is the current day.
   */
  readonly isCurrentDay: boolean;

  /**
   * Indicates if the day should be disabled.
   */
  readonly isDisabled?: boolean;

  /**
   * Indicates if the day should be selected.
   */
  readonly isSelected?: boolean;

  /**
   * Indicates if the day is part of the selected range boundary.
   * For blue boundary cells.
   */
  readonly isSelectedBoundary?: boolean;

  /**
   * The position of the cell, if applicable.
   */
  readonly position?: CellProps['position'];
};

/**
 * Props of Datepicker component.
 *
 * @author Rostyslav Nahornyi
 */
interface DatepickerProps extends Pick<BaseHTMLAttributes<HTMLElement>, 'className'> {
  /**
   * The title of the Datepicker.
   */
  readonly title: (middleDate: Date) => string;

  /**
   * An array of short day names to display.
   */
  readonly shortDaysTranslate: ShortDaysTranslateType;

  /**
   * The size.
   */
  readonly size: 'small' | 'large';

  /**
   * Indicates whether multiple dates can be selected.
   */
  readonly multiple: boolean;

  /**
   * If true, the Datepicker will be focused automatically on render.
   */
  readonly autoFocus?: boolean;

  /**
   * The default value.
   * It can be a single date or a range.
   */
  readonly defaultValue?: DatepickerValueType;

  /**
   * Displaying current day.
   * By default false.
   */
  readonly disableCurrentDay?: boolean;

  /**
   * The maximum selectable date.
   */
  readonly maxDate?: Date;

  /**
   * The minimum selectable date.
   */
  readonly minDate?: Date;

  /**
   * Callback function triggered when the selected date(s) change.
   */
  readonly onChange: (value: DatepickerValueType) => void;

  /**
   * Callback function triggered when the month changes.
   */
  readonly onMonthChange?: (date: Date) => void;

  /**
   * A function that can be used to dynamically disable specific dates.
   * TODO
   */
  readonly shouldDisableDates?: (func: (date: ReadonlyArray<Date>) => boolean) => void;
}

export type { CalendarDayType, DatepickerProps, DatepickerValueType, Range };
