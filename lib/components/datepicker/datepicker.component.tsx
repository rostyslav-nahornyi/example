import ChevronLeft from '@assets/icons/chevron-left.svg';
import ChevronRight from '@assets/icons/chevron-right.svg';
import { Button, Text } from '@components';
import classNames from 'classnames';
import { FC } from 'react';
import { Cell } from './components';
import { useDatepicker } from './datepicker.hook';
import styles from './datepicker.module.scss';
import { DatepickerProps } from './datepicker.types';
/**
 * Datepicker component
 * @description Component for selecting date.
 *
 * @author Rostyslav Nahornyi
 * @category Components
 * @param { DatepickerProps } props - ButtonProps defined in the './datepicker.types.ts'
 */
const Datepicker: FC<DatepickerProps> = ({ className, ...props }) => {
  const { datepickerRef, currentMonthRange, title, getCellState, paginateMonth, selectDate } =
    useDatepicker(props);

  return (
    <div
      className={classNames(styles.wrapper, className)}
      ref={datepickerRef}
    >
      <div className={styles.header}>
        <Button
          buttonType="borderless"
          size="s"
          variant="minor"
          leadingIcon={<ChevronLeft style={{ height: '24px', width: '24px' }} />}
          onClick={() => paginateMonth('back')}
        />
        <Text variant="caption16Medium">{title}</Text>
        <Button
          buttonType="borderless"
          size="s"
          variant="minor"
          leadingIcon={<ChevronRight />}
          onClick={() => paginateMonth('forward')}
        />
      </div>

      <div className={styles.calendar}>
        <div className={styles.daysNaming}>
          {props.shortDaysTranslate.map((day, index) => (
            <div
              key={index}
              className={classNames(styles.dayCell, styles[props.size])}
            >
              <Text variant="caption16Medium">{day}</Text>
            </div>
          ))}
        </div>

        <div className={styles.days}>
          {currentMonthRange.map((cell) => (
            <Cell
              key={cell.date.valueOf()}
              date={cell.date}
              size={props.size}
              state={getCellState(cell)}
              disabled={cell.isDisabled || !cell.isCurrentMonth}
              position={cell.isSelected ? cell.position ?? 'middle' : undefined}
              onClick={selectDate}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export { Datepicker };
