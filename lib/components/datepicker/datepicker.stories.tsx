import { Meta, StoryFn } from '@storybook/react';
import dayjs from 'dayjs';
import { useState } from 'react';
import { Button } from '../button';
import { Datepicker } from './datepicker.component';
import { DatepickerValueType } from './datepicker.types';

export default {
  title: 'UI-KIT/Datepicker',
  component: Datepicker,
} as Meta<typeof Datepicker>;

const Template: StoryFn<typeof Datepicker> = (props) => {
  const [value, setValue] = useState<DatepickerValueType>();

  const isRange = value instanceof Date;

  return (
    <>
      <Button
        buttonType="primary"
        size="s"
        variant="main"
        label={isRange ? value.toISOString() : JSON.stringify(value)}
        style={{ marginBottom: 30 }}
      />
      <Datepicker
        {...props}
        title={(date) => dayjs(date).format('MMMM YYYY')}
        onChange={setValue}
      />
    </>
  );
};

const MultipleSelection = Template.bind({});
MultipleSelection.args = {
  shortDaysTranslate: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  multiple: true,
  size: 'large',
  defaultValue: {
    from: dayjs().subtract(7, 'day').toDate(),
    to: dayjs().subtract(1, 'day').toDate(),
  },
};

const SingleSelection = Template.bind({});
SingleSelection.args = {
  shortDaysTranslate: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  size: 'large',
  defaultValue: dayjs().add(3, 'day').toDate(),
};

export { MultipleSelection, SingleSelection };
