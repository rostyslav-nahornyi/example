import { PropsWithChildren } from 'react';

/**
 * PortalProps
 * @description Props for the Portal component
 *
 */
interface PortalProps extends PropsWithChildren {
  readonly domNode: string;
}

export type { PortalProps };
