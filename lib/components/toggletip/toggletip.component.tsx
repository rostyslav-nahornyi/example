import { FC, ReactElement, cloneElement, useState } from 'react';
import { ThemeEnum, ToggleTipProps } from './toggletip.types';

import CloseIcon from '@assets/icons/times.svg';
import { Button } from '@components';
import classNames from 'classnames';
import { usePopper } from 'react-popper';
import styles from './toggletip.module.scss';

/**
 * ToggleTip component
 * @description ToggleTip
 *
 * @author Sergii Goncharuk
 * @category Components
 * @param { ToggleTipProps } props - ToggleTipProps defined in the './toggletip.types.ts'
 */
const ToggleTip: FC<ToggleTipProps> = ({
  title,
  content,
  parent,
  buttonPrimaryTitle,
  buttonPrimaryClickHandler,
  buttonSecondaryTitle,
  buttonSecondaryClickHandler,
  url,
  zIndex = 70,
  noArrow = false,
  theme = ThemeEnum.light,
}) => {
  const [referenceElement, setReferenceElement] = useState<HTMLButtonElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);
  const [arrowElement, setArrowElement] = useState<HTMLDivElement | null>(null);
  const [isToggleTipVisible, setIsToggleTipVisible] = useState<boolean>(false);

  const { styles: popperStyles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [{ name: 'arrow', options: { element: arrowElement } }],
  });

  return (
    <>
      {parent &&
        cloneElement(parent as ReactElement, {
          onClick: () => setIsToggleTipVisible(true),
          ref: setReferenceElement,
        })}
      {isToggleTipVisible && (
        <div
          role="tooltip"
          className={classNames(
            styles.host,
            popperStyles.popper,
            attributes.popper?.['data-popper-placement'] === 'bottom' && styles.hostArrowBottom,
            attributes.popper?.['data-popper-placement']?.startsWith('top') && styles.hostArrowTop,
            attributes.popper?.['data-popper-placement']?.startsWith('left') &&
              styles.hostArrowLeft,
            attributes.popper?.['data-popper-placement']?.startsWith('right') &&
              styles.hostArrowRight,
          )}
          style={{ ...popperStyles.popper, zIndex }}
          ref={setPopperElement}
          {...attributes.popper}
        >
          {!noArrow && popperStyles && (
            <div
              className={classNames(
                styles.arrow,
                styles[`arrow--${theme}`],
                attributes.popper?.['data-popper-placement'] === 'bottom' && styles.arrowBottom,
                attributes.popper?.['data-popper-placement']?.startsWith('top') && styles.arrowTop,
                attributes.popper?.['data-popper-placement']?.startsWith('left') &&
                  styles.arrowLeft,
                attributes.popper?.['data-popper-placement']?.startsWith('right') &&
                  styles.arrowRight,
              )}
              style={popperStyles.arrow}
              ref={setArrowElement}
            />
          )}
          <div className={classNames(styles.content, styles[`content--${theme}`])}>
            <div className={styles.main}>
              <div className={styles.header}>
                <span className={styles[`title-${theme}`]}>{title}</span>
                <button
                  className={styles.closeBtn}
                  onClick={() => setIsToggleTipVisible(!isToggleTipVisible)}
                >
                  <CloseIcon className={styles[`svg--${theme}`]} />
                </button>
              </div>
              {content &&
                (url ? (
                  <a
                    href={url}
                    className={styles.link}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {content}
                  </a>
                ) : (
                  <p className={classNames(styles.text, styles[`text--${theme}`])}>{content}</p>
                ))}
            </div>
            <div className={styles.btns}>
              {buttonPrimaryTitle && buttonPrimaryClickHandler && (
                <Button
                  className={styles.btn}
                  size="s"
                  variant="main"
                  buttonType="primary"
                  label={buttonPrimaryTitle}
                  onClick={() => {
                    setIsToggleTipVisible(!isToggleTipVisible);
                    buttonPrimaryClickHandler();
                  }}
                />
              )}
              {buttonSecondaryTitle &&
                buttonSecondaryClickHandler &&
                (theme === ThemeEnum.dark ? (
                  <Button
                    className={styles.btn}
                    size="s"
                    variant="main"
                    buttonType="secondary"
                    label={buttonSecondaryTitle}
                    onClick={() => {
                      buttonSecondaryClickHandler();
                      setIsToggleTipVisible(!isToggleTipVisible);
                    }}
                  />
                ) : (
                  <Button
                    className={styles.btn}
                    size="s"
                    variant="minor"
                    buttonType="outlined"
                    label={buttonSecondaryTitle}
                    onClick={() => {
                      buttonSecondaryClickHandler();
                      setIsToggleTipVisible(!isToggleTipVisible);
                    }}
                  />
                ))}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

ToggleTip.displayName = 'ToggleTip';

export { ToggleTip };
