import { ComponentProps, ReactElement } from 'react';

/**
 * Props of ToggleTip component.
 *
 * @author Sergii Goncharuk
 */
export enum ThemeEnum {
  light = 'light',
  dark = 'dark',
}

type ToggleTipProps = {
  /**
   * Title.
   */
  readonly title: string;

  /**
   * Content.
   */
  readonly content?: string;

  /**
   * Content url. If link present, content will be an anchor element.
   */
  readonly url?: string;

  /**
   * Primary button title.
   */
  readonly buttonPrimaryTitle?: string;

  /**
   * Primary button onClick handler.
   */
  readonly buttonPrimaryClickHandler?: () => unknown;

  /**
   * Secondary button title.
   */
  readonly buttonSecondaryTitle?: string;

  /**
   * Secondary button onClick handler.
   */
  readonly buttonSecondaryClickHandler?: () => unknown;

  /**
   * z-index
   * default value = 60
   */
  readonly zIndex?: number;

  /**
   * toggletip theme
   */
  readonly theme?: ThemeEnum;

  /**
   * specifies to show arrow or not
   */
  readonly noArrow?: boolean;

  /**
   * Parent element
   */
  readonly parent?: ReactElement;
} & ComponentProps<'div'>;

export type { ToggleTipProps };
