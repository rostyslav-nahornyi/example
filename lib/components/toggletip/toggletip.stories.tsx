import InfoCircle from '@assets/icons/info-circle.svg';
import { action } from '@storybook/addon-actions';
import { Meta, Story } from '@storybook/react';
import { ToggleTip } from './toggletip.component';
import { ThemeEnum, ToggleTipProps } from './toggletip.types';

export default {
  title: 'UI-KIT/Toggletip',
  component: ToggleTip,
} as Meta;

const centerDecorator = (Story: Story) => (
  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
    <Story />
  </div>
);

const rightDecorator = (Story: Story) => (
  <div style={{ display: 'flex', justifyContent: 'right', alignItems: 'flex-end', height: '90vh' }}>
    <Story />
  </div>
);

const ToggleTipTemplate: Story<ToggleTipProps> = (args) => <ToggleTip {...args} />;

const Primary = ToggleTipTemplate.bind({});
Primary.args = {
  title: 'Toggletip title goes here',
  content:
    'A toggletip is used on click or enter when you must expose interactive elements, such as a button, that a user needs to interact with.',
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};

const Dark = ToggleTipTemplate.bind({});
Dark.args = {
  title: 'Toggletip title goes here',
  content:
    'A toggletip is used on click or enter when you must expose interactive elements, such as a button, that a user needs to interact with.',
  theme: ThemeEnum.dark,
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};

const PrimaryLink = ToggleTipTemplate.bind({});
PrimaryLink.args = {
  title: 'Toggletip title goes here',
  content: 'Label',
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  url: 'https://www.google.com',
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};

const DarkCenter = ToggleTipTemplate.bind({});
DarkCenter.args = {
  title: 'Toggletip title goes here',
  content:
    'A toggletip is used on click or enter when you must expose interactive elements, such as a button, that a user needs to interact with.',
  theme: ThemeEnum.dark,
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};
DarkCenter.decorators = [centerDecorator];

const PrimaryRight = ToggleTipTemplate.bind({});
PrimaryRight.args = {
  title: 'Toggletip title goes here',
  content:
    'A toggletip is used on click or enter when you must expose interactive elements, such as a button, that a user needs to interact with.',
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};
PrimaryRight.decorators = [rightDecorator];

const NoArrow = ToggleTipTemplate.bind({});
NoArrow.args = {
  title: 'Toggletip title goes here',
  content:
    'A toggletip is used on click or enter when you must expose interactive elements, such as a button, that a user needs to interact with.',
  noArrow: true,
  parent: (
    <button
      style={{ border: 'none', background: 'inherit', display: 'flex', alignItems: 'center' }}
    >
      <InfoCircle />
    </button>
  ),
  buttonPrimaryTitle: 'Label 1',
  buttonPrimaryClickHandler: action('Btn 1 clicked'),
  buttonSecondaryTitle: 'Label 2',
  buttonSecondaryClickHandler: action('Btn 2 clicked'),
};

export { Dark, DarkCenter, NoArrow, Primary, PrimaryLink, PrimaryRight };
