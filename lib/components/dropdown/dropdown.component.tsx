import { FC, ReactNode, cloneElement, useRef, useState } from 'react';
import { DropdownProps } from './dropdown.types';

import { useClickOutside } from '@hooks';
import classNames from 'classnames';
import { usePopper } from 'react-popper';
import { DropdownMenu } from './components/dropdown-menu';
import styles from './dropdown.module.scss';

/**
 * Dropdown component
 * @description Dropdown
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { DropdownProps } props - DropdownProps defined in the './dropdown.types.ts'
 */
const Dropdown: FC<DropdownProps> = ({
  button,
  textField,
  customMenu,
  expand = false,
  options,
  ...menuProps
}) => {
  const [isOpened, setIsOpened] = useState(false);
  const [textLabelValue, setTextLabelValue] = useState<string>();
  const [leadingIcon, setLeadingIcon] = useState<ReactNode>();
  const [filteredOptions, setFilteredOptions] = useState(options);
  const referenceElement = useRef<HTMLDivElement>(null);

  useClickOutside(referenceElement, () => {
    setIsOpened(false);
  });

  const handleClick = () => {
    setIsOpened(!isOpened);
  };

  const handleSearch = (value: string) => {
    setFilteredOptions(
      options?.filter((item) => item.value.toLocaleLowerCase().includes(value.toLocaleLowerCase())),
    );
  };

  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);

  const { styles: popperStyles, attributes } = usePopper(referenceElement.current, popperElement, {
    placement: 'bottom-start',
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 8],
        },
      },
      {
        name: 'preventOverflow',
        options: {
          boundary: 'clippingParents',
          padding: 0,
        },
      },
    ],
  });

  return (
    <div
      className={classNames(styles.wrapper, menuProps.className, expand ? styles.expand : '')}
      ref={referenceElement}
    >
      {button &&
        cloneElement(button, {
          ...button.props,
          style: { ...button.props.style, cursor: 'pointer' },
          onClick: handleClick,
          isOpened: isOpened,
          selectedValue: textLabelValue,
          expand,
        })}
      {textField &&
        cloneElement(textField, {
          ...textField.props,
          style: { ...textField.props.style, cursor: 'pointer' },
          onClick: () => {
            setLeadingIcon(null);
            setTextLabelValue('');
            handleClick();
            setFilteredOptions(options);
          },
          onSearch: handleSearch,
          isOpened: isOpened,
          icon: leadingIcon,
          value: textField.props.value || textLabelValue,
          expand,
        })}

      {!customMenu ? (
        <DropdownMenu
          menuRef={referenceElement.current}
          className={styles.menu}
          {...{ options: filteredOptions, ...menuProps }}
          isOpened={isOpened}
          onSelect={(currentItemId) => {
            const { value, leadingIcon, id } =
              filteredOptions?.find((item) => item.id === currentItemId) ?? {};

            setTextLabelValue(value ?? '');
            setLeadingIcon(leadingIcon ?? null);
            setIsOpened(!isOpened);
            id && menuProps.onSelect(id);
          }}
        />
      ) : (
        isOpened && (
          <div
            className={styles.customWrapper}
            ref={setPopperElement}
            role="tooltip"
            style={{ ...popperStyles.popper }}
            {...attributes.popper}
          >
            {cloneElement(customMenu, {
              ...customMenu.props,
            })}
          </div>
        )
      )}
    </div>
  );
};

export { Dropdown };
