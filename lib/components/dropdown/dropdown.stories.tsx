import { action } from '@storybook/addon-actions';
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import AvatarIcon from '@assets/icons/avatar.svg';
import Circle from '@assets/icons/circle.svg';
import { Datepicker } from '@components/datepicker/datepicker.component';
import dayjs from 'dayjs';
import { DatepickerValueType } from '../datepicker/datepicker.types';
import { DropdownButton } from './components/dropdown-button/dropdown-button.component';
import { DropdownField } from './components/dropdown-field/dropdown-field.component';

import { Dropdown } from './dropdown.component';

export default {
  title: 'UI-KIT/Dropdown/Dropdown',
  component: Dropdown,
} as Meta<typeof Dropdown>;

const Default: StoryFn<typeof Dropdown> = () => {
  return (
    <Dropdown
      button={
        <DropdownButton
          placeholder="Some title"
          changable
        />
      }
      hideScrollbar
      onSelect={(value) => action('selected')(value)}
      options={[
        {
          id: '0',
          value: 'Item 1',
          leadingIcon: <Circle />,
          onSelect: (id) => action('selected')(id),
        },
        { id: '1', value: 'Item 2', isSelected: true, onSelect: (id) => action('selected')(id) },
        { id: '2', value: 'Item 4', onSelect: (id) => action('selected')(id) },
        {
          id: '3',
          value: 'Item 4',
          isAvatar: true,
          leadingIcon: <AvatarIcon />,
          onSelect: (id) => action('selected')(id),
        },
        {
          id: '4',
          value: 'Item 5',
          leadingIcon: <Circle />,
          onSelect: (id) => action('selected')(id),
        },
      ]}
    />
  );
};

const Field: StoryFn<typeof DropdownButton> = () => {
  return (
    <Dropdown
      textField={<DropdownField placeholder="pe-pe" />}
      hideScrollbar
      onSelect={(value) => action('selected')(value)}
      options={[
        {
          id: '0',
          value: 'Item 1',
          leadingIcon: <Circle />,
          onSelect: (id) => action('selected')(id),
        },
        { id: '1', value: 'Item 2', isSelected: true },
        { id: '2', value: 'Item 4' },
        { id: '3', value: 'Item 4', isAvatar: true, leadingIcon: <AvatarIcon /> },
        { id: '4', value: 'Item 5', leadingIcon: <Circle /> },
      ]}
    />
  );
};

const Custom: StoryFn<typeof DropdownButton> = () => {
  const [value, setValue] = useState<DatepickerValueType>();

  const date =
    value instanceof Date
      ? value.toLocaleDateString()
      : `${value?.from?.toLocaleDateString() ?? ''}${value?.to ? ' - ' : ''}${value?.to?.toLocaleDateString() ?? ''}`;

  return (
    <Dropdown
      textField={
        <DropdownField
          value={date}
          placeholder="bla"
        />
      }
      hideScrollbar
      onSelect={(value) => action('selected')(value)}
      customMenu={
        <Datepicker
          title={(date) => dayjs(date).format('MMMM YYYY')}
          onChange={setValue}
          shortDaysTranslate={['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']}
          multiple
          size="large"
          defaultValue={{
            from: dayjs().subtract(7, 'day').toDate(),
            to: dayjs().subtract(1, 'day').toDate(),
          }}
        />
      }
    />
  );
};

export { Custom, Default, Field };
