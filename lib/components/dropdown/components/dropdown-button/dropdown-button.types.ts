/**
 * Props of DropdownButton component.
 *
 * @author Vitalii Bodnarchuk
 */

import { HTMLAttributes, PropsWithChildren, ReactNode } from 'react';

interface DropdownButtonProps
  extends PropsWithChildren,
    Pick<HTMLAttributes<HTMLDivElement>, 'className'> {
  /**
   * leadingIcon
   */
  readonly icon?: ReactNode;
  /**
   * starting text
   */
  readonly placeholder?: string;
  /**
   * disable dropdown button
   */
  readonly disabled?: boolean;

  /**
   * another type of dropdown button
   */
  readonly borderless?: boolean;

  /**
   * remove chevron for trailing icon
   */
  readonly hideChevron?: boolean;

  /**
   * should change title
   */
  readonly changable?: boolean;

  /**
   * expand
   */
  readonly expand?: boolean;

  /**
   * boolean value specifies dropdown to be open or not
   */
  readonly isOpened?: boolean;

  /**
   * callback function
   */
  readonly onClick?: () => void;

  /**
   * selected value
   */
  readonly selectedValue?: string;
}

export type { DropdownButtonProps };
