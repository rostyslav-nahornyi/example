import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import DotsVertical from '@assets/icons/dots-vertical.svg';
import { DropdownButton } from './dropdown-button.component';

export default {
  title: 'UI-KIT/Dropdown/DropdownButton',
  component: DropdownButton,
} as Meta<typeof DropdownButton>;

const Default: StoryFn<typeof DropdownButton> = () => {
  const [isOpened, setIsOpened] = useState(false);

  return (
    <div>
      <DropdownButton
        placeholder="Label"
        isOpened={isOpened}
        onClick={() => setIsOpened(!isOpened)}
      />
    </div>
  );
};

const WithIcon: StoryFn<typeof DropdownButton> = () => {
  const [isOpened, setIsOpened] = useState(false);
  return (
    <div>
      <DropdownButton
        icon={<DotsVertical />}
        isOpened={isOpened}
        hideChevron={true}
        borderless={true}
        onClick={() => setIsOpened(!isOpened)}
      />
    </div>
  );
};

export { Default, WithIcon };
