import ChevronDown from '@assets/icons/chevron-down.svg';
import classNames from 'classnames';
import { Button } from '../../../button';
import classes from './dropdown-button.module.scss';
import { DropdownButtonProps } from './dropdown-button.types';

/**
 * DropdownButton component
 * @description DropdownButton
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { DropdownButtonProps } props - DropdownButtonProps defined in the './dropdown-button.types.ts'
 */
const DropdownButton = ({
  icon,
  placeholder,
  changable = false,
  expand = false,
  onClick,
  disabled,
  isOpened,
  selectedValue,
  borderless = false,
  hideChevron,
}: DropdownButtonProps) => (
  <div className={classNames(classes.wrapper, expand ? classes.expand : '')}>
    <div className={classes.dropdown}>
      <Button
        size="m"
        variant={borderless ? 'minor' : 'main'}
        buttonType={borderless ? 'borderless' : 'primary'}
        disabled={disabled}
        onClick={onClick}
        className={classNames(classes.selector, isOpened && classes.selectorWithOpenedMenu)}
        leadingIcon={icon}
        label={changable ? selectedValue ?? placeholder : placeholder}
        trailingIcon={
          !hideChevron ? (
            <ChevronDown className={classNames(isOpened ? classes.on : classes.off)} />
          ) : null
        }
      />
    </div>
  </div>
);

export { DropdownButton };
