import { useEffect, useState } from 'react';

import ChevronDown from '@assets/icons/chevron-down.svg';
import classNames from 'classnames';
import { TextField } from '../../../text-field';
import classes from './dropdown-field.module.scss';
import { DropdownFieldProps } from './dropdown-field.types';

/**
 * DropdownField component
 * @description DropdownField
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { DropdownFieldProps } props - DropdownFieldProps defined in the './dropdown-field.types.ts'
 */
const DropdownField = ({
  icon,
  placeholder,
  disabled,
  hideChevron,
  label,
  onClick,
  isOpened,
  onSearch,
  value,
  className,
}: DropdownFieldProps) => {
  const [searchTerm, setSearchTerm] = useState<string>();

  const handleSearchChange = (value: string) => {
    setSearchTerm(value);
    if (onSearch) onSearch(value);
  };

  useEffect(() => {
    setSearchTerm('');
  }, [value]);

  return (
    <div className={classNames(classes.wrapper)}>
      <div className={(classes.dropdown, className)}>
        <TextField
          disabled={disabled}
          placeholder={placeholder}
          onClick={() => {
            setSearchTerm('');
            onClick && onClick();
          }}
          className={classNames(classes.selector, isOpened && classes.selectorWithOpenedMenu)}
          leadingIcon={icon}
          label={label}
          value={searchTerm || value || ''}
          onChange={handleSearchChange}
          tailingIcon={
            !hideChevron ? (
              <ChevronDown
                onClick={onClick}
                className={isOpened ? classes.on : classes.off}
              />
            ) : null
          }
        />
      </div>
    </div>
  );
};

export { DropdownField };
