import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { DropdownButton } from '../dropdown-button';
import { DropdownField } from './dropdown-field.component';

export default {
  title: 'UI-KIT/Dropdown/DropdownField',
  component: DropdownButton,
} as Meta<typeof DropdownButton>;

const Default: StoryFn<typeof DropdownButton> = () => {
  const [isOpened, setIsOpened] = useState(false);

  return (
    <div>
      <DropdownField
        placeholder="Label"
        isOpened={isOpened}
        onClick={() => {
          setIsOpened(!isOpened);
        }}
        disabled={false}
      />
    </div>
  );
};

export { Default };
