import { HTMLAttributes, PropsWithChildren } from 'react';

/**
 * Props of DropdownField component.
 *
 * @author Vitalii Bodnarchuk
 */
interface DropdownFieldProps
  extends PropsWithChildren,
    Pick<HTMLAttributes<HTMLDivElement>, 'style' | 'className'> {
  /**
   * label
   */
  readonly label?: string;

  /**
   * value
   */
  readonly value?: string;

  /**
   * leadingIcon
   */
  readonly icon?: JSX.Element;
  /**
   *  starting text
   */
  readonly placeholder?: string;
  /**
   * disable dropdown button
   */
  readonly disabled?: boolean;

  /**
   * remove chevron for trailing icon
   */
  readonly hideChevron?: boolean;

  /**
   * boolean value specifies dropdown to be open or not
   */
  readonly isOpened?: boolean;

  /**
   * callback function
   */
  readonly onClick?: () => void;

  /**
   * Search functionality
   */
  readonly onSearch?: (searchTerm: string) => void;
}

export type { DropdownFieldProps };
