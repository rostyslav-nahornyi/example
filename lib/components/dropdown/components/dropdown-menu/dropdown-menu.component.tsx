import classNames from 'classnames';
import { FC, useCallback, useEffect, useState } from 'react';
import { usePopper } from 'react-popper';

import { Text } from '@components';
import { DropdownItem } from './components';
import styles from './dropdown-menu.module.scss';
import { DropdownMenuProps } from './dropdown-menu.types';

/**
 * DropdownMenu component
 * @description Menu of dropdown items
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { DropdownMenuProps } props - DropdownMenuProps defined in the './dropdown-menu-.types.ts'
 */
const DropdownMenu: FC<DropdownMenuProps> = ({
  options = [],
  style,
  className,
  hideScrollbar,
  maxHeight,
  menuRef,
  isOpened,
  placement = 'bottom-start',
  onSelect,
  updatePopper,
}) => {
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);

  const {
    styles: popperStyles,
    attributes,
    update,
  } = usePopper(menuRef, popperElement, {
    placement: placement,
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 8],
        },
      },
      {
        name: 'preventOverflow',
        options: {
          boundary: 'clippingParents',
          padding: 0,
        },
      },
    ],
  });

  const updatePopperPosition = useCallback(() => {
    update && update();
  }, [update]);

  useEffect(() => {
    if (updatePopper) {
      updatePopper(updatePopperPosition);
    }
  }, [updatePopper, updatePopperPosition]);

  return (
    <div
      ref={setPopperElement}
      role="tooltip"
      className={classNames(
        styles.wrapper,
        hideScrollbar && styles.hideScrollbar,
        !menuRef ? styles.noVisible : styles.isVisible,
      )}
      style={{ ...popperStyles.popper, maxHeight }}
      {...attributes.popper}
    >
      {isOpened ? (
        <div
          className={classNames(styles.dropdownMenu, className)}
          style={{ ...style, maxHeight }}
        >
          {options?.length > 0 ? (
            options?.map(
              ({
                id,
                value,
                leadingIcon,
                disabled,
                isAvatar,
                isSelected,
                subText,
                onSelect: optionCallback,
                isHidden,
              }) => (
                <DropdownItem
                  key={id}
                  id={id}
                  value={value}
                  leadingIcon={leadingIcon}
                  isAvatar={isAvatar}
                  isSelected={isSelected}
                  className={classNames(styles.dropdownItem)}
                  onSelect={(value) => {
                    !disabled && onSelect(value);
                    optionCallback && optionCallback(id);
                  }}
                  disabled={disabled}
                  subText={subText}
                  isHidden={isHidden}
                />
              ),
            )
          ) : (
            <Text
              variant="caption14Medium"
              className={styles.notFound}
            >
              Not found
            </Text>
          )}
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export { DropdownMenu };
