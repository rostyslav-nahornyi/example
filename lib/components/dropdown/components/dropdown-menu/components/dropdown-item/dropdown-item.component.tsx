import { FC } from 'react';

import Check from '@assets/icons/check.svg';
import { Avatar, Text } from '@components';
import classNames from 'classnames';
import styles from './dropdown-item.module.scss';
import { DropdownItemProps } from './dropdown-item.types';

/**
 * DropdownItem component
 * @description dropdown item for DropdownMenu and DropdownButton
 *
 * @category Components
 * @param { DropdownItemProps } props - DropdownItemProps defined in the './dropdown-item.types.ts'
 */
const DropdownItem: FC<DropdownItemProps> = ({
  id,
  value,
  leadingIcon,
  isSelected,
  className,
  style,
  disabled,
  isAvatar,
  subText,
  onSelect,
  isHidden,
}) =>
  !isHidden ? (
    <div
      role={'button'}
      tabIndex={0}
      onKeyDown={(e) => {
        if (!disabled && e.code === 'Enter' && id && onSelect) {
          onSelect(id);
        }
      }}
      className={classNames(
        styles.dropdownItem,
        isSelected && styles.isSelected,
        disabled && styles.disabled,
        isAvatar && styles.avatarItem,
        className,
      )}
      style={style}
      onClick={() => {
        onSelect && onSelect(id);
      }}
    >
      <div className={styles.block}>
        {leadingIcon && !isAvatar && <div className={styles.leadingIcon}>{leadingIcon}</div>}
        {isAvatar && (
          <Avatar
            statusIndicatorPosition="off"
            className={styles.avatarSize}
            size={'s'}
            img={leadingIcon}
          />
        )}
        {!isAvatar ? (
          <Text
            variant="caption14Medium"
            className={styles.value}
          >
            {value}
          </Text>
        ) : (
          <div className={styles.avatarBlock}>
            <Text
              variant="caption14Medium"
              className={styles.value}
            >
              {value}
            </Text>
            <Text
              variant="caption14Regular"
              className={styles.subText}
            >
              {subText}
            </Text>
          </div>
        )}
      </div>
      {isSelected && <Check className={styles.check} />}
    </div>
  ) : null;

export { DropdownItem };
