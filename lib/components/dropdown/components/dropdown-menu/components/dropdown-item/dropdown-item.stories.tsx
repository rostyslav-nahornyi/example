import { Meta, Story } from '@storybook/react';

import AvatarIcon from '@assets/icons/avatar.svg';
import Circle from '@assets/icons/circle.svg';
import { Indicator } from '../../../../../indicator';
import { DropdownItem } from './dropdown-item.component';
import { DropdownItemProps } from './dropdown-item.types';

export default {
  title: 'UI-KIT/Dropdown/DropdownMenu/DropdownItem',
  component: DropdownItem,
} as Meta;

const Template: Story<DropdownItemProps> = (args) => <DropdownItem {...args} />;

const Default = Template.bind({});
Default.args = {
  id: 'default-item',
  value: 'Text',
};

const WithIcon = Template.bind({});
WithIcon.args = {
  ...Default.args,
  leadingIcon: <Circle />,
  isAvatar: false,
};

const WithIndicator = Template.bind({});
WithIndicator.args = {
  ...Default.args,
  leadingIcon: (
    <Indicator
      size="s"
      type="success"
    />
  ),
};

const WithAvatar = Template.bind({});
WithAvatar.args = {
  ...Default.args,
  isAvatar: true,
  leadingIcon: <AvatarIcon />,
  subText: 'Text',
  isSelected: true,
};

const Selected = Template.bind({});
Selected.args = {
  ...Default.args,
  leadingIcon: <Circle />,
  isSelected: true,
};

const Disabled = Template.bind({});
Disabled.args = {
  ...Default.args,
  disabled: true,
};

export { Default, Disabled, Selected, WithAvatar, WithIcon, WithIndicator };
