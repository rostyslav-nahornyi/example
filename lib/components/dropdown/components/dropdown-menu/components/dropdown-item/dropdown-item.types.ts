import { HTMLAttributes, ReactNode } from 'react';

/**
 * Props of DropdownItem component.
 *
 * @author Vitalii Bodnarchuk
 */
interface DropdownItemProps extends Pick<HTMLAttributes<HTMLDivElement>, 'style' | 'className'> {
  /**
   * The unique identifier of the option.
   */
  readonly id: string;

  /**
   * The display value of the option.
   */
  readonly value: string;

  /**
   * Left item icon.
   */
  readonly leadingIcon?: ReactNode;

  /**
   * Specifies the dropdown is selected.
   */
  readonly isSelected?: boolean;

  /**
   * Called when dropdown item was selected.
   */
  readonly onSelect?: (optionId: string) => void;

  /**
   * Disabled
   */
  readonly disabled?: boolean;

  /**
   * Specifies the structure with avatar
   */
  readonly isAvatar?: boolean;

  /**
   * subText of avatar block
   */
  readonly subText?: string;

  /**
   * display item
   */
  readonly isHidden?: boolean;
}

export type { DropdownItemProps };
