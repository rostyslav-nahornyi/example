import { Placement } from '@popperjs/core';
import { HTMLAttributes } from 'react';
import { DropdownItemProps } from './components/dropdown-item/dropdown-item.types';

/**
 * Props of DropdownMenu component.
 *
 * @author Vitalii Bodnarchuk
 */
interface DropdownMenuProps extends Pick<HTMLAttributes<HTMLDivElement>, 'className' | 'style'> {
  /**
   *  The dropdown options.
   */
  readonly options?: ReadonlyArray<DropdownItemProps>;

  /**
   * Specifies the dropdown is opened.
   */
  readonly isOpened?: boolean;

  /**
   * Specifies the dropdown is multiple or single.
   */
  readonly multiple?: boolean;

  /**
   * Specifies if the dropdown menu should be limited by height in px.
   */
  readonly maxHeight?: number;

  /**
   * Scrollable or not
   */
  readonly hideScrollbar?: boolean;

  /**
   * Root element ref
   */
  readonly menuRef: HTMLDivElement | null;

  /**
   * Popper placement.
   */
  readonly placement?: Placement;

  /**
   * Select handler
   */
  readonly onSelect: (value: string) => void;

  /**
   * Update popper trigger
   */
  readonly updatePopper?: (updatePopperPosition: () => void) => void;
}

export type { DropdownMenuProps };
