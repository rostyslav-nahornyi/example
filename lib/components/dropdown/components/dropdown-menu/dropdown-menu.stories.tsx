import { Meta, StoryFn } from '@storybook/react';

import AvatarIcon from '@assets/icons/avatar.svg';
import Circle from '@assets/icons/circle.svg';
import { DropdownMenu } from './dropdown-menu.component';
import type { DropdownMenuProps } from './dropdown-menu.types';

export default {
  title: 'UI-KIT/Dropdown/DropdownMenu',
  component: DropdownMenu,
} as Meta<typeof DropdownMenu>;

const Template: StoryFn<typeof DropdownMenu> = (props: DropdownMenuProps) => (
  <DropdownMenu
    {...props}
    isOpened={true}
  />
);

const WithoutScrollbar = Template.bind({});
WithoutScrollbar.args = {
  multiple: true,
  hideScrollbar: true,
  options: [
    { id: '0', value: 'Item 1', leadingIcon: <Circle /> },
    { id: '1', value: 'Item 2' },
    { id: '2', value: 'Item 3' },
    { id: '3', value: 'Item 4', isAvatar: true, leadingIcon: <AvatarIcon /> },
    { id: '4', value: 'Item 5', leadingIcon: <Circle /> },
  ],
};

const WithScrollbar = Template.bind({});
WithScrollbar.args = {
  multiple: true,
  hideScrollbar: false,
  options: [
    { id: '0', value: 'Item 1', leadingIcon: <Circle /> },
    { id: '1', value: 'Item 2' },
    { id: '2', value: 'Item 3' },
    {
      id: '3',
      value: 'Item 4',
      subText: 'Title',
      isAvatar: true,
      leadingIcon: <AvatarIcon />,
    },
    { id: '4', value: 'Item 5', leadingIcon: <Circle /> },
    { id: '5', value: 'Item 6', leadingIcon: <Circle /> },
    { id: '6', value: 'Item 7', leadingIcon: <Circle /> },
    { id: '7', value: 'Item 8', leadingIcon: <Circle /> },
    { id: '8', value: 'Item 9', leadingIcon: <Circle /> },
    { id: '9', value: 'Item 10', leadingIcon: <Circle /> },
    { id: '10', value: 'Item 11', leadingIcon: <Circle /> },
    { id: '11', value: 'Item 12', leadingIcon: <Circle /> },
  ],
};

export { WithScrollbar, WithoutScrollbar };
