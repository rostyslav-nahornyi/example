import { DropdownMenuProps } from './components/dropdown-menu/dropdown-menu.types';

/**
 * Props of Dropdown component.
 *
 * @author Vitalii Bodnarchuk
 */
interface DropdownProps extends Omit<DropdownMenuProps, 'isOpened' | 'menuRef'> {
  /**
   * Specifies the dropdown menu toggle button element
   */
  readonly button?: JSX.Element;

  /**
   * Specifies the dropdown TextField element
   */
  readonly textField?: JSX.Element;

  /**
   * Custom menu
   */
  readonly customMenu?: JSX.Element;

  /**
   * Expand (width 100%)
   */
  readonly expand?: boolean;
}

interface Option {
  /**
   * Specifies the dropdown menu value
   */
  readonly value: string | number;
  /**
   * Specifies the dropdown menu label
   */
  readonly label: string;
  /**
   * Disables the option
   */
  readonly disabled?: boolean;
  /**
   * Callback for option item
   */
  readonly onClick?: () => void;
}

export type { DropdownProps, Option };
