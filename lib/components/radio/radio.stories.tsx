import { Meta, StoryFn } from '@storybook/react';
import { Radio } from './radio.component';
import { RadioTypes } from './radio.types';

export default {
  title: 'UI-KIT/Radio',
  component: Radio,
} as Meta<typeof Radio>;

const Template: StoryFn<typeof Radio> = (args: RadioTypes) => <Radio {...args} />;

const UnSelected = Template.bind({});
UnSelected.args = {
  name: 'radio',
};

const Selected = Template.bind({});
Selected.args = {
  checked: true,
};

const WithText = Template.bind({});
WithText.args = {
  text: 'Label',
};

const WithHelpText = Template.bind({});
WithHelpText.args = {
  text: 'Label',
  helpText: 'Example body text',
};

export { Selected, UnSelected, WithHelpText, WithText };
