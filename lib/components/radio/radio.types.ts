/**
 * Props of Radio component.
 *
 * @author Vitalii Bodnarchuk
 */
interface RadioTypes extends Pick<HTMLInputElement, 'name' | 'id' | 'checked'> {
  /**
   * The value of the radio input.
   */
  readonly value: string;

  /**
   * The callback function to be called when the radio input value changes.
   */
  readonly onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;

  /**
   * The label text for the radio input.
   */
  readonly text?: string;

  /**
   * The help text of the label for radio input.
   */
  readonly helpText?: string;

  /**
   * Is disabled.
   */
  readonly disabled?: boolean;
}

export type { RadioTypes };
