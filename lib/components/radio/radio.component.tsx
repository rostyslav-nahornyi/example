import { FC } from 'react';

import { Text } from '@components';
import classNames from 'classnames';
import styles from './radio.module.scss';
import { RadioTypes } from './radio.types';

/**
 * Radio component
 * @description Radio
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { RadioTypes } props - RadioTypes defined in the './radios.types.ts'
 */
const Radio: FC<RadioTypes> = ({
  name,
  id,
  value,
  onChange,
  checked,
  disabled,
  text,
  helpText,
}) => (
  <label
    id="labelId"
    htmlFor={id}
    className={classNames(styles.wrapper, disabled && styles.disabledWrapper)}
  >
    <input
      className={styles.radioInput}
      type={'radio'}
      name={name}
      id={id}
      value={value}
      onChange={onChange}
      checked={checked}
      hidden
      disabled={disabled}
      aria-labelledby="labelId"
    />
    <div className={styles.block}>
      <span
        tabIndex={disabled ? -1 : 0}
        role="radio"
        aria-checked={checked}
        className={styles.customRadio}
      />
      {helpText ? (
        <div className={styles.helpBlock}>
          <Text
            variant="caption16Medium"
            className={styles.label}
          >
            {text}
          </Text>
          <Text
            variant="caption14Regular"
            className={styles.text}
          >
            {helpText}
          </Text>
        </div>
      ) : (
        <Text
          variant="caption14Medium"
          className={styles.label}
        >
          {text}
        </Text>
      )}
    </div>
  </label>
);

export { Radio };
