import { ReactNode } from 'react';
/**
 * Props of Avatar component.
 *
 * @author Vitalii Bodnarchuk
 */

type AvatarProps = {
  /**
   * The color of avatar
   */
  readonly background?: 'light' | 'gray' | 'dark';
  /**
   * The size of avatar
   */
  readonly size: 'xxs' | 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';
  /**
   * Status indicator
   */
  readonly statusIndicatorPosition?: 'off' | 'top' | 'bottom';
  /**
   * The img
   */
  readonly img?: ReactNode;

  /**
   * Image url
   */
  readonly src?: string;

  /**
   * Initials
   */
  readonly initials?: string;

  /**
   * Placeholder
   */
  readonly placeholder?: boolean;

  /**
   * Add style
   */
  readonly className?: string;
  /**
   * onClick
   */
  readonly onClick?: () => void;
};

export type { AvatarProps };
