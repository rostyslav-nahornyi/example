import AvatarIcon from '@assets/icons/avatar.svg';
import { Meta, StoryFn } from '@storybook/react';
import { Avatar } from './avatar.components';

export default {
  title: 'UI-KIT/Avatar',
  component: Avatar,
} as Meta<typeof Avatar>;

const Template: StoryFn<typeof Avatar> = (props) => <Avatar {...props} />;

const AvatarImg = Template.bind({});
AvatarImg.args = {
  size: 'xxs',
  img: <AvatarIcon />,
  statusIndicatorPosition: 'bottom',
};

const AvatarImgUrl = Template.bind({});
AvatarImgUrl.args = {
  size: 'xxl',
  src: 'https://lh3.googleusercontent.com/-u2l-x3rZbF8/TXXCuY14tgI/AAAAAAAAAGs/0qHOPsR6BJA/s1600/Kaji.5.jpg',
  statusIndicatorPosition: 'top',
};

const AvatarPlaceholder = Template.bind({});
AvatarPlaceholder.args = {
  size: 's',
  placeholder: true,
  statusIndicatorPosition: 'bottom',
  background: 'light',
};

const AvatarInitials = Template.bind({});
AvatarInitials.args = {
  size: 'l',
  background: 'dark',
  initials: 'JW',
};

export { AvatarImg, AvatarImgUrl, AvatarInitials, AvatarPlaceholder };
