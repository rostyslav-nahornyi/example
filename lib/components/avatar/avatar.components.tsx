import { Indicator } from '@components';
import { AvatarProps } from './avatar.types';

import UserIcon from '@assets/icons/user.svg';
import classNames from 'classnames';
import { FC } from 'react';
import styles from './avatar.module.scss';

/**
 * Avatar component
 * @description Avatar
 *
 * @author Vitalii Bodnarchuk
 * @category Components
 * @param { AvatarTypes } props - AvatarTypes defined in the './avatar.types.ts'
 */

const Avatar: FC<AvatarProps> = ({
  background,
  size,
  statusIndicatorPosition = 'off',
  img,
  src,
  placeholder,
  initials,
  className,
  onClick,
}) => {
  const indicatorSize = size === 'xxs' ? 'xs' : size;

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter' || event.key === ' ') {
      onClick && onClick();
    }
  };

  return (
    <div className={styles.wrapper}>
      <div
        role="button"
        tabIndex={0}
        className={classNames(
          styles.avatar,
          styles[size],
          background && styles[background],
          className,
        )}
        onClick={onClick}
        onKeyDown={handleKeyDown}
      >
        {img && img}
        {src && !img && (
          <img
            src={src}
            alt={src}
            className={styles.src}
          />
        )}
        {placeholder && (
          <div className={styles.icon}>
            <UserIcon />
          </div>
        )}
        {initials && <span>{initials}</span>}
        {statusIndicatorPosition !== 'off' && (
          <Indicator
            className={statusIndicatorPosition === 'top' ? styles.top : styles.bottom}
            type="success"
            size={indicatorSize}
          />
        )}
      </div>
    </div>
  );
};

export { Avatar };
