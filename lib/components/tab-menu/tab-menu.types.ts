import { HTMLAttributes } from 'react';
import { PillTabProps } from '../tab/components/pill-tab/pill-tab.types';
import { TabProps } from '../tab/tab.types';

/**
 * TabMenuProps
 * @description Props for the TabMenu component
 *
 * @author Vitalii Bodnarchuk
 */
interface TabMenuProps
  extends Pick<HTMLAttributes<HTMLDivElement>, 'className'>,
    Pick<PillTabProps, 'size'> {
  readonly tabs: ReadonlyArray<TabProps & { readonly link?: string; readonly id: string }>;
  readonly menuType: 'underline' | 'pill' | 'pillWithBorders';
}

export type { TabMenuProps };
