import { FC, useState } from 'react';

import { Tab } from '@components';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import { PillTabProps } from '../tab/components/pill-tab/pill-tab.types';
import styles from './tab-menu.module.scss';
import { TabMenuProps } from './tab-menu.types';

const renderTabItem = (
  item: TabMenuProps['tabs'][0],
  size: PillTabProps['size'],
  key?: string,
): Record<TabMenuProps['menuType'], JSX.Element> => ({
  underline: (
    <Tab.Underline
      {...item}
      size={size ?? 'large'}
      key={key}
    />
  ),
  pill: (
    <Tab.Pill
      {...item}
      size={size ?? 'large'}
      key={key}
    />
  ),
  pillWithBorders: (
    <Tab.Pill
      {...item}
      size={size ?? 'large'}
      key={key}
    />
  ),
});

/**
 * TabMenu component
 * @description The horizontal menu with switchable tabs
 *
 * @author Vitalii Bodnarchuk, Sergii Goncharuk
 * @category Components
 * @param { TabMenuProps } props - TabMenuProps defined in the './tab-menu.types.ts'
 */
const TabMenu: FC<TabMenuProps> = ({ tabs, menuType, className, size }) => {
  const [selectedTab, setSelectedTab] = useState<string | undefined>();
  const handleSelectTab = (id?: string) => {
    setSelectedTab(id);
  };

  return (
    <div
      className={classNames(
        styles.wrapper,
        menuType === 'pillWithBorders' && styles.pillWithBorders,
        className,
      )}
    >
      {tabs?.map(({ label, link, id, onClick, ...rest }, index) => {
        const key = link ? link.concat(label ?? index.toString()) : label ?? index.toString();
        const isSelected = id === selectedTab;

        return link ? (
          <NavLink
            key={key}
            to={link}
            className={styles.link}
            onClick={() => handleSelectTab(id)}
          >
            {renderTabItem({ id, label, link, ...rest, isSelected }, size, key)[menuType]}
          </NavLink>
        ) : (
          renderTabItem({ id, label, link, onClick, ...rest }, size, key)[menuType]
        );
      })}
    </div>
  );
};

export { TabMenu };
