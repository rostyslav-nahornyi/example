/* eslint-disable react/prop-types */
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { TabMenu } from './tab-menu.component';
import './tab-menu.module.scss';

export default {
  title: 'UI-KIT/TabMenu',
  component: TabMenu,
  decorators: [
    (Story) => (
      <BrowserRouter>
        <Routes>
          <Route
            path={'*'}
            element={<Story />}
          />
          <Route
            path={'/tab 1'}
            element={
              <>
                <Story />
                <div style={{ marginTop: '1rem' }}>We are on page 1</div>
              </>
            }
          />
          <Route
            path={'/tab 2'}
            element={
              <>
                <Story />
                <div style={{ marginTop: '1rem' }}>We are on page 2</div>
              </>
            }
          />
          <Route
            path={'/tab 3'}
            element={
              <>
                <Story />
                <div style={{ marginTop: '1rem' }}>We are on page 3</div>
              </>
            }
          />
        </Routes>
      </BrowserRouter>
    ),
  ],
} as Meta<typeof TabMenu>;

const Template: StoryFn<typeof TabMenu> = ({ tabs, menuType, size }) => {
  const [selectedTab, setSelectedTab] = useState<string>();

  const handleSelectTab = (id?: string) => setSelectedTab(id);

  return (
    <TabMenu
      menuType={menuType}
      {...{
        tabs: tabs.map((item) =>
          item.link
            ? item
            : {
                ...item,
                onClick: () => {
                  handleSelectTab(item.label);
                },
                isSelected: item.id === selectedTab,
              },
        ),
      }}
      size={size}
    />
  );
};

const PillTabMenu = Template.bind({});
PillTabMenu.args = {
  tabs: [
    {
      label: 'Tab 1',
      link: '/tab 1',
      id: 'tab1',
      color: 'blue',
    },
    {
      label: 'Tab 2',
      link: '/tab 2',
      id: 'tab2',
      color: 'blue',
    },
    {
      label: 'Tab 3',
      link: '/tab 3',
      id: 'tab3',
      color: 'blue',
    },
  ],
  menuType: 'pill',
  size: 'large',
};

const UnderlineTabMenu = Template.bind({});
UnderlineTabMenu.args = {
  tabs: PillTabMenu.args.tabs,
  menuType: 'underline',
  size: 'large',
};

const PillTabMenuWithBorders = Template.bind({});
PillTabMenuWithBorders.args = {
  tabs: [
    {
      label: 'Tab 1',
      link: '/tab 1',
      id: 'tab1',
      color: 'white',
    },
    {
      label: 'Tab 2',
      link: '/tab 2',
      id: 'tab2',
      color: 'white',
    },
    {
      label: 'Tab 3',
      link: '/tab 3',
      id: 'tab3',
      color: 'white',
    },
  ],
  menuType: 'pillWithBorders',
  size: 'large',
};

export { PillTabMenu, PillTabMenuWithBorders, UnderlineTabMenu };
