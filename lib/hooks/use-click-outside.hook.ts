import { RefObject, useCallback, useEffect } from 'react';

/**
 * useClickOutside hook.
 * Used fo invoking callbacks, when clicking outside the ref element.
 * @author Vitalii Bodnarchuk
 * @category Hooks
 * @param { RefObject<HTMLElement> } ref - ref element to watch on.
 * @param { Function } handleClickOutside - a callback function invoked when clicked outside the ref element.
 */
const useClickOutside = (ref: RefObject<HTMLElement>, handleClickOutside: () => void): void => {
  const handleClick = useCallback(
    (event: PointerEvent) => {
      if (
        ref.current &&
        !ref.current.contains(event.target as Element) &&
        event.button !== 2 &&
        !(event.target as HTMLElement)?.dataset?.ignoreclickoutside
      ) {
        handleClickOutside();
      }
    },
    [handleClickOutside, ref],
  );

  useEffect(() => {
    document.addEventListener('pointerdown', handleClick);
    return () => document.removeEventListener('pointerdown', handleClick);
  }, [handleClick]);
};

export { useClickOutside };
