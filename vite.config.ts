import react from '@vitejs/plugin-react-swc';
import path, { join, resolve } from 'node:path';
import { defineConfig } from 'vite';
import { chunkSplitPlugin } from 'vite-plugin-chunk-split';
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';
import dts from 'vite-plugin-dts';
import svgr from 'vite-plugin-svgr';

import { peerDependencies } from './package.json';

export default defineConfig({
  plugins: [
    svgr({ include: '**/*.svg' }),
    react(),
    dts({ rollupTypes: true }),
    cssInjectedByJsPlugin(),
    chunkSplitPlugin(),
  ],
  build: {
    target: 'esnext',
    minify: true,
    lib: {
      entry: resolve(__dirname, join('lib', 'index.ts')),
      fileName: 'index',
      formats: ['es', 'cjs'],
    },
    rollupOptions: {
      external: ['react/jsx-runtime', ...Object.keys(peerDependencies)],
    },
  },
  resolve: {
    alias: {
      '@components': path.resolve(__dirname, './lib/components'),
      '@styles': path.resolve(__dirname, './lib/styles'),
      '@hooks': path.resolve(__dirname, './lib/hooks'),
      '@assets': path.resolve(__dirname, './lib/assets'),
      '@store': path.resolve(__dirname, './lib/store'),
    },
  },
  css: { devSourcemap: true },
});
