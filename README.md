# ZADE DESIGN SYSTEM

This is a Zade Design System – comprehensive collection of reusable components, styles, and guidelines designed to help you build consistent, accessible, and high-quality user interfaces with `React`.

## Features

- React library;
- TypeScript: A strongly typed superset of JavaScript;
- SCSS: CSS preprocessor;
- Storybook: A frontend workshop for building UI components and pages in isolation;
- Vite: A next generation frontend tooling that runs and builds your library incredibly fast;
- ESLint: A tool that finds and fixes problems in your code;
- Prettier: A code formatter;
- Husky: A pre-commit, pre-push hook;
- Commitlint: Commit message conventions;
- Surge: Auto deploy when pushing changes;
- pnpm: Fast, disk space efficient package manager;
- NPM package: A library that published in npm registry.

## Getting Started

1. Clone this repository;

2) Install pnpm using `npm install -g pnpm`;
3) Install dependencies `pnpm i`;
4) Start project: `pnpm dev`.

## Scripts

An explanation of the `package.json` scripts.

| Command     | Description                                                                        |
| ----------- | ---------------------------------------------------------------------------------- |
| `dev`       | Starts the local Storybook server, use this to develop and preview your components |
| `build`     | Builds your Storybook as a static web application                                  |
| `build:lib` | Builds your component library with Vite                                            |
| `lint`      | Runs ESLint tool                                                                   |
| `format`    | Formats your code with Prettier                                                    |
| `prepare`   | Installs Husky, a tool for setting up Git hooks                                    |

## Contributing | Add feature/fix/improvement

1. Create a Branch `git checkout -b feature/your-feature-name`;
2. Upgrade npm [version](https://docs.npmjs.com/about-semantic-versioning#using-semantic-versioning-to-specify-update-types-your-package-can-accept) with `npm version {patch/minor/major}`
3. Commit your changes with [convential commit](https://www.conventionalcommits.org/en/v1.0.0/). For example: `git commit -m "feat(header): added new component"`;
4. Push new branch `git push origin feature/your-feature-name`
5. Create Merge Request or merge with `main` branch immediately.

## Storybook deploy

There are two ways to get deployed version:

First:

1. Build storybook via `pnpm build:storybook`;
2. Go to the build folder `cd storybook-static`;
3. Deploy with surge: `npx surge` (press enter twice);
4. Go to the link in console to see deploy.
   \
   Second:
   When you push changes to the `main` branch, pre-push husky hook will auto deploy app with new changes to the [deployed Storybook link](https://zade-design-system-official.surge.sh/).

## Publish new version | NPM

Requirements:

- Be sure that you have raised npm package version (see Contribution Guideline, p. 2): `npm version {patch/minor/major}`;
- Ask for invite as a developer in [npm package](https://www.npmjs.com/package/zade-design-system) (see Support paragraph).

1. Build your lib via Vite: `pnpm build: lib`;
2. Publish new version: `npm publish`.

## Support

If you encounter any issues or have questions, please reach out to our support team:

Emails: \
oleksiy.medvediev@zade.agency;\
rostyslav.nahornyi@zade.agency;\
sergii.goncharuk@zade.agency.

Bugs, improvements? [Issues board](https://gitlab.com/zade-agency/public/design-system/-/boards).

## Links:

- [Issue board](https://gitlab.com/zade-agency/public/design-system/-/boards);
- [Git repository](https://gitlab.com/zade-agency/public/design-system);
- [Npm package](https://www.npmjs.com/package/zade-design-system);
- [Deploy storybook version](https://zade-design-system-official.surge.sh/).
